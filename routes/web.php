<?php

Route::get('/','DashboardController@index')->middleware('auth');
Route::get('account-settings','UsersController@getSettings');
Route::post('account-settings','UsersController@saveSettings');
Route::get('violation/reindex','ViolationController@reindex');
Route::group(['middleware' => ['auth', 'roles'],'roles' => ['admin','user','Company']], function () {

    Route::get('dashboard','DashboardController@index');



});
Route::get('logout','Auth\LoginController@logout');
function formatDateForm($date=null){ if(!empty($date)){return  date("m-d-Y", strtotime($date));}return '';}
function formatDateDb($date=null,$format='m-d-Y'){ if(!empty($date)){$datetime = DateTime::createFromFormat($format,$date);return  $datetime->format('Y-m-d');}return '';}
//Auth::routes();
Auth::routes(['register' => false]);
Route::resource('mvr_misc_reports', 'Mvr_misc_reportsController');
Route::resource('mvr_reports', 'Mvr_reportsController');
Route::resource('mvr-files', 'MvrFilesController');

