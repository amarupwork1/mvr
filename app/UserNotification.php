<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Scopes\SourceCompanyScope;
use App\Scopes\CompanyRoleScope;
use Illuminate\Database\Eloquent\SoftDeletes;



class UserNotification extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'notifications';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['id','type', 'notifiable_type', 'notifiable_id', 'data'];

      protected $casts = [
        'data' => 'array',
    ];
    protected $dates = ['read_at'];


     public function user()
    {
        return $this->belongsTo('App\User','notifiable_id');
    }
     


}
