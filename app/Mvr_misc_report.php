<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mvr_misc_report extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'mvr_misc_reports';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['mvr_report_id', 'cdl_medical_self_certification', 'medical_examiner_specialty', 'examiner_juridiction', 'medical_restriction', 'medical_examiner', 'expire_date', 'issue_date', 'registry', 'license', 'status', 'phone'];

     public function report()
    {
        return $this->belongsTo('App\Mvr_report');
    }

    
}
