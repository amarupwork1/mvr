<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\HtmlString;


class ViolationCitationNotification extends Notification
{
    use Queueable;

    public $data;


    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $this->violationTable($data);
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line(new HtmlString($this->data));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }

    public function violationTable($data)
    {

        $data = json_decode(json_encode($data), true);
        $html ="<h4>Violation Citation Notification</h4>";
       $html .= '<table width="100%" border="1"><thead><th width="50%" >Type </th><th width="50%">Value</th></thead><tbody>';
       //print_r($data); die;
        foreach ($data as $key => $value) {
            $html .= '<tr><td width="50%">'.$key.'</td> <td width="50%">'.$value.'</td></tr>';
            //echo'<br>'.$key;
        }

       // echo '<pre>'; print_r($myArray); die;
        return $html .= '</tbody></table>'; 
    }
}
