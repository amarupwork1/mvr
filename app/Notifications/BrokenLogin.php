<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Carbon\Carbon;

class BrokenLogin extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public $file_name;
    public $path;
    public $name;
    public $type;

    public function __construct($file_name,$path,$type)
    {
        $this->file_name = $file_name;
        $this->path = $path;

        if($type == 'cdlmanager.com')
        {
           $this->name = 'CDL Managers Broken Login '.Carbon::now()->format('m-d-Y'); 
        }
        else
        {
            $this->name = 'CSA WatchPortal Broken Login '.Carbon::now()->format('m-d-Y'); 
        }
        

    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                 //    ->error()
        //             ->from('notifications@cdlmanager.com', 'Notifications')
                     ->subject($this->name)
                    ->bcc("ricky@cdlconsultants.com")
                    ->bcc("justin@cdlconsultants.com")
                    ->line('Latest Broken login accounts details are attached in attached file.')
                    ->attach($this->path,[
                    'as' => $this->name.'.xlsx',
                    'mime' => 'application/xlsx',
                    ]);
                   
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
