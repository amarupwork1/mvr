<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\HtmlString;


class InspectionNotification extends Notification implements ShouldQueue
{
    use Queueable;

    public $data;
    public $type;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($type,$data)
    {
        $this->type = $type;

        if($this->type=='inspection_tabel')
        {
            $this->data = $this->inspectionTable($data);
        }

        else if($this->type =='tickets_table')
        {
            $this->data = $this->TicketTable($data);
        }
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line(new HtmlString($this->data));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }



    public function inspectionTable($data)
    {

        $data = json_decode(json_encode($data), true);
        $html ="<h4>Inspection Data</h4>";
       $html .= '<table width="100%" border="1"><thead><th width="50%" >Key </th><th width="50%">value</th></thead><tbody>';
       //print_r($data); die;
        foreach ($data as $key => $value) {
            $html .= '<tr><td width="50%">'.$key.'</td> <td width="50%">'.$value.'</td></tr>';
            //echo'<br>'.$key;
        }

       // echo '<pre>'; print_r($myArray); die;
        return $html .= '</tbody></table>'; 
    }


    public function TicketTable($data)
    {

        $data = json_decode(json_encode($data), true);
        $html ="<h4> State Citations</h4>";
       $html .= '<table width="100%" border="1"><thead><th width="50%" >Type  </th><th width="50%">Value</th></thead><tbody>';
       //print_r($data); die;
        foreach ($data as $key => $value) {
            $html .= '<tr><td width="50%">'.$key.'</td> <td width="50%">'.$value.'</td></tr>';
            //echo'<br>'.$key;
        }

       // echo '<pre>'; print_r($myArray); die;
        return $html .= '</tbody></table>'; 
    }
}
