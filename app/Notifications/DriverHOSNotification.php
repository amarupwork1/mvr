<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class DriverHOSNotification extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */

    public $link;
    public $name;

    public function __construct($data=null)
    {
        $this->link = $data['link'];
        $this->name = $data['name'];
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                   // ->error()
         //            ->from('notifications@cdlmanager.com', 'Notifications')
                     ->subject('HOS Signing Request ')


                    ->line('This is to inform you that, new Hour of Service form is available for signing ')
                    ->line('Follow the link bellow to sign into portal and check the service records ')
                    ->action('Click here ', url($this->link))
                   // ->bcc("rickywhitedesigns@gmail.com")
                    ->bcc("tashanpb@gmail.com")
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
