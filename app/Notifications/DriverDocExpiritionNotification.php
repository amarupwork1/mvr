<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class DriverDocExpiritionNotification extends Notification implements ShouldQueue
{
    use Queueable;

    public $message;
    public $name;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($message,$name)
    {
        $this->message = $message;
        $this->name = $name;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                     ->error()
      //               ->from('notifications@cdlmanager.com', 'Notifications')
                     ->subject($this->message)
                    ->line('Hi, '.$this->name)
                    ->line($this->message)
                    //->bcc("rickywhitedesigns@gmail.com")
                    ->bcc("tashanpb@gmail.com");
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
