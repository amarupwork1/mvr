<?php

namespace App\Scopes;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;
use Auth;
class SourceFmcsaScope implements Scope
{
    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @return void
     */

    public function apply(Builder $builder, Model $model)
    {
      if(Auth::user())
      {
       $user = Auth::user();
       if($user->roles)
        {
       $rolesUser = $user->roles->pluck('id')->toArray();

       if(in_array('14',$rolesUser))
       {
                
          $builder->where('source','like', '%cdlmanager.com%');

       }

      if(in_array('15',$rolesUser))
       {

          $builder->where('source', 'like','%csawatchportal.com%');
          
       }
  }
        
}
}
    
}
