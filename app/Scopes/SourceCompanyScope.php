<?php

namespace App\Scopes;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;
use Auth;
class SourceCompanyScope implements Scope
{
    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @return void
     */

    public function apply(Builder $builder, Model $model)
    {
        if(Auth::user())
        {
        $user = Auth::user();
        if($user->roles)
        {
       $rolesUser = $user->roles->pluck('id')->toArray();

       if(in_array('14',$rolesUser))
       {

                $builder->whereHas('Fmcsa_user', function ($query) {
                        $query->where('source','like', '%cdlmanager.com%');
                });
       }

      else if(in_array('15',$rolesUser))
       {

        $builder->whereHas('Fmcsa_user', function ($query) {
                        $query->where('source', 'like','%csawatchportal.com%');
                });
       }

     }

        
}

    $builder->whereHas('Fmcsa_user');
}
    
}
