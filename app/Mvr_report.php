<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mvr_report extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'mvr_reports';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['report_provided_by', 'date_completed', 'customer_sub', 'customer_ref', 'first_name', 'mvr_status', 'last_name', 'customer', 'license', 'accident_info', 'actor', 'state', 'license_info', 'driver_info'];

    public function misc()
    {
        return $this->hasOne('App\Mvr_misc_report');
    }

    public function violations(){
        return $this->hasMany('App\Mvr_viol_report');
    }
    public function driver()
    {
        return $this->hasOne('App\Mvr_driver_report');
    }


}
