<?php

namespace App\Helper;
use DB;
use GuzzleHttp\Client;
use Illuminate\Support\Str;
use App\Hos_service_record;
class Signer 
{

    public $signer;
    function __construct() {
        $this->signer = config('signer.signer_url');
    }

      public function  createOrUpdate($userdata)
      {
   

        $isUserExist = DB::connection('signer')->table('users')->where('email', $userdata['email'])->get()->toArray();
        if(count($isUserExist) > 0){
            
                    $signerUserStatus = DB::connection('signer')->table('users')
                                            ->where('email',$userdata['email'])
                                            ->update(['auth_token' => $userdata['auth_token']]);
                    $signer_user_id =  $isUserExist[0]->id;
                }else{
                    $signerUserStatus = DB::connection('signer')->table('users')->insert($userdata);
                    $signer_user_id = DB::connection('signer')->getPdo()->lastInsertId();
                }

            return  $signer_user_id;

     }


     public function uploadFile($data,$requestData,$file_path)
     {

                $signer = config('signer.signer_url');
                $signer_url = $signer. '/uploads/files/pz10.php';

                $client = new Client();
                $response = $client->request('POST', $signer_url, [
                                'multipart' => [
                                     [
                                        'name'     => 'bridge_key',
                                        'contents' => 'jhdfg%^$&*bjoasd)({}',
                                    ],
                                    [
                                        'name'     => 'fileToUpload',
                                        'contents' => fopen($file_path, 'r')
                                    ],
                                ]
                            ]);

                if($response->getStatusCode() == 200 && $response->getBody() == 'success'){
                    $document_key = Str::random(32);
                    $fileStatus = DB::connection('signer')->table('files')->insert($data);
                    if($fileStatus){
                        $reqStatus = DB::connection('signer')->table('requests')->insert($requestData);

                        $reqStatus = DB::connection('signer')->table('history')->insert([
                            'file' => $requestData['document'],
                            'company' => 1,
                            'activity' => 'File uploaded by <span class="text-primary">CDL Staff</span>.',
                            'type' => 'default'
                        ]);

                    }
                }
     }


     public  function loginSigner($signer_url)
     {
               // $signer = config('signer.signer_url');



                
                $client = new Client();
                $response = $client->request('GET', $signer_url);
                $response_data = json_decode($response->getBody()->getContents());
               // print_r($response_data);
              //  print_r($response_data->title);
                if(isset($response_data->status) && $response_data->status=='success' && $response_data->title=='Login Successful')
                {
                    return true;
                }
                else
                {
                    return false;
                }
   
     }

     public function updateHosStatus()
     {

        $hos_pendding_record = Hos_service_record::where('status',0)->pluck('document_key');
        
        $signed_document_key = DB::connection('signer')->table('files')->whereIn('document_key',$hos_pendding_record)->where('status','Signed')->pluck('document_key');

        DB::connection('signer')->table('requests')->whereIn('document',$signed_document_key)->update(['status'=>'Signed']);
        Hos_service_record::whereIn('document_key',$signed_document_key)->update(['status'=>1]);
     }


      public function  syncCreateOrUpdate($userdata)
      {
   

        $isUserExist = DB::connection('signer')->table('users')->where('email', $userdata['email'])->get()->toArray();
        if(count($isUserExist) > 0){
            
                    $signerUserStatus = DB::connection('signer')->table('users')
                                            ->where('email',$userdata['email'])
                                            ->update(['auth_token' => $userdata['auth_token'],'fname'=>$userdata['fname'],'UserType'=>$userdata['UserType'],'lname'=>$userdata['lname'],'companyName'=>$userdata['companyName'],'usdot'=>$userdata['usdot'],'phone'=>$userdata['phone']]);
                    $status =  'update';
                }else{
                    $signerUserStatus = DB::connection('signer')->table('users')->insert($userdata);
                    $signer_user_id = DB::connection('signer')->getPdo()->lastInsertId();
                    $status =  'create';
                }

            return  $status;

     }

     
}
