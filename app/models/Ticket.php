<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tickets';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'Name', 'company', 'company_id', 'Address', 'Birthdate', 'City', 'State', 'Zip', 'Dl_Number', 'Class_Commercial', 'Vehicle_Lic_No', 'Citation_Type', 'violation', 'Location_violation', 'City_County_Occurence', 'Speed_Approx', 'Arresting_Officer_Name', 'Note', 'file', 'path', 'date_time', 'user_email', 'indicator', 'lawyer_email', 'Admin_note', 'citation_no', 'status', 'updated_by', 'court_date', 'court_address', 'court_phone', 'ticket_dispo', 'date_issued', 'court_name', 'county', 'ticket_number', 'lawyer_id', 'road_side_inspection', 'road_side_inspection_results', 'sales_agent', 'fname', 'lname', 'sales_agent_name', 'sales_agent_email', 'sales_agent_id', 'sf_id', 'DataQ_Number__c', 'Roadside_Inspection_Number__c', 'Ticket_Type', 'Beginning_Fine_Amount', 'Final_Fine_Amount', 'Processor_Name', 'Processor_Email', 'Processor_Ph_Number', 'Processor_Notes_To_Attorney'];

    
}
