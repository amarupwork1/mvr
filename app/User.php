<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use App\HasRoles;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Company;
use App\Hos_service_record;

class User extends Authenticatable
{
    use Notifiable;
    use HasRoles;
    use SoftDeletes;

  
    protected $fillable = [
        'name', 'email', 'password',
    ];

   
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function profile(){
        return $this->hasOne(Profile::class);
    }
    public function company(){
        return $this->hasOne(Company::class);
    }
    public function driver(){
        return $this->hasOne(Driver::class);
    }

    public function auditor(){
        return $this->hasOne(Auditor::class);
    }

    public function company_drivers(){
        return $this->hasMany('App\Driver','company_id');

    }
    public function permissionsList(){
        $roles = $this->roles;
        $permissions = [];
        foreach ($roles as $role){
            $permissions[] = $role->permissions()->pluck('name')->implode(',');
        }
       return collect($permissions);
    }

    public function permissions(){
        $permissions = [];

        $roles = $this->roles()->get();
       // dd($roles->toArray());
         if($this->roles()->where('name','=','SuperAdmin')->first()!= null) {
          
            return $permissions = \App\Permission::all();
            
        }

        foreach ($roles as $role) {
            //echo $role->name . "<br>";
            $userpermissions = $role->permissions()->get();
            foreach ($userpermissions as  $per) {
                //echo $per->name . "</br>";
                $permissions[] = $per;
            }
        }
      
        return collect($permissions);
    }

      public function isSuperAdmin(){
        
       $role =$this->roles()->where('name','SuperAdmin')->first();
       if($role != null){
           $role = true;
       }else{
           $role = false;
       }
       return $role;
       }

    public function isAdmin(){
        
       $is_admin =$this->roles()->where('name','admin')->first();
       if($is_admin != null){
           $is_admin = true;
       }else{
           $is_admin = false;
       }
       return $is_admin;
    }

      public function isCompany(){
        
       $role =$this->roles()->where('name','Company')->first();
       if($role != null){
           $role = true;
       }else{
           $role = false;
       }
       return $role;
    }
    public function isDriver(){
        
       $role =$this->roles()->where('name','Driver')->first();
       if($role != null){
           $role = true;
       }else{
           $role = false;
       }
       return $role;
    }


    public function isClassAdmin(){
        
       $role =$this->roles()->where('name','Class/Quiz Admin')->first();
       if($role != null){
           $role = true;
       }else{
           $role = false;
       }
       return $role;
    }



    public function isLeadsAdmin(){
        
       $role =$this->roles()->where('name','Leads')->first();
       if($role != null){
           $role = true;
       }else{
           $role = false;
       }
       return $role;
    }

    public function isCdlAdmin(){
        
       $role =$this->roles()->where('name','CDL Manager Admin')->first();
       if($role != null){
           $role = true;
       }else{
           $role = false;
       }
       return $role;
    }

    public function isCsaAdmin(){
        
       $role =$this->roles()->where('name','CSA Watch Admin')->first();
       if($role != null){
           $role = true;
       }else{
           $role = false;
       }
       return $role;
    }



     public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    public function topics()
    {
        return $this->belongsToMany(Topic::class)->withPivot('status');
    }

    public function topics_pending()
    {
        return $this->belongsToMany(Topic::class)->wherePivot('status', 0)->withPivot('status');
    }

    public function topics_completed()
    {
        return $this->belongsToMany(Topic::class)->wherePivot('status', 1)->withPivot('status');
    }

    public function hos_records()
    {
        return $this->hasMany(Hos_service_record::class, 'driver_id', 'id')->where('status', 1);
    }

    public function unread_notification_count(){

      return $this->notifications()->whereNull('read_at')->count();

    }

    public function unread_notification(){

      return $this->notifications()->where('type','App\Notifications\DatabaseNotification')->latest()->limit('5');

    }

    public function userNotification(){

        return $this->hasMany('App\UserNotification');

    }


}
