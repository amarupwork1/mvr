<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Auth;

class Checkusertype
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if(Auth::user()){
            if(Auth::user()->UserType=='DemoUser')
            {
                DB::disconnect('mysql');
                Config::set('database.connections.mysql.database', 'demo_db_cdlmanagers');
            }
       
        }
        
        return $next($request);
    }
}
