<?php

namespace App\ Http\ Controllers;

use App\ Profile;
use App\ Role;
use App\ User;
use App\ Auditor;
use App\ Company;
use Carbon\ Carbon;
use Illuminate\ Cache\ RetrievesMultipleKeys;
use Illuminate\ Http\ Request;
use Illuminate\ Support\ Facades\ Session;
use File;
use App\ HasRoles;
use DB;

use Yajra\ Datatables\ Datatables;


class UsersController extends Controller {
	public
	function getIndex() {
		$users = User::get();
		return view( 'users.index2', compact( 'users' ) );
	}
	public
	function superadmin( Request $request ) {
		if(auth()->user()->UserType=='DemoUser'){
			return response(view('403'), 403);
		}

		if ( $request->ajax() ) {
			$users = Role::findOrfail( 3 )->users;
			return Datatables::of( $users )->addColumn( 'action', function ( $item ) {
				$model = str_slug( 'superadmin', '-' );
				$action = " ";

				if ( auth()->user()->permissions()->where( 'name', '=', 'edit-' . $model )->first() != null ) {
					$action .= '<a href="' . url( '/user/edit/' . $item->id ) . '"
                                               title="Edit User">
                                                <button class="btn btn-primary btn-sm">
                                                    <i class="fa fa-pencil-alt" aria-hidden="true"> </i> Edit
                                                </button>
                                            </a>';
				}
				if ( auth()->user()->permissions()->where( 'name', '=', 'delete-' . $model )->first() != null ) {
					$action .= '<form method="POST" id="form_' . $item->id . '" action="' . url( '/user/' . $item->id ) . '" accept-charset="UTF-8" style="display:inline"><input name="_token" type="hidden" value="' . csrf_token() . '"><input name="_method" type="hidden" value="DELETE">
                                            <button type="submit" value="form_' . $item->id . '" class="btn btn-danger btn-sm" title="Delete User">
                                            <i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                                                                                </form> ';
				}

				return $action;

			} )->addColumn( 'updated_at', function ( $item ) {
				return $item->updated_at->format('m/d/Y');
			} )

			->addColumn( 'created_at', function ( $item ) {
				return $item->created_at->diffForHumans();
			} )

			->addColumn( 'name', function ( $item ) {
				return ucwords( $item->name );
			} )->addColumn( 'roles', function ( $item ) {

				return ucwords( $item->roles()->pluck( 'name' )->implode( ', ' ) );
			} )

			->make( true );
		}
		$users = [];
		$users = Role::findOrfail( 3 )->users;
		$data[ 'title' ] = "Super Admin";
		return view( 'users.index', compact( 'users', 'data' ) );

	}
	public
	function admin( Request $request ) {

		//$users = Role::findOrfail(1)->users;

		if ( $request->ajax() ) {
			$users = Role::findOrfail( 1 )->users;
			return Datatables::of( $users )->addColumn( 'action', function ( $item ) {
				$model = str_slug( 'admin', '-' );
				$action = " ";

				if ( auth()->user()->permissions()->where( 'name', '=', 'edit-' . $model )->first() != null ) {
					$action .= '<a href="' . url( '/user/edit/' . $item->id ) . '"
                                               title="Edit User">
                                                <button class="btn btn-primary btn-sm">
                                                    <i class="fa fa-pencil-square-o" aria-hidden="true"> </i> Edit
                                                </button>
                                            </a>';
				}
				if ( auth()->user()->permissions()->where( 'name', '=', 'delete-' . $model )->first() != null ) {
					$action .= '<form method="POST" id="form_' . $item->id . '" action="' . url( '/user/' . $item->id ) . '" accept-charset="UTF-8" style="display:inline"><input name="_token" type="hidden" value="' . csrf_token() . '"><input name="_method" type="hidden" value="DELETE">
                                            <button type="submit" value="form_' . $item->id . '" class="btn btn-danger btn-sm" title="Delete User">
                                            <i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                                                                                </form> ';
				}

				return $action;

			} )->addColumn( 'updated_at', function ( $item ) {
				return $item->updated_at->format('m/d/Y');
			} )

			->addColumn( 'created_at', function ( $item ) {
				return $item->created_at->diffForHumans();
			} )


			->addColumn( 'name', function ( $item ) {
				return ucwords( $item->name );
			} )->addColumn( 'roles', function ( $item ) {

				return ucwords( $item->roles()->pluck( 'name' )->implode( ', ' ) );
			} )

			->make( true );
		}
		$users = [];
		$data[ 'title' ] = "Admin";
		return view( 'users.index', compact( 'users', 'data' ) );

	}
	public
	function manager( Request $request ) {


		if ( $request->ajax() ) {
			$users = Role::findOrfail( 4 )->users;
			return Datatables::of( $users )->addColumn( 'action', function ( $item ) {
				$model = str_slug( 'manager', '-' );
				$action = " ";

				if ( auth()->user()->permissions()->where( 'name', '=', 'edit-' . $model )->first() != null ) {
					$action .= '<a href="' . url( '/user/edit/' . $item->id ) . '"
                                               title="Edit User">
                                                <button class="btn btn-primary btn-sm">
                                                    <i class="fa fa-pencil-square-o" aria-hidden="true"> </i> Edit
                                                </button>
                                            </a>';
				}
				if ( auth()->user()->permissions()->where( 'name', '=', 'delete-' . $model )->first() != null ) {
					$action .= '<form method="POST" id="form_' . $item->id . '" action="' . url( '/user/' . $item->id ) . '" accept-charset="UTF-8" style="display:inline"><input name="_token" type="hidden" value="' . csrf_token() . '"><input name="_method" type="hidden" value="DELETE">
                                            <button type="submit" value="form_' . $item->id . '" class="btn btn-danger btn-sm" title="Delete User">
                                            <i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                                                                                </form> ';
				}

				return $action;

			} )->addColumn( 'updated_at', function ( $item ) {
				return $item->updated_at->format('m/d/Y');
			} )

			->addColumn( 'created_at', function ( $item ) {
				return $item->created_at->diffForHumans();
			} )

			->addColumn( 'name', function ( $item ) {
				return ucwords( $item->name );
			} )->addColumn( 'roles', function ( $item ) {

				return ucwords( $item->roles()->pluck( 'name' )->implode( ', ' ) );
			} )

			->make( true );
		}
		$users = [];
		$data[ 'title' ] = "Manager";
		return view( 'users.index', compact( 'users', 'data' ) );

	}
	public
	function company( Request $request ) {


		if ( $request->ajax() ) {
			$users = Role::findOrfail( 6 )->users;
			return Datatables::of( $users )->addColumn( 'action', function ( $item ) {
				$model = str_slug( 'company', '-' );
				$action = " ";

				if ( auth()->user()->permissions()->where( 'name', '=', 'edit-' . $model )->first() != null ) {
					$action .= '<a href="' . url( '/user/edit/' . $item->id ) . '"
                                               title="Edit User">
                                                <button class="btn btn-primary btn-sm">
                                                    <i class="fa fa-pencil-alt" aria-hidden="true"> </i> Edit
                                                </button>
                                            </a>';
				}
				if ( auth()->user()->permissions()->where( 'name', '=', 'delete-' . $model )->first() != null ) {
					$action .= '<form method="POST" id="form_' . $item->id . '" action="' . url( '/user/' . $item->id ) . '" accept-charset="UTF-8" style="display:inline"><input name="_token" type="hidden" value="' . csrf_token() . '"><input name="_method" type="hidden" value="DELETE">
                                            <button type="submit" value="form_' . $item->id . '" class="btn btn-danger btn-sm" title="Delete User">
                                            <i class="fa fa-trash" aria-hidden="true"></i> Delete</button>
                                                                                </form> ';
				}

				return $action;

			} )->addColumn( 'updated_at', function ( $item ) {
				return $item->updated_at->format('m/d/Y');
			} )

			->addColumn( 'created_at', function ( $item ) {
				return $item->created_at->diffForHumans();
			} )

			->addColumn( 'name', function ( $item ) {
				return ucwords( $item->name );
			} )->addColumn( 'roles', function ( $item ) {

				return ucwords( $item->roles()->pluck( 'name' )->implode( ', ' ) );
			} )

			->make( true );
		}
		$users = [];
		$data[ 'title' ] = "Company";
		return view( 'users.company', compact( 'users', 'data' ) );

	}
	public
	function driver( Request $request ) {


		if ( $request->ajax() ) {
			$users = Role::findOrfail( 7 )->users;
			
			return Datatables::of( $users )->addColumn( 'action', function ( $item ) {
				$model = str_slug( 'driver', '-' );
				$action = " ";

				if ( auth()->user()->permissions()->where( 'name', '=', 'edit-' . $model )->first() != null ) {
					$action .= '<a href="' . url( '/user/edit/' . $item->id ) . '"
                                               title="Edit User">
                                                <button class="btn btn-primary btn-sm">
                                                    <i class="fa fa-pencil-square-o" aria-hidden="true"> </i> Edit
                                                </button>
                                            </a>';
				}
				if ( auth()->user()->permissions()->where( 'name', '=', 'delete-' . $model )->first() != null ) {
					$action .= '<form method="POST" id="form_' . $item->id . '" action="' . url( '/user/' . $item->id ) . '" accept-charset="UTF-8" style="display:inline"><input name="_token" type="hidden" value="' . csrf_token() . '"><input name="_method" type="hidden" value="DELETE">
                                            <button type="submit" value="form_' . $item->id . '" class="btn btn-danger btn-sm" title="Delete User">
                                            <i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                                                                                </form> ';
				}

				return $action;

			} )->addColumn( 'updated_at', function ( $item ) {
				return $item->updated_at->format('m/d/Y');
			} )

			->addColumn( 'created_at', function ( $item ) {
				return $item->created_at->diffForHumans();
			} )

			->addColumn( 'name', function ( $item ) {
				return ucwords( $item->name );
			} )->addColumn( 'roles', function ( $item ) {

				return ucwords( $item->roles()->pluck( 'name' )->implode( ', ' ) );
			} )

			->make( true );
		}
		$users = [];
		$data[ 'title' ] = "Drivers";
		return view( 'users.index', compact( 'users', 'data' ) );

	}
	public
	function call_center( Request $request ) {


		if ( $request->ajax() ) {
			$users = Role::findOrfail( 8 )->users;
			return Datatables::of( $users )->addColumn( 'action', function ( $item ) {
				$model = str_slug( 'callcenter', '-' );
				$action = " ";

				if ( auth()->user()->permissions()->where( 'name', '=', 'edit-' . $model )->first() != null ) {
					$action .= '<a href="' . url( '/user/edit/' . $item->id ) . '"
                                               title="Edit User">
                                                <button class="btn btn-primary btn-sm">
                                                    <i class="fa fa-pencil-square-o" aria-hidden="true"> </i> Edit
                                                </button>
                                            </a>';
				}
				if ( auth()->user()->permissions()->where( 'name', '=', 'delete-' . $model )->first() != null ) {
					$action .= '<form method="POST" id="form_' . $item->id . '" action="' . url( '/user/' . $item->id ) . '" accept-charset="UTF-8" style="display:inline"><input name="_token" type="hidden" value="' . csrf_token() . '"><input name="_method" type="hidden" value="DELETE">
                                            <button type="submit" value="form_' . $item->id . '" class="btn btn-danger btn-sm" title="Delete User">
                                            <i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                                                                                </form> ';
				}

				return $action;

			} )->addColumn( 'updated_at', function ( $item ) {
				return $item->updated_at->format('m/d/Y');
			} )

			->addColumn( 'created_at', function ( $item ) {
				return $item->created_at->diffForHumans();
			} )

			->addColumn( 'name', function ( $item ) {
				return ucwords( $item->name );
			} )->addColumn( 'roles', function ( $item ) {

				return ucwords( $item->roles()->pluck( 'name' )->implode( ', ' ) );
			} )

			->make( true );
		}
		$users = [];
		$data[ 'title' ] = "Call Center";
		return view( 'users.index', compact( 'users', 'data' ) );

	}
	public
	function demo_account( Request $request ) {


		if ( $request->ajax() ) {
			$users = User::where('UserType','DemoUser');
			return Datatables::of( $users )->addColumn( 'action', function ( $item ) {
				$model = str_slug( 'demoaccount', '-' );
				$action = " ";

				if ( auth()->user()->permissions()->where( 'name', '=', 'edit-' . $model )->first() != null ) {
					$action .= '<a href="' . url( '/user/edit/' . $item->id ) . '"
                                               title="Edit User">
                                                <button class="btn btn-primary btn-sm">
                                                    <i class="fa fa-pencil-square-o" aria-hidden="true"> </i> Edit
                                                </button>
                                            </a>';
				}
				if ( auth()->user()->permissions()->where( 'name', '=', 'delete-' . $model )->first() != null ) {
					$action .= '<form method="POST" id="form_' . $item->id . '" action="' . url( '/user/' . $item->id ) . '" accept-charset="UTF-8" style="display:inline"><input name="_token" type="hidden" value="' . csrf_token() . '"><input name="_method" type="hidden" value="DELETE">
                                            <button type="submit" value="form_' . $item->id . '" class="btn btn-danger btn-sm" title="Delete User">
                                            <i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                                                                                </form> ';
				}

				return $action;

			} )->addColumn( 'updated_at', function ( $item ) {
				return $item->updated_at->format('m/d/Y');
			} )

			->addColumn( 'created_at', function ( $item ) {
				return $item->created_at->diffForHumans();
			} )

			->addColumn( 'name', function ( $item ) {
				return ucwords( $item->name );
			} )->addColumn( 'roles', function ( $item ) {

				return ucwords( $item->roles()->pluck( 'name' )->implode( ', ' ) );
			} )

			->make( true );
		}
		$users = [];
		$data[ 'title' ] = "Demo Account";
		return view( 'users.index', compact( 'users', 'data' ) );

	}
	public
	function auditor( Request $request ) {


		if ( $request->ajax() ) {
			$users = Role::findOrfail( 10 )->users;
			return Datatables::of( $users )->addColumn( 'action', function ( $item ) {
				$model = str_slug( 'manager', '-' );
				$action = " ";

				if ( auth()->user()->permissions()->where( 'name', '=', 'edit-' . $model )->first() != null ) {
					$action .= '<a href="' . url( '/auditor/edit/' . $item->id ) . '"
                                               title="Edit User">
                                                <button class="btn btn-primary btn-sm">
                                                    <i class="fa fa-pencil-square-o" aria-hidden="true"> </i> Edit
                                                </button>
                                            </a>';
				}
				if ( auth()->user()->permissions()->where( 'name', '=', 'delete-' . $model )->first() != null ) {
					$action .= '<form method="POST" id="form_' . $item->id . '" action="' . url( '/user/' . $item->id ) . '" accept-charset="UTF-8" style="display:inline"><input name="_token" type="hidden" value="' . csrf_token() . '"><input name="_method" type="hidden" value="DELETE">
                                            <button type="submit" value="form_' . $item->id . '" class="btn btn-danger btn-sm" title="Delete User">
                                            <i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                                                                                </form> ';
				}

				return $action;

			} )->addColumn( 'updated_at', function ( $item ) {
				return $item->updated_at->format('m/d/Y');
			} )

			->addColumn( 'created_at', function ( $item ) {
				return $item->created_at->diffForHumans();
			} )

			->addColumn( 'name', function ( $item ) {
				return ucwords( $item->name );
			} )

			->addColumn( 'company', function ( $item ) {
				return ucwords( $item->auditor->company->name );
			} )

			->addColumn( 'roles', function ( $item ) {

				return ucwords( $item->roles()->pluck( 'name' )->implode( ', ' ) );
			} )

			->make( true );
		}
		$users = [];
		$data[ 'title' ] = "Auditor";
		return view( 'users.auditor', compact( 'users', 'data' ) );

	}

	public
	function create() {
		//$roles = auth()->user()->roles()->get();
		if(auth()->user()->UserType=='DemoUser'){
			return response(view('403'), 403);
		}
		$roles = Role::all();
		return view( 'users.create', compact( 'roles' ) );
	}

	public
	function create_auditor() {
		if(auth()->user()->UserType=='DemoUser'){
			return response(view('403'), 403);
		}
		$roles = Role::where('name','Auditor')->get();
		$companies = Role::findOrFail(6)->users()->has('company')->get();
		//dd($roles);
		return view( 'users.create_auditor', compact( 'roles','companies' ) );
	}

	public
	function save( Request $request ) {
		if(auth()->user()->UserType=='DemoUser'){
			return response(view('403'), 403);
		}
	
		$this->validate( $request, [
			'name' => 'required',
			'email' => 'required|unique:users,email',
			'password' => 'required|min:6|confirmed',
			//            'dob' => 'required',
			//            'pic_file' => 'required',
			//            'bio' => 'required',
			//            'gender' => 'required',
			//            'country' => 'required',
			//            'state' => 'required',
			//            'city' => 'required',
			//            'address' => 'required',
			//            'postal' => 'required',
			'role' => 'required',
			'pic_file'=>'image|mimes:jpg,jpeg,png,gif'

		], [
			'pic_file.required' => 'Profile picture required',
			'dob.required' => 'Date of Birth required'
		] );
		//        $user->assignRole($role->name);
		$requestData = $request->all();
		$user = User::firstOrCreate( [ 'name' => $request->name, 'email' => $request->email ] );
		$user->title = $request->title;

		 if(isset($requestData['role']['0']) && $requestData['role']['0'] ==9)
		  {
		  	$requestData['role']['0'] = 3;
		  	$user->UserType = 'DemoUser';
		  	$this->save_data_demo_db($requestData,$user->id);
		  }
		$user->status = 1;
		$user->password = bcrypt( $request->password );
		$user->save();

		if ( $file = $request->file( 'pic_file' ) ) {
			$extension = $file->extension() ? : 'png';
			$destinationPath = public_path() . '/storage/uploads/users/';
			$safeName = str_random( 10 ) . '.' . $extension;
			$file->move( $destinationPath, $safeName );
			$request[ 'pic' ] = $safeName;
		} else {
			$request[ 'pic' ] = 'no_avatar.jpg';
		}
		$profile = $user->profile;
		if ( $user->profile == null ) {
			$profile = new Profile();
		}
		if ( $request->dob != null ) {
			$date = formatDateDb($request->dob);
		} else {
			$date = $request->dob;
		}
		$profile->user_id = $user->id;
		$profile->bio = $request->bio;
		$profile->gender = $request->gender;
		$profile->dob = $date;
		$profile->country = $request->country;
		$profile->state = $request->state;
		$profile->city = $request->city;
		$profile->address = $request->address;
		$profile->postal = $request->postal;
		$profile->pic = $request[ 'pic' ];
		$profile->save();
		if ( $user->roles()->first() != null ) {
			//   $user->roles()->first()->pivot->delete();
			$user->roles()->detach();

		}

		foreach ( $requestData['role'] as $role_new ) {
			$role = Role::find( $role_new );

			// continue;
			if ( !$user->hasRole( $role->name ) ) {

				$user->assignRole( $role->name );
			}
			if ( $role->name == 'SuperAdmin' ) {
				$user->assignRole( 'admin' );
			}

			if ( $role->name == 'CDL Manager Admin' || $role->name == 'CSA Watch Admin' ) {
				$user->assignRole( 'admin' );
			}


			if ( $role->name == 'Company' ) {
				$company = $user->company;
				if ( $user->company == null ) {
					$company = new Company();
					$company->user_id = $user->id;
					$company->save();
				}
			}
		}

		Session::flash( 'message', 'User has been added' );
		return redirect()->back();
	}

	private function save_data_demo_db($data,$user_id)
	{
		if(auth()->user()->UserType=='DemoUser'){
			return response(view('403'), 403);
		}
		//dd($data);
		$userdata = [
			'id'=>$user_id,
			'title'=>$data['title'],
			'name'=>$data['name'],
			'email'=>$data['email'],
			'UserType'=>'DemoUser',
			'password'=>bcrypt( $data['password'] ),
		];

		
		$userCount = DB::connection('demoDb')->table('users')->where('id', $user_id)->count();
		if($userCount >0)
		{
			DB::connection('demoDb')->table('users')->where('id', $user_id)->update($userdata);
		}
		else
		{
		DB::connection('demoDb')->table('users')->insert($userdata);
		}
		$profile = [
			'user_id'=>$user_id,
			'dob'=>$data['dob'],
			'bio'=>$data['bio'],
			'gender'=>$data['gender'],
			'country'=>$data['country'],
			'state'=>$data['state'],
			'city'=>$data['city'],
			'address'=>$data['address'],
			'postal'=>$data['postal'],
			'pic'=>'no_avatar.jpg'
		];

		$userCount = DB::connection('demoDb')->table('profiles')->where('user_id', $user_id)->count();
		if($userCount >0)
		{
		DB::connection('demoDb')->table('profiles')->where('user_id', $user_id)->update($profile);
		}
		else
		{
			DB::connection('demoDb')->table('profiles')->insert($profile);
		}
		DB::connection('demoDb')->table('role_user')->where('user_id',$user_id)->delete();
		DB::connection('demoDb')->table('role_user')->insert(['role_id'=>1,'user_id'=>$user_id]);
		DB::connection('demoDb')->table('role_user')->insert(['role_id'=>3,'user_id'=>$user_id]);


	}
function save_auditor( Request $request ) {

if(auth()->user()->UserType=='DemoUser'){
			return response(view('403'), 403);
		}
		$this->validate( $request, [
			'name' => 'required',
			'email' => 'required|unique:users,email',
			'password' => 'required|min:6|confirmed',
			'role' => 'required',

		], [
			'pic_file.required' => 'Profile picture required',
			'dob.required' => 'Date of Birth required'
		] );
		//        $user->assignRole($role->name);

		$user = User::firstOrCreate( [ 'name' => $request->name, 'email' => $request->email ] );
		$user->status = 1;
		$user->password = bcrypt( $request->password );
		$user->save();

		if ( $file = $request->file( 'pic_file' ) ) {
			$extension = $file->extension() ? : 'png';
			$destinationPath = public_path() . '/storage/uploads/users/';
			$safeName = str_random( 10 ) . '.' . $extension;
			$file->move( $destinationPath, $safeName );
			$request[ 'pic' ] = $safeName;
		} else {
			$request[ 'pic' ] = 'no_avatar.jpg';
		}
		$profile = $user->profile;
		if ( $user->profile == null ) {
			$profile = new Profile();
		}
		if ( $request->dob != null ) {
			$date = Carbon::parse( $request->dob )->format( 'Y-m-d' );
		} else {
			$date = $request->dob;
		}
		$profile->user_id = $user->id;
		$profile->bio = $request->bio;
		$profile->gender = $request->gender;
		$profile->dob = $date;
		$profile->country = $request->country;
		$profile->state = $request->state;
		$profile->city = $request->city;
		$profile->address = $request->address;
		$profile->postal = $request->postal;
		$profile->pic = $request[ 'pic' ];
		$profile->save();
		if ( $user->roles()->first() != null ) {
			$user->roles()->detach();
		}

		
			$role = Role::find( $request->role );
			if ( !$user->hasRole( $role->name ) ) {
				$user->assignRole( $role->name );
			}

			
				$auditor = $user->auditor;
				if ( $user->auditor == null ) {
					$auditor = new Auditor();
					$auditor->user_id = $user->id;
					$auditor->company_id = $request->company_id;
					$auditor->save();
				}
			
		

		Session::flash( 'message', 'Auditor has been added' );
		return redirect()->back();
	}

	public
	function edit( Request $request ) {
		if(auth()->user()->UserType=='DemoUser'){
			return response(view('403'), 403);
		}
		$user = User::findOrfail( $request->id );
		$roles = Role::all();
		return view( 'users.edit', compact( 'user', 'roles' ) );
	}
	public
	function edit_auditor( Request $request ) {
		if(auth()->user()->UserType=='DemoUser'){
			return response(view('403'), 403);
		}
		$user = User::findOrfail( $request->id );
		$roles = Role::all();
		$companies = Role::findOrFail(6)->users()->has('company')->get();

		return view( 'users.edit_auditor', compact( 'user', 'roles','companies' ) );
	}

	public
	function update( Request $request ) {
		if(auth()->user()->UserType=='DemoUser'){
			return response(view('403'), 403);
		}
		$this->validate( $request, [
			'name' => 'required',
			'email' => 'required',
			//            'dob' => 'required',
			//            'bio' => 'required',
			//            'gender' => 'required',
			//            'country' => 'required',
			//            'state' => 'required',
			//            'city' => 'required',
			//            'address' => 'required',
			//            'postal' => 'required',
			'role' => 'required',
			'pic_file'=>'image|mimes:jpg,jpeg,png,gif'

		], [
			'pic_file.required' => 'Profile picture required',
			'dob.required' => 'Date of Birth required'
		] );

		$user = User::findOrfail( $request->id );

		if ( $request->password ) {
			$user->password = bcrypt( $request->password );
		}
		$user->email = $request->email;
		$user->name = $request->name;
		$user->title = $request->title;
		$requestData = $request->all();
		 if(isset($requestData['role']['0']) && $requestData['role']['0'] ==9)
		  {
		  	$requestData['role']['0'] = 3;
		  	$user->UserType = 'DemoUser';
		  	$this->save_data_demo_db($requestData,$user->id);
		  }
		$user->save();

		$profile = $user->profile;
		if ( $user->profile == null ) {
			$profile = new Profile();
		}
		//dd($request->dob);
		$date = formatDateDb($request->dob);


		if ( $file = $request->file( 'pic_file' ) ) {
			$extension = $file->extension() ? : 'png';
			$destinationPath = public_path() . '/storage/uploads/users/';
			$safeName = str_random( 10 ) . '.' . $extension;
			$file->move( $destinationPath, $safeName );
			//delete old pic if exists
			if ( File::exists( $destinationPath . $user->pic ) ) {
				File::delete( $destinationPath . $user->pic );
			}
			//save new file path into db
			$profile->pic = $safeName;
		}


		$profile->user_id = $user->id;
		$profile->bio = $request->bio;
		$profile->gender = $request->gender;
		$profile->dob = $date;
		$profile->country = $request->country;
		$profile->state = $request->state;
		$profile->city = $request->city;
		$profile->address = $request->address;
		$profile->postal = $request->postal;
		$profile->save();

		if ( $user->roles()->first() != null ) {
			//   $user->roles()->first()->pivot->delete();
			$user->roles()->detach();

		}

		foreach ( $requestData['role'] as $role_new ) {
			$role = Role::find( $role_new );

			// continue;
			if ( !$user->hasRole( $role->name ) ) {

				$user->assignRole( $role->name );
			}

			if ( $role->name == 'SuperAdmin' ) {
				$user->assignRole( 'admin' );
			}

			if ( $role->name == 'CDL Manager Admin' || $role->name == 'CSA Watch Admin' ) {
				$user->assignRole( 'admin' );
			}
			
			if ( $role->name == 'Company' ) {
				$company = $user->company;
				if ( $user->company == null ) {
					$company = new Company();
					$company->user_id = $user->id;
					$company->save();
				}
			}
		}
		Session::flash( 'message', 'User has been updated' );
		return redirect()->back();
	}

	public
	function update_auditor( Request $request ) {
		if(auth()->user()->UserType=='DemoUser'){
			return response(view('403'), 403);
		}
		$this->validate( $request, [
			'name' => 'required',
			'email' => 'required|email|unique:users,email,'.$request->id,
			'role' => 'required',

		], [
			'pic_file.required' => 'Profile picture required',
			'dob.required' => 'Date of Birth required'
		] );

		$user = User::findOrfail( $request->id );

		if ( $request->password ) {
			$user->password = bcrypt( $request->password );
		}
		$user->email = $request->email;
		$user->name = $request->name;
		$user->save();

		$auditor = $user->auditor;
		if ( $user->auditor == null ) {
			$auditor = new Auditor();
			$auditor->user_id = $user->id;
			$auditor->company_id = $request->company_id;
			$auditor->save();
		}else{

			$auditor->user_id = $user->id;
			$auditor->company_id = $request->company_id;
			$auditor->save();
		}
	
		Session::flash( 'message', 'Auditor has been updated' );
		return redirect()->back();
	}

	public
	function delete( $id ) {
		if(auth()->user()->UserType=='DemoUser'){
			return response(view('403'), 403);
		}

		$user = User::findOrfail( $id );
		$user->delete();
		Session::flash( 'message', 'User has been deleted' );
		return back();
	}

	public
	function getDeletedUsers() {
		if(auth()->user()->UserType=='DemoUser'){
			return response(view('403'), 403);
		}
		$users = User::onlyTrashed()->get();
		return view( 'users.deleted', compact( 'users' ) );
	}

	public
	function getDeletedDrivers() {
		// $users = Role::findOrfail(4)->users;
		$users = User::whereHas( 'roles', function ( $query ) {
			$query->where( 'name', 'like', 'driver%' );
		} )->onlyTrashed()->get();
		return view( 'users.deleted', compact( 'users' ) );
	}

	public
	function restoreUser( Request $request ) {
		$user = User::onlyTrashed()->where( 'id', '=', $request->id );
		$user->restore();
		Session::flash( 'message', 'User has been restored' );
		return back();
	}

	public
	function getSettings() {
		$user = auth()->user();

		return view( 'users.account-settings', compact( 'user' ) );
	}

	public
	function saveSettings( Request $request ) {
		$this->validate( $request, [
			'name' => 'required',
			'email' => 'required',
			'pic_file'=>'image|mimes:jpg,jpeg,png,gif'
		] );

		$user = auth()->user();
		//dd($request);
		if ( $request->password ) {
			$user->password = bcrypt( $request->password );
		}
		$user->email = $request->email;
		$user->name = $request->name;
		$user->save();

		$profile = $user->profile;
		if ( $user->profile == null ) {
			$profile = new Profile();
		}



		

		if ( $file = $request->file( 'pic_file' ) ) {
			$extension = $file->extension() ? : 'png';
			$destinationPath = public_path() . '/storage/uploads/users/';
			$safeName = str_random( 10 ) . '.' . $extension;
			$file->move( $destinationPath, $safeName );
			//delete old pic if exists
			if ( File::exists( $destinationPath . $user->pic ) ) {
				File::delete( $destinationPath . $user->pic );
			}
			//save new file path into db
			$profile->pic = $safeName;
		}


		$profile->user_id = $user->id;
		$profile->bio = $request->bio;
		$profile->gender = $request->gender;
		if(!empty($request->dob))
		{
		$date = formatDateDb($request->dob);
		$profile->dob = $date;
		}

		$profile->country = $request->country;
		$profile->state = $request->state;
		$profile->city = $request->city;
		$profile->address = $request->address;
		$profile->postal = $request->postal;
		$profile->save();

		Session::flash( 'message', 'Account has been updated' );
		return redirect()->back();
	}
	 public function destroy($id)
    {
    	if(auth()->user()->UserType=='DemoUser'){
			return response(view('403'), 403);
		}

     
            User::destroy($id);

            return back()->with('flash_message', 'User deleted!');
       

    }
}