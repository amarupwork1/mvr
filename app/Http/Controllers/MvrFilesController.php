<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\MvrFile;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;


class MvrFilesController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */

    public function index(Request $request)
    {
        $model = str_slug('mvrfiles','-');
        if(auth()->user()->permissions()->where('name','=','view-'.$model)->first()!= null) {
         


            if ($request->ajax()) {

                $item = MvrFile::select('*');

                return Datatables::of($item)

             
                 
                 ->addColumn('action', function ($item) {
                    $model = str_slug('mvrfiles','-');
                     $action = " ";
                if(auth()->user()->permissions()->where('name','=','view-'.$model)->first()!= null) {    
                $action.=  ' <a href=" '. url('/mvr-files/' . $item->id) .'"
                                               title="View mvr-files">
                                                <button class="btn btn-info btn-sm">
                                                    <i class="fa fa-eye" aria-hidden="true"></i> View
                                                </button>
                                            </a>';
                }
                if(auth()->user()->permissions()->where('name','=','edit-'.$model)->first()!= null) {
                $action .= '<a href="'. url('/mvr-files/' . $item->id . '/edit').'"
                                               title="Edit mvr-files">
                                                <button class="btn btn-primary btn-sm">
                                                    <i class="fa fa-pencil-alt" aria-hidden="true"> </i> Edit
                                                </button>
                                            </a>';
                }
             


                if(auth()->user()->permissions()->where('name','=','delete-'.$model)->first()!= null) {
               $action .= '<form method="POST" id="form_'.$item->id.'" action="'. url('/mvr-files/' . $item->id ).'" accept-charset="UTF-8" style="display:inline"><input name="_token" type="hidden" value="'.csrf_token().'"><input name="_method" type="hidden" value="DELETE">
                                            <button type="submit" value="form_'.$item->id.'" class="btn btn-warning archive_alert btn-sm" title="Delete mvr-files">
                                            <i class="fa fa-trash" aria-hidden="true"></i> Delete</button>
                                                                                </form> ' ;
                }
                     
                return $action;
            
            })
                 ->rawColumns(['status', 'action'])
                 ->addColumn('status', function ($item) {
                        $status = "";
                        if($item->status == 1)
                             {
                                $status = '<span class="btn waves-effect waves-light btn-success  pt-0 pb-0 pl-4 pr-4 ml-3">Active</span>';
                             }
                             else{
                                $status = '<span class="btn waves-effect waves-light btn-warning  pt-0 pb-0 pl-4 pr-4 ml-3">In Active</span>';
                             }
                            
                            return $status;
                        })



              
                
             



                ->make(true);
            }

            $mvrfiles = [];

            return view('mvr-files.index', compact('mvrfiles'));
        }
        return response(view('403'), 403);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $model = str_slug('mvrfiles','-');
        if(auth()->user()->permissions()->where('name','=','add-'.$model)->first()!= null) {
            return view('mvr-files.create');
        }
        return response(view('403'), 403);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $model = str_slug('mvrfiles','-');
        if(auth()->user()->permissions()->where('name','=','add-'.$model)->first()!= null) {
            $this->validate($request, [
			'name' => 'required'
		]);
            $requestData = $request->all();
            if ( $file = $request->file( 'path' ) ) {
            $extension = $file->extension() ? : 'png';
            $destinationPath = public_path() . '/storage/uploads/mvrfiles/';
            $safeName = str_random( 10 ) . '.' . $extension;
            $file->move( $destinationPath, $safeName );
            $requestData[ 'path' ] = $safeName;
        } 
            MvrFile::create($requestData);
            return redirect('mvr-files')->with('flash_message', 'MvrFile added!');
        }
        return response(view('403'), 403);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $model = str_slug('mvrfiles','-');
        if(auth()->user()->permissions()->where('name','=','view-'.$model)->first()!= null) {
            $mvrfile = MvrFile::findOrFail($id);
            return view('mvr-files.show', compact('mvrfile'));
        }
        return response(view('403'), 403);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $model = str_slug('mvrfiles','-');
        if(auth()->user()->permissions()->where('name','=','edit-'.$model)->first()!= null) {
            $mvrfile = MvrFile::findOrFail($id);
            return view('mvr-files.edit', compact('mvrfile'));
        }
        return response(view('403'), 403);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $model = str_slug('mvrfiles','-');
        if(auth()->user()->permissions()->where('name','=','edit-'.$model)->first()!= null) {
            $this->validate($request, [
			'name' => 'required'
		]);
            $requestData = $request->all();
            
            $mvrfile = MvrFile::findOrFail($id);
            if ( $file = $request->file( 'path' ) ) {
            $extension = $file->extension() ? : 'png';
            $destinationPath = public_path() . '/storage/uploads/mvrfiles/';
            $safeName = str_random( 10 ) . '.' . $extension;
            $file->move( $destinationPath, $safeName );
            $requestData[ 'path' ] = $safeName;
        } 
             $mvrfile->update($requestData);

             return redirect('mvr-files')->with('flash_message', 'MvrFile updated!');
        }
        return response(view('403'), 403);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $model = str_slug('mvrfiles','-');
        if(auth()->user()->permissions()->where('name','=','delete-'.$model)->first()!= null) {
            MvrFile::destroy($id);

            return redirect('mvr-files')->with('flash_message', 'MvrFile deleted!');
        }
        return response(view('403'), 403);

    }
}
