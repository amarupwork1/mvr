<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use File;
use Illuminate\Support\Facades\DB;
use App\Charts\DashboardChart;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Mvr_report;



class DashboardController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
	
	function index(){
			$user = Auth::user();

        $data = [
            'MVR' => Mvr_report::count()
        ];
		return view('dashboard.index',compact('data'));

	}


}
