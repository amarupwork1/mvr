<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Mvr_misc_report;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;


class Mvr_misc_reportsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */

    public function index(Request $request)
    {
        $model = str_slug('mvr_misc_reports','-');
        if(auth()->user()->permissions()->where('name','=','view-'.$model)->first()!= null) {

            if ($request->ajax()) {

                $item = Mvr_misc_report::select('*');

                return Datatables::of($item)

             
                 
                 ->addColumn('action', function ($item) {
                    $model = str_slug('mvr_misc_reports','-');
                     $action = " ";
                if(auth()->user()->permissions()->where('name','=','view-'.$model)->first()!= null) {    
                $action.=  ' <a href=" '. url('/mvr_misc_reports/' . $item->id) .'"
                                               title="View mvr_misc_reports">
                                                <button class="btn btn-info btn-sm">
                                                    <i class="fa fa-eye" aria-hidden="true"></i> View
                                                </button>
                                            </a>';
                }
                if(auth()->user()->permissions()->where('name','=','edit-'.$model)->first()!= null) {
                $action .= '<a href="'. url('/mvr_misc_reports/' . $item->id . '/edit').'"
                                               title="Edit mvr_misc_reports">
                                                <button class="btn btn-primary btn-sm">
                                                    <i class="fa fa-pencil-alt" aria-hidden="true"> </i> Edit
                                                </button>
                                            </a>';
                }
             


                if(auth()->user()->permissions()->where('name','=','delete-'.$model)->first()!= null) {
               $action .= '<form method="POST" id="form_'.$item->id.'" action="'. url('/mvr_misc_reports/' . $item->id ).'" accept-charset="UTF-8" style="display:inline"><input name="_token" type="hidden" value="'.csrf_token().'"><input name="_method" type="hidden" value="DELETE">
                                            <button type="submit" value="form_'.$item->id.'" class="btn btn-danger   btn-sm" title="Delete mvr_misc_reports">
                                            <i class="fa fa-trash" aria-hidden="true"></i> Delete</button>
                                                                                </form> ' ;
                }
                     
                return $action;
            
            })

              
                
             



                ->make(true);
            }

            $mvr_misc_reports = [];
            return view('mvr_misc_reports.index', compact('mvr_misc_reports'));
        }
        return response(view('403'), 403);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $model = str_slug('mvr_misc_reports','-');
        if(auth()->user()->permissions()->where('name','=','add-'.$model)->first()!= null) {
            return view('mvr_misc_reports.create');
        }
        return response(view('403'), 403);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $model = str_slug('mvr_misc_reports','-');
        if(auth()->user()->permissions()->where('name','=','add-'.$model)->first()!= null) {
            
            $requestData = $request->all();
            
            Mvr_misc_report::create($requestData);
            return redirect('mvr_misc_reports')->with('flash_message', 'Mvr_misc_report added!');
        }
        return response(view('403'), 403);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $model = str_slug('mvr_misc_reports','-');
        if(auth()->user()->permissions()->where('name','=','view-'.$model)->first()!= null) {
            $mvr_misc_report = Mvr_misc_report::findOrFail($id);
            return view('mvr_misc_reports.show', compact('mvr_misc_report'));
        }
        return response(view('403'), 403);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $model = str_slug('mvr_misc_reports','-');
        if(auth()->user()->permissions()->where('name','=','edit-'.$model)->first()!= null) {
            $mvr_misc_report = Mvr_misc_report::findOrFail($id);
            return view('mvr_misc_reports.edit', compact('mvr_misc_report'));
        }
        return response(view('403'), 403);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $model = str_slug('mvr_misc_reports','-');
        if(auth()->user()->permissions()->where('name','=','edit-'.$model)->first()!= null) {
            
            $requestData = $request->all();
            
            $mvr_misc_report = Mvr_misc_report::findOrFail($id);
             $mvr_misc_report->update($requestData);

             return redirect('mvr_misc_reports')->with('flash_message', 'Mvr_misc_report updated!');
        }
        return response(view('403'), 403);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $model = str_slug('mvr_misc_reports','-');
        if(auth()->user()->permissions()->where('name','=','delete-'.$model)->first()!= null) {
            Mvr_misc_report::destroy($id);

            return redirect('mvr_misc_reports')->with('flash_message', 'Mvr_misc_report deleted!');
        }
        return response(view('403'), 403);

    }
}
