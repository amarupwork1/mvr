<?php

namespace App\Exports;

use App\Company;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;


class CompanyExport implements FromCollection, WithHeadings, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
	
    public function collection()
    {
        $company =  Company::select('usdot','legal_name','dba_name','zip_code','physical_address','mailing_address','state_mailing','city_mailing','zip_code_mailing','phone','mobile','company_fax','owner_name','owner_phone','owner_email','ein','dot_pi','fields','lmc_url','lmc_username','lmc_password','twc_url','twc_username','twc_password','eld_url','eld_username','eld_password','mw1_url','mw1_username','mw1_password','mw2_url','mw2_username','mw2_password','mw3_url','mw3_username','mw3_password','created_at','updated_at');
        return $company = $company->get();
    }


    public function headings(): array
    {
        return [
            'USDOT',
			'Legal Name',
			'DBA Name',
			'ZIP Code',
			'Physical Address',
			'Mailing Address',
			'State ',
			'City ',
			'ZIP Code ',
			'Phone',
			'Mobile',
			'Company Fax',
			'Owner Name',
			'Owner Phone',
			'Owner Email',
			'EIN',
			'DOT PI',
			'fields',
			'LMC URL',
			'LMC Username',
			'LMC Password',
			'TWC URL',
			'TWC Username',
			'TWC Password',
			'ELD URL',
			'ELD Username',
			'ELD Password',
			'MW1 URL',
			'MW1 Username',
			'MW1 Password',
			'MW2 URL',
			'MW2 Username',
			'MW2 Password',
			'MW3 URL',
			'MW3 Username',
			'MW3 Password',
			'Created Date',
			'Updated Date',
        ];
    }

   
}
