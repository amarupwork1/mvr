<?php

namespace App\Exports;
use Illuminate\Database\Eloquent\Builder;

use App\company;
use App\inspection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithTitle;
use DB;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class DailyInspectionExport implements FromCollection, ShouldAutoSize, WithHeadings,WithTitle
{


	public $type;

    public function __construct($type='cdlmanager.com')
    {
        $this->type = $type;
    }


    /**
    * @return \Illuminate\Support\Collection
    */
     public function collection()
    {
		/*$inspections = inspection::select('mcmx','report','inspection_level','inspection_date','inspection_location','start_end_time','post_crash_inspection','name','license_state','material_desc','dob','license','report_qty','age','waste','meta_verify');
		$inspections = $inspections->whereHas('company.Fmcsa_user', function (Builder $query) {
	    	$query->where('source', 'like', $this->type."%");
		})->whereNull('notified')->orWhere('notified','!=', 1)->get();*/


		$inspections = DB::table('inspections')
            ->join('companies', 'inspections.company_id', '=', 'companies.id')
			->join('fmcsa_users', 'companies.fmcsa_user_id', '=', 'fmcsa_users.id')	
         
            ->select('mcmx', 'companies.legal_name','report','inspection_level','inspection_date','fmcsaPostDate','inspection_location','start_end_time','post_crash_inspection','name','license_state','material_desc','dob','license','report_qty','age','waste','meta_verify')
            ->where('fmcsa_users.source', 'like', "%" . $this->type . "%")
            ->whereNull('notified')->orWhere('notified','!=', 1)
            ->get();
	
		///return  $inspections->count();
		return $inspections;
    }

    public function title(): string
    {
        return 'Inspections';
    }
	public function headings(): array
    {
        return [
            'MCMX',
            'Company',
			'Report',
			'Inspection Level',
			'Inspection Date',
			'FMCSA Post Date',
			'Inspection Location',
			'Start-End Time',
			'Post Crash Inspection',
			'Driver Name',
			'License State',
			'Material Desc',
			'DOB',
			'License #',
			'Report Qty',
			'Age',
			'Waste',
			'Meta Verify',
        ];
    }
}
