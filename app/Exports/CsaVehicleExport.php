<?php

namespace App\Exports;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use carbon\Carbon;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use App\Vehicle;






class CsaVehicleExport implements FromCollection, WithHeadings, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Vehicle::select('inspection_id', 'unit', 'type', 'make', 'state', 'plate', 'equipment_id', 'vin', 'iep_usdot','meta')->get();

    }


    public function headings(): array
    {
        $headings =  [

			'inspection_id', 'unit', 'type', 'make', 'state', 'plate', 'equipment_id', 'vin', 'iep_usdot','meta'
        ];

        $headings = array_map(
		    function($str) {
		        return strtoupper(str_replace('_', ' ', $str));
		    },
		    $headings
		);
		return $headings;

        foreach ($headings as &$str) {
		    $str = ucwords(str_replace('_', ' ', $str));
		}

    }

}
