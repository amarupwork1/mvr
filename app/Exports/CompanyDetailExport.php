<?php

namespace App\Exports;

use App\company;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;


class CompanyDetailExport implements FromCollection, WithMultipleSheets, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
	protected $company_id;
	public function __construct(int $company_id)
    {
        $this->company_id = $company_id;
    }
    public function collection()
    {
		$companyDetails = Company::where('id',0)->get();
        return $companyDetails;
       
    }

	public function sheets(): array
    {
		$sheets = [];	
		$sheets[] = new CompanyInfoExport($this->company_id);
		$sheets[] = new InspectionExport($this->company_id);
		$sheets[] = new ViolationExport($this->company_id);
		return $sheets;
    }
}
