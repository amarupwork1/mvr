<?php

namespace App\Exports;
use App\Fmcsa_user;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;



class FmcsaLoginProblem implements FromCollection,WithHeadings,ShouldAutoSize
{

    public $type;

    public function __construct($type='cdlmanager.com')
    {
        $this->type = $type;
    }



    /**
    * @return \Illuminate\Support\Collection
    */


    public function collection()
    {
        $user =  Fmcsa_user::select('login', 'password', 'dot_number', 'company_name', 'inception_date', 'source', 'last_success');
         $user = $user->where('login_problem',1);

         return $user = $user->where('source',$this->type)->get();
    }


    public function headings(): array
    {
        $headings =  [
            'login', 'password', 'usdot', 'company_name','inception_date', 'source', 'last_success'
        ];

		return  array_map(function ($headings) {  return str_replace("_", " ",strtoupper($headings)); }, $headings);
    }


}
