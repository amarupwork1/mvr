<?php

namespace App\Exports;

use App\Violation;
use App\inspection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;


class ViolationExport implements FromCollection, WithTitle, WithHeadings, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
	protected $company_id;
	public function __construct(int $company_id)
    {
        $this->company_id = $company_id;
    }
	
    public function collection()
    {		
		$inspections = inspection::where('company_id',$this->company_id)->get();
		$inspection_ids = $inspections->pluck('id')->toArray();
		return $violations = Violation::select('unit','violation_code','oos','violations_category','violations_basics_category','violations_discovered','state_citation_number','state_citation_result','certify','post_crash','AccidentRelated','OriginalViolationSeverityWeight','iep','notified','status','created_at','updated_at')->whereIn('inspection_id',$inspection_ids)->get();	
    }
	public function title(): string
    {
        return 'Violations';
    }
	public function headings(): array
    {
        return [
			'Unit',
			'Violation Code',
			'OOS',
			'Violations Category',
			'violations_basics_category',
			'Violations Discovered',
			'State Citation Number',
			'State Citation Result',
			'Certify',
			'Post Crash',
			'AccidentRelated',
			'OriginalViolationSeverityWeight',
			'IEP',
			'Notified',
			'Status',
			'created_at',
			'updated_at',
        ];
    }
}
