<?php

namespace App\Exports;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use carbon\Carbon;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use App\Driver;






class driverExport implements FromCollection, WithHeadings, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Driver::join('users', 'drivers.company_id', '=', 'users.id')->select(
								"first_name",
								"last_name",
								"users.name",
								"drivers.email",
								"mobile_number",
								"DOB",
								"license_state",
								"license_number",
								"license_exp",
								"hire_date",
								"termination_date",
								"reason_for_termination",
								"employment_type",
								"street_address",
								"city",
								"state",
								"zipcode",
								"sec_street_address",
								"city_mailing",
								"state_mailing",
								"zipcode_mailing",
								"eld_login_username",
								"eld_login_password",
								"driver_points",
								"hiring_drug_screen_date",
								"drug_screen_availability",
								"from_mro_date",
								"mro_availability",
								"hiring_mvr_date",
								"mvr_availability",
								"first_trip_date",
								"first_trip_time",
								"first_trip_availability",
								"current_dot_physical_date",
								"mec_exp_date",
								"dot_medical_examine_name",
								"medical_examine_nation_registry_num",
								"medical_examiners_license_state",
								"most_recent_date",
								"date_of_review_of_mvr",
								"first_and_last_name_of_reviewer",
								"date_of_mec_exp_mvr",
								"date_recent_certificate_of_violation",
								"date_of_review_certi_vio",
								"name_of_reviewer_of_cert",
								"comm_driv_licen_exp",
								"hazmat_endorsement",
								"hazmat_end_exp_date",
								"double_endorsement",
								"double_end_exp_date",
								"tank_endorse",
								"tax_endor_expi_date",
								"passenger_endor",
								"passenger_endor_exp_date",
								"endorse_other",
								"corrective_lenses_restr",
								"hearing_device_restrict",
								"medical_waiver_req",
								"restriction_other",
								"twic_qualified",
								"twic_expiration",
								"fast_qualified",
								"fast_exp",
								"passport",
								"passport_exp",
								"driver_qualification_file_received_by_cdlm",
								"initial_driver_qualification_file_reviewed_by_cdlm",
								"name_of_initial_driver_qualification_file_reviewer",
								"date_of_initial_driver_qualification_file_review",
								"driver_application_in_file",
								"inquiry_to_previous_employers_in_file",
								"hiring_motor_vehicle_record_in_file",
								"commercial_drivers_license_front_and_back_in_file",
								"road_test_in_file",
								"certification_of_road_test_in_file",
								"motor_vehicle_record_ran_within_last_12_months",
								"certificate_of_violations_signed_by_driver_within_last_12_months",
								"dot_medical_examiners_certificate_mec",
								"medical_waiver",
								"national_registry_of_certified_medical_examiners_verified",
								"dq_file_issues_comments"
								)->orderBy('users.name','asc')->orderBy('first_name','asc')->get();

    }


    public function headings(): array
    {
        $headings =  [

				"first_name",
				"last_name",
				"company_name",
				"email",
				"mobile_number",
				"DOB",
				"license_state",
				"license_number",
				"license_exp",
				"hire_date",
				"termination_date",
				"reason_for_termination",
				"employment_type",
				"street_address",
				"city",
				"state",
				"zipcode",
				"sec_street_address",
				"city_mailing",
				"state_mailing",
				"zipcode_mailing",
				"eld_login_username",
				"eld_login_password",
				"driver_points",
				"hiring_drug_screen_date",
				"drug_screen_availability",
				"from_mro_date",
				"mro_availability",
				"hiring_mvr_date",
				"mvr_availability",
				"first_trip_date",
				"first_trip_time",
				"first_trip_availability",
				"current_dot_physical_date",
				"mec_exp_date",
				"dot_medical_examine_name",
				"medical_examine_nation_registry_num",
				"medical_examiners_license_state",
				"most_recent_date",
				"date_of_review_of_mvr",
				"first_and_last_name_of_reviewer",
				"date_of_mec_exp_mvr",
				"date_recent_certificate_of_violation",
				"date_of_review_certi_vio",
				"name_of_reviewer_of_cert",
				"comm_driv_licen_exp",
				"hazmat_endorsement",
				"hazmat_end_exp_date",
				"double_endorsement",
				"double_end_exp_date",
				"tank_endorse",
				"tax_endor_expi_date",
				"passenger_endor",
				"passenger_endor_exp_date",
				"endorse_other",
				"corrective_lenses_restr",
				"hearing_device_restrict",
				"medical_waiver_req",
				"restriction_other",
				"twic_qualified",
				"twic_expiration",
				"fast_qualified",
				"fast_exp",
				"passport",
				"passport_exp",
				"driver_qualification_file_received_by_cdlm",
				"initial_driver_qualification_file_reviewed_by_cdlm",
				"name_of_initial_driver_qualification_file_reviewer",
				"date_of_initial_driver_qualification_file_review",
				"driver_application_in_file",
				"inquiry_to_previous_employers_in_file",
				"hiring_motor_vehicle_record_in_file",
				"commercial_drivers_license_front_and_back_in_file",
				"road_test_in_file",
				"certification_of_road_test_in_file",
				"motor_vehicle_record_ran_within_last_12_months",
				"certificate_of_violations_signed_by_driver_within_last_12_months",
				"dot_medical_examiners_certificate_mec",
				"medical_waiver",
				"national_registry_of_certified_medical_examiners_verified",
				"dq_file_issues_comments",
				        ];

        $headings = array_map(
		    function($str) {
		        return strtoupper(str_replace('_', ' ', $str));
		    },
		    $headings
		);
		return $headings;

        foreach ($headings as &$str) {
		    $str = ucwords(str_replace('_', ' ', $str));
		}

    }

}
