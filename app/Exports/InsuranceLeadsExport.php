<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use App\InsuranceInformation;
use Maatwebsite\Excel\Concerns\WithHeadings;
use carbon\Carbon;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class InsuranceLeadsExport implements FromCollection, WithHeadings, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
          return InsuranceInformation::select("id","dot_number","docket_no","blanket_company","property","passenger","household_goods","private","enterprise","common_application_pending","bipd_insurance_required","cargo_insurance_required","cargo_insurance_on_file","undeliverable_mail","contract_application_pending","mail_telephone_and_fax","common_authority_status","business_tel_and_fax","broker_authority_status","mail_address","bussiness_address","bond_insurance_required","doing_business_as_name","boc3_yes","contract_authority_status","legal_name","bond_insurance_on_file","bipd_insurance_on_file","broker_application_pending")->get();
    }

     public function headings(): array
    {
        $headings = 
           ["id","dot_number","docket_no","blanket_company","property","passenger","household_goods","private","enterprise","common_application_pending","bipd_insurance_required","cargo_insurance_required","cargo_insurance_on_file","undeliverable_mail","contract_application_pending","mail_telephone_and_fax","common_authority_status","business_tel_and_fax","broker_authority_status","mail_address","bussiness_address","bond_insurance_required","doing_business_as_name","boc3_yes","contract_authority_status","legal_name","bond_insurance_on_file","bipd_insurance_on_file","broker_application_pending"];


            $headings = array_map(
		    function($str) {
		        return strtoupper(str_replace('_', ' ', $str));
		    },
		    $headings
		);
		return $headings;

    }
}
