<?php

namespace App\Exports;
use Illuminate\Database\Eloquent\Builder;

use App\company;
use App\inspection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithTitle;
use DB;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class DailyViolationExport implements FromCollection, ShouldAutoSize, WithHeadings,WithTitle
{


	public $type;

    public function __construct($type='cdlmanager.com')
    {
        $this->type = $type;
    }


    /**
    * @return \Illuminate\Support\Collection
    */
     public function collection()
    {
		/*$inspections = inspection::select('mcmx','report','inspection_level','inspection_date','inspection_location','start_end_time','post_crash_inspection','name','license_state','material_desc','dob','license','report_qty','age','waste','meta_verify');
		$inspections = $inspections->whereHas('company.Fmcsa_user', function (Builder $query) {
	    	$query->where('source', 'like', $this->type."%");
		})->whereNull('notified')->orWhere('notified','!=', 1)->get();*/


		$violations = DB::table('violations')

            ->join('inspections', 'inspections.id', '=', 'violations.inspection_id')
            ->join('companies', 'inspections.company_id', '=', 'companies.id')
			->join('fmcsa_users', 'companies.fmcsa_user_id', '=', 'fmcsa_users.id')	
         
            ->select('inspections.report','companies.legal_name','unit', 'violation_code', 'oos', 'violations_category', 'violations_discovered', 'state_citation_number', 'state_citation_result', 'certify', 'post_crash', 'iep', 'AccidentRelated')
            ->where('fmcsa_users.source', 'like', "%" . $this->type . "%")
            ->whereNull('violations.notified')->orWhere('violations.notified','!=', 1)
            ->get();
	
		///return  $inspections->count();
		return $violations;
    }

    public function title(): string
    {
        return 'Violations';
    }
	public function headings(): array
    {
        $headings =  ['Report #',
        'Company Name',
        'Unit',
         'Violation_code',
         'Oos',
         'Violations_category',
         'Violations_discovered',
         'State_citation_number',
         'State_citation_result',
         'certify',
         'post_crash',
         'iep',
         'AccidentRelated'
        ];
           $headings = array_map(
		    function($str) {
		        return strtoupper(str_replace('_', ' ', $str));
		    },
		    $headings
		);
		return $headings;

    }
}
