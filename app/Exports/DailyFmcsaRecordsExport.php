<?php

namespace App\Exports;

use App\company;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;


class DailyFmcsaRecordsExport implements FromCollection, WithMultipleSheets, ShouldAutoSize
{
    

    public $type;

    public function __construct($type='cdlmanager.com')
    {
        $this->type = $type;
    }


    public function collection()
    {
		//$companyDetails = Company::where('id',0)->get();
        //return $companyDetails;
       
    }

	public function sheets(): array
    {
		$sheets = [];	
		$sheets[] = new DailyInspectionExport($this->type);
		
		$sheets[] = new DailyViolationExport($this->type);

        $sheets[] = new DailyCrashExport($this->type);
		return $sheets;
    }
}
