<?php

namespace App\Exports;
use Illuminate\Database\Eloquent\Builder;

use App\company;
use App\inspection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithTitle;
use DB;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class DailyCrashExport implements FromCollection, ShouldAutoSize, WithHeadings,WithTitle
{


	public $type;

    public function __construct($type='cdlmanager.com')
    {
        $this->type = $type;
    }


    /**
    * @return \Illuminate\Support\Collection
    */
     public function collection()
    {
		/*$crashes = inspection::select('mcmx','report','inspection_level','inspection_date','inspection_location','start_end_time','post_crash_inspection','name','license_state','material_desc','dob','license','report_qty','age','waste','meta_verify');
		$crashes = $crashes->whereHas('company.Fmcsa_user', function (Builder $query) {
	    	$query->where('source', 'like', $this->type."%");
		})->whereNull('notified')->orWhere('notified','!=', 1)->get();*/


		$crashes = DB::table('crashes')

            ->join('companies', 'crashes.company_id', '=', 'companies.id')
			->join('fmcsa_users', 'companies.fmcsa_user_id', '=', 'fmcsa_users.id')	
         
            ->select('companies.legal_name','mcmx', 'number_of_fatalities', 'injuries', 'traffic_level', 'weather', 'tow_away', 'vehicles_in_crash', 'access_control', 'light_level', 'surface_condition', 'date_time', 'city_state', 'name', 'citation_issued', 'location', 'county', 'dob', 'license', 'age', 'reporting_state', 'reporting_sequence', 'vin', 'hm_placards', 'reporting_agency', 'state_recordable', 'plate', 'release_of_cargo', 'officer_badge', 'federally_recordable', 'license_state', 'body_type', 'report', 'configuration', 'gvw', 'gvw_range','CrashTotalSeverityWeight')
            ->where('fmcsa_users.source', 'like', "%" . $this->type . "%")
            ->whereNull('notified')->orWhere('notified','!=', 1)
            ->get();
	
		///return  $crashes->count();
		return $crashes;
    }

    public function title(): string
    {
        return 'Crashes';
    }
	public function headings(): array
    {
        $headings =  [
             'company Name',
             'mcmx',
             'number_of_fatalities',
             'injuries',
             'traffic_level',
             'weather',
             'tow_away',
             'vehicles_in_crash',
             'access_control',
             'light_level',
             'surface_condition',
             'date_time',
             'city_state',
             'name',
             'citation_issued',
             'location',
             'county',
             'dob',
             'license',
             'age',
             'reporting_state',
             'reporting_sequence',
             'vin',
             'hm_placards',
             'reporting_agency',
             'state_recordable',
             'plate',
             'release_of_cargo',
             'officer_badge',
             'federally_recordable',
             'license_state',
             'body_type',
             'report',
             'configuration',
             'gvw',
             'gvw_range',
             'CrashTotalSeverityWeight'
        ];


          $headings = array_map(
		    function($str) {
		        return strtoupper(str_replace('_', ' ', $str));
		    },
		    $headings
		);
		return $headings;
    }
}
