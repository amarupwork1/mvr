<?php

namespace App\Exports;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use carbon\Carbon;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use App\Vehicle_field;






class VehicleExport implements FromCollection, WithHeadings, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Vehicle_field::join('companies', 'companies.usdot', '=', 'vehicle_fields.owner_dot')->select("companies.legal_name","owner_dot","operating_carrier_dot","owner_type","unit_number","license_state","license_number","year","make","vin","equipment_type","tire_size","vehicle_weight","gvwr","annual_inspection_date","pm_cycle_last_performed","pm_cycle_next_due","last_pm_mileage","next_pm_mileage","last_pm_date","pm_cycle_days","next_pm_date")->get();

    }


    public function headings(): array
    {
        $headings =  [

			"company_name",
			"owner_dot",
			"operating_carrier_dot",
			"owner_type",
			"unit_number",
			"license_state",
			"license_number",
			"year",
			"make",
			"vin",
			"equipment_type",
			"tire_size",
			"vehicle_weight",
			"gvwr",
			"annual_inspection_date",
			"pm_cycle_last_performed",
			"pm_cycle_next_due",
			"last_pm_mileage",
			"next_pm_mileage",
			"last_pm_date",
			"pm_cycle_days",
			"next_pm_date"
        ];

        $headings = array_map(
		    function($str) {
		        return strtoupper(str_replace('_', ' ', $str));
		    },
		    $headings
		);
		return $headings;

        foreach ($headings as &$str) {
		    $str = ucwords(str_replace('_', ' ', $str));
		}

    }

}
