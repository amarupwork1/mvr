<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class UsersPasswordExport implements FromArray,WithHeadings, ShouldAutoSize
{

	public $users;

	public function __construct($users=null)
    {
       if(is_array($users))
       {
       	//$users = collect($users);

       }

       $this->users = $users;
    }



    /**
    * @return \Illuminate\Support\array
    */
    public function array(): array
    {

       //dd($this->users);
       return $this->users;
    }

    public function headings(): array
    {
        $headings =  [

        	'NAME',
        	'EMAIL',
        	'PASSWORD',

        ];

        return $headings;

     }
}
