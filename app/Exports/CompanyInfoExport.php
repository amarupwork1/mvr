<?php

namespace App\Exports;

use App\company;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Events\AfterSheet;
use Illuminate\Support\ServiceProvider;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;


class CompanyInfoExport implements FromCollection, WithHeadings, WithTitle, ShouldAutoSize
{
 
	protected $company_id;
	public function __construct(int $company_id)
    {
        $this->company_id = $company_id;
    }
		
    public function collection()
    {
		$companyDetails = Company::where('id',0)->get();
        return $companyDetails;
       
    }
	public function headings(): array
    {
		$companyDetails = Company::where('id',$this->company_id)->get();		
		$other_contacts = array();
		if(isset($companyDetails[0]->fields) && $companyDetails[0]->fields != ''){
			$other_contacts = json_decode($companyDetails[0]->fields);
			
		}		
        $headings =  [
           ['USDOT', $companyDetails[0]->usdot],
           ['Legal Name', $companyDetails[0]->legal_name],         
           ['DBA Name', $companyDetails[0]->legal_name],         
           ['zip_code', $companyDetails[0]->zip_code],         
           ['Physical Address', $companyDetails[0]->physical_address],         
           ['Mailing Address', $companyDetails[0]->mailing_address],        
           ['State', $companyDetails[0]->state_mailing],      
           ['City', $companyDetails[0]->city_mailing],    
           ['Zip Code', $companyDetails[0]->zip_code_mailing],   
           ['Phone', $companyDetails[0]->phone],   
           ['Mobile', $companyDetails[0]->mobile],   
           ['Fax', $companyDetails[0]->company_fax],   
           ['Owner Name', $companyDetails[0]->owner_name],   
           ['Owner Phone', $companyDetails[0]->owner_phone],   
           ['Owner Email', $companyDetails[0]->owner_email],  
           ['Ein', $companyDetails[0]->ein],  
           ['DOT PI', $companyDetails[0]->dot_pi], 		   
           ['Other Contact Name', (@$other_contacts->name[0])?$other_contacts->name[0]:''],
           ['Other Contact Phone', (@$other_contacts->phone[0])?$other_contacts->phone[0]:''],
           ['Other Contact Email', (@$other_contacts->email[0])?$other_contacts->email[0]:''],		   
           ['License monitoring Url', $companyDetails[0]->lmc_url],
           ['License monitoring Username', $companyDetails[0]->lmc_username],
           ['Training Website Url', $companyDetails[0]->twc_url],
           ['Training Website Username', $companyDetails[0]->twc_username],
           ['ELD Website Url', $companyDetails[0]->eld_url],
           ['ELD Website Username', $companyDetails[0]->eld_username],
           ['Misc Website Url', $companyDetails[0]->mw1_url],
           ['Misc Website Username', $companyDetails[0]->mw1_username],
           ['Misc Website 2 Url', $companyDetails[0]->mw2_url],
           ['Misc Website 2 Username', $companyDetails[0]->mw2_username],
           ['Misc Website 3 Url', $companyDetails[0]->mw3_url],
           ['Misc Website 3 Username', $companyDetails[0]->mw3_username],
           //['created at', $companyDetails[0]->created_at],
           //['updated_at', $companyDetails[0]->updated_at],
        ];
        return $headings;
          $headings = array_map(
        function($str) {
            return strtoupper(str_replace('_', ' ', $str[0]));
        },
        $headings
    );
          return $headings;
    }
	public function title(): string
    {
        return 'Company Details';
    }
	 
	
	
}


