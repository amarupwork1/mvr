<?php

namespace App\Exports;

use App\Lead;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use carbon\Carbon;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class LeadsExport implements FromCollection, WithHeadings, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Lead::select('usdot','entity_type','operating_status','out_of_service_date','legal_name','DBA_name','physical_address','phone','mailing_address','state_carrier_ID_number','MCMXFF','DUNS_number','power_units','drivers','MCS150_from_date','MCS150_mileage_year','operation_classification','carrier_operation','cargo_carried','inspections_vehicle','inspections_iep','inspections_hazmat','inspections_driver','out_of_service_vehicle','out_of_service_driver','out_of_service_hazmat','out_of_service_iep','out_of_service_percent_vehicle','out_of_service_percent_driver','out_of_service_percent_hazmat','out_of_service_percent_iep','nat_average_vehicle','nat_average_driver','nat_average_hazmat','nat_average_iep','rating_date','review_date','rating','type','date','date_info','notified','status_color','review_date_changes')->where("review_date","<",Carbon::now())->where("review_date",">",Carbon::now()->subDay(30))->orderBy('review_date','DESC')->get();
         
    }


    public function headings(): array
    {
        $headings =  [
        	
			'usdot',
			'entity_type',
			'operating_status',
			'out_of_service_date',
			'legal_name',
			'DBA_name',
			'physical_address',
			'phone',
			'mailing_address',
			'state_carrier_ID_number',
			'MCMXFF',
			'DUNS_number',
			'power_units',
			'drivers',
			'MCS150_from_date',
			'MCS150_mileage_year',
			'operation_classification',
			'carrier_operation',
			'cargo_carried',
			'inspections_vehicle',
			'inspections_iep',
			'inspections_hazmat',
			'inspections_driver',
			'out_of_service_vehicle',
			'out_of_service_driver',
			'out_of_service_hazmat',
			'out_of_service_iep',
			'out_of_service_percent_vehicle',
			'out_of_service_percent_driver',
			'out_of_service_percent_hazmat',
			'out_of_service_percent_iep',
			'nat_average_vehicle',
			'nat_average_driver',
			'nat_average_hazmat',
			'nat_average_iep',
			'rating_date',
			'review_date',
			'rating',
			'type',
			'date',
			'date_info',
			'notified',
			'status_color',
			'review_date_changes',
        ];

        $headings = array_map(
		    function($str) {
		        return strtoupper(str_replace('_', ' ', $str));
		    },
		    $headings
		);
		return $headings;

        foreach ($headings as &$str) {
		    $str = ucwords(str_replace('_', ' ', $str));
		}

    }

}
