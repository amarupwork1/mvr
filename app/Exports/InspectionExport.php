<?php

namespace App\Exports;
use App\company;
use App\inspection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;


class InspectionExport implements FromCollection, WithTitle, WithHeadings, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
	protected $company_id;
	public function __construct(int $company_id)
    {
        $this->company_id = $company_id;
    }
    public function collection()
    {
		$inspections = inspection::select('mcmx','report','inspection_level','inspection_date','inspection_location','start_end_time','post_crash_inspection','name','license_state','material_desc','dob','license','report_qty','age','waste','notified','status','TotalViolSeverityWeight','UnsafeDrivingInspTotalViolSeverityWeight','HOSCompInspTotalViolSeverityWeight','VehMaintInspTotalViolSeverityWeight','DrivFitInspTotalViolSeverityWeight','ContSubInspTotalViolSeverityWeight','HazMatCompInspViolTotalSeverityWeight','created_at','updated_at','meta_verify')->where('company_id',$this->company_id)->get();		
		return $inspections;
    }
	public function title(): string
    {
        return 'Inspections';
    }
	public function headings(): array
    {
        return [
            'MCMX',
			'Report',
			'Inspection Level',
			'Inspection Date',
			'Inspection Location',
			'Start-End Time',
			'Post Crash Inspection',
			'Name',
			'License State',
			'Material Desc',
			'DOB',
			'License #',
			'Report Qty',
			'Age',
			'Waste',
			'Notified',
			'Status',
			'TotalViolSeverityWeight',
			'UnsafeDrivingInspTotalViolSeverityWeight',
			'HOSCompInspTotalViolSeverityWeight',
			'VehMaintInspTotalViolSeverityWeight',
			'DrivFitInspTotalViolSeverityWeight',
			'ContSubInspTotalViolSeverityWeight',
			'HazMatCompInspViolTotalSeverityWeight',
			'created_at',
			'updated_at',
			'meta_verify',
        ];
    }
}
