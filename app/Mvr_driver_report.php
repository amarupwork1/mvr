<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mvr_driver_report extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'mvr_driver_reports';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [];

     public function report()
    {
        return $this->belongsTo('App\Mvr_report');
    }

    
}
