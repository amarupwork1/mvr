<?php

namespace App\Console;

use App\Console\Commands\RefreshSite;
use Appzcoder\CrudGenerator\Commands\CrudCommand;
use Appzcoder\CrudGenerator\Commands\CrudControllerCommand;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        CrudCommand::class,
        CrudControllerCommand::class,
        RefreshSite::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        //$schedule->command(RefreshSite::class)
         //   ->daily();

            $schedule->call('App\Http\Controllers\CompanyController@CompaniesToUsers')
                ->hourly();
            $schedule->call('App\Http\Controllers\Fmcsa_userController@getBrokenLogin')->dailyAt('10:00')->timezone('America/Chicago');
            $schedule->call('App\Http\Controllers\ViolationController@reindex')->daily();
            $schedule->call('App\Http\Controllers\CrashController@reindex')->daily();
            $schedule->command('queue:work --tries=1')->hourly();
  
            $schedule->call('App\Http\Controllers\CronController@DriverDocExpiritionNotification')->daily();
            $schedule->call('App\Http\Controllers\CronController@InspectionNotification')->twiceDaily(7, 23)->timezone('America/Chicago');
           // $schedule->call('App\Http\Controllers\CronController@ViolationNotification')->dailyAt('10:15')->timezone('America/Chicago');
            // for alerts
           // $schedule->call('App\Http\Controllers\AlertController@getDriverAlerts')->daily();




    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
