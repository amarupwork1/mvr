<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;
use App\Company;
use DB;

class DriverRanking extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [
        'id' => null,
    ];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
			
         $company = Company::findOrFail($this->config['id']);
		 if(!empty($this->config['date'])){
			 $inspections = $company->inspection()->where('inspection_date','<=',$this->config['date'])->groupBy('inspections.license')->get();   
		 }else{
         	$inspections = $company->inspection()->groupBy('inspections.license')->get(); 
		 }
        return view('widgets.drivers.report', [
            'config' => $this->config['id'],
            'drivers' => $inspections
        ]);
    }
}
