<?php

namespace App\Widgets;

class PermitionHtml 
{
   
   public function html($url=null,$name=null,$class=null,$id=null)
   {
    
    switch ($name) {
        case 'View':
            
            $html = ' <a href=" '. url($url) .'"
                                               title="View">
                                                <button class="btn btn-info btn-sm">
                                                    <i class="fa fa-eye" aria-hidden="true"></i> View
                                                </button>
                                            </a>';
            break;

        case 'Edit':

            $html = '<a href="'. url($url).'"
                                               title="Edit ">
                                                <button class="btn btn-primary btn-sm">
                                                    <i class="fa fa-pencil-alt" aria-hidden="true"> </i> Edit
                                                </button>
                                            </a>';
            break;
        case 'Delete':

            !empty($class) ? $class : 'danger';

            $html  = '<form method="POST" id="form_'.$id.'" action="'. url($url).'" accept-charset="UTF-8" style="display:inline"><input name="_token" type="hidden" value="'.csrf_token().'"><input name="_method" type="hidden" value="DELETE">
                                            <button type="submit" value="form_'.$id.'" class="btn btn-'.$class.' btn-sm" title="Delete">
                                            <i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                                                                                </form> ' ;
            break;
        
        default:

            $html =  ' <a href=" '. url($url) .'"
                                               title="View">
                                                <button class="btn btn-2 btn-sm">
                                                    <i class="fa fa-'.$class.'" aria-hidden="true"></i> '.$name.'
                                                </button>
                                            </a>';
            break;
    }

    return $html;
   }
}
