

$("#newddis").bootstrapValidator({
    fields: {
        company_id:{
    validators:{
        notEmpty: {
            message: 'This is required'
        }
    }
},
driver_id:{
    validators:{
        notEmpty: {
            message: 'This is required'
        }
    }
},
logging_policy_disciplinary:{
    validators:{
        notEmpty: {
            message: 'This is required'
        }
    }
},
logging_date_placed_current_phase:{
    validators:{
        notEmpty: {
            message: 'This is required'
        }
    }
},
logging_date_current_phase_expires:{
    validators:{
        notEmpty: {
            message: 'This is required'
        }
    }
},
unsafe_driving_policy:{
    validators:{
        notEmpty: {
            message: 'This is required'
        }
    }
},
unsafe_driving_date_placed_current_phase:{
    validators:{
        notEmpty: {
            message: 'This is required'
        }
    }
},
unsafe_driving_date_current_expires:{
    validators:{
        notEmpty: {
            message: 'This is required'
        }
    }
},
preventive_maintenance_policy_disciplinary:{
    validators:{
        notEmpty: {
            message: 'This is required'
        }
    }
},
preventive_maintenance_date_placed:{
    validators:{
        notEmpty: {
            message: 'This is required'
        }
    }
},
preventive_maintenance_date_phase_expired:{
    validators:{
        notEmpty: {
            message: 'This is required'
        }
    }
},
accident_incident_policy_disciplinary:{
    validators:{
        notEmpty: {
            message: 'This is required'
        }
    }
},
accident_incident_date_placed:{
    validators:{
        notEmpty: {
            message: 'This is required'
        }
    }
},
accident_incident_date_phase_expired:{
    validators:{
        notEmpty: {
            message: 'This is required'
        }
    }
},
daily_vehicle_inspection_policy_disciplinary:{
    validators:{
        notEmpty: {
            message: 'This is required'
        }
    }
},
daily_vehicle_inspection_date_placed:{
    validators:{
        notEmpty: {
            message: 'This is required'
        }
    }
},
daily_vehicle_inspection_date_phase_expired:{
    validators:{
        notEmpty: {
            message: 'This is required'
        }
    }
},
road_side_inspection_policy_disciplinary:{
    validators:{
        notEmpty: {
            message: 'This is required'
        }
    }
},
road_side_inspection_date_placed:{
    validators:{
        notEmpty: {
            message: 'This is required'
        }
    }
},
road_side_inspection_date_phase_expired:{
    validators:{
        notEmpty: {
            message: 'This is required'
        }
    }
},
vehicle_defects_inspection_policy_disciplinary:{
    validators:{
        notEmpty: {
            message: 'This is required'
        }
    }
},
vehicle_defects_inspection_date_placed:{
    validators:{
        notEmpty: {
            message: 'This is required'
        }
    }
},
vehicle_defects_inspection_date_phase_expired:{
    validators:{
        notEmpty: {
            message: 'This is required'
        }
    }
},
    }
});


$('#rootwizard').bootstrapWizard({
    'tabClass': 'nav nav-pills',
    'onNext': function(tab, navigation, index) {
        var $validator = $('#companyForm').data('bootstrapValidator').validate();
        return $validator.isValid();
    },
    onTabClick: function(tab, navigation, index) {
        return false;
    },
    onTabShow: function(tab, navigation, index) {
        var $total = navigation.find('li').length;
        var $current = index + 1;

        // If it's the last tab then hide the last button and show the finish instead
        if ($current >= $total) {
            $('#rootwizard').find('.pager .next').hide();
            $('#rootwizard').find('.pager .finish').show();
            $('#rootwizard').find('.pager .finish').removeClass('disabled');
        } else {
            $('#rootwizard').find('.pager .next').show();
            $('#rootwizard').find('.pager .finish').hide();
        }
    }});


$('#rootwizard .finish').click(function () {
    var $validator = $('#companyForm').data('bootstrapValidator').validate();
    if ($validator.isValid()) {
        document.getElementById("companyForm").submit();
    }

});
$('#activate').on('ifChanged', function(event){
    $('#companyForm').bootstrapValidator('revalidateField', $('#activate'));
});