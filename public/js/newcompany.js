// bootstrap wizard//
$("#gender, #gender1").select2({
    theme:"bootstrap",
    placeholder:"",
    width: '100%'
});
$('input[type="checkbox"].custom-checkbox').iCheck({
    checkboxClass: 'icheckbox_minimal-blue',
    increaseArea: '20%'
});

$("#companyForm").bootstrapValidator({
    fields: {
        usdot: {
            validators: {
                notEmpty: {
                    message: 'US DOT is required'
                },
                  stringLength: {
                        min: 6,
                        message: 'US DOT must be less than 6 digits'
                    },
                
            },
        }, /*dba_name: {
            validators: {
                notEmpty: {
                    message: 'The DBA name is required'
                }
            }
        }, logo: {
            validators: {
                notEmpty: {
                    message: 'Company logo is required'
                },
                file: {
                        
                        type: 'image/gif,image/jpeg,image/png,image/svg+xml,image/webp',
                        message: 'The selected file is not valid'
                    }
            }
        }, state: {
            validators: {
                notEmpty: {
                    message: 'The state is required'
                }
            }
        },*/company_name: {
            validators: {
                notEmpty: {
                    message: 'Company name is required'
                }
            }
        },company_email: {
            validators: {
                notEmpty: {
                    message: 'Company email is required'
                }, emailAddress: {
                        message: 'The value is not a valid email address'
                    }
            }
        },/*city: {
            validators: {
                notEmpty: {
                    message: 'City is required'
                }
            }
        },zip_code: {
            validators: {

                    zipCode: {
                        country: 'US',
                        message: 'The value is not valid US postcode'
                    },
                notEmpty: {
                    message: 'Zip code is required'
                }
            }
        },physical_address: {
            validators: {
                notEmpty: {
                    message: 'Address is required'
                }
            }
        },phone: {
            validators: {
                phone: {
                        country: 'US',
                        message: 'Please enter a valid phone number'
                    },
                notEmpty: {
                    message: 'Phone is required'
                }
            }
        },mobile55: {
            validators: {
                notEmpty: {
                    message: 'Mobile is required'
                }
            }
        },company_fax55: {
            validators: {
                notEmpty: {
                    message: 'Company fax is required'
                }
            }
        },owner_name: {
            validators: {
                notEmpty: {
                    message: 'Owner name is required'
                }
            }
        },ein: {
            validators: {
                notEmpty: {
                    message: 'EIN is required'
                }
            }
        },owner_email: {
            validators: {
                notEmpty: {
                    message: 'Owner email is required'
                }, emailAddress: {
                        message: 'The value is not a valid email address'
                    }
            }
        },
owner_phone: {
            validators: {
                phone: {
                        country: 'US',
                        message: 'Please enter a valid phone number'
                    },
                notEmpty: {
                    message: 'Phone is required'
                }
            }
        }
        ,physical_address: {
            validators: {
                notEmpty: {
                    message: 'Address is required'
                }
            }
        },*/
        password: {
            validators: {
                different: {
                    field: 'first_name,last_name',
                    message: 'Password should not match first or last name'
                }
            }
        },
        password_confirmation: {
            validators: {
                identical: {
                    field: 'password'
                },
                different: {
                    field: 'first_name,last_name',
                    message: 'Confirm Password should match with password'
                }
            }
        },
        email: {
            validators: {
                notEmpty: {
                    message: 'The email address is required'
                },
                emailAddress: {
                    message: 'The input is not a valid email address'
                }
            }
        },
        activate: {
            validators: {
                notEmpty: {
                    message: 'Please check the checkbox to activate'
                }
            }
        },
        group: {
            validators:{
                notEmpty:{
                    message: 'You must select a group'
                }
            }
        }
    }
});

$('#rootwizard').bootstrapWizard({
    'tabClass': 'nav nav-pills',
    'onNext': function(tab, navigation, index) {
        
        var $validator = $('#companyForm').data('bootstrapValidator').validate();
        return $validator.isValid();
    },
    onTabClick: function(tab, navigation, index) {
        return false;
    },
    onTabShow: function(tab, navigation, index) {
        var $total = navigation.find('li').length;
        var $current = index + 1;

        // If it's the last tab then hide the last button and show the finish instead
        if ($current >= $total) {
            $('#rootwizard').find('.pager .next').hide();
            $('#rootwizard').find('.pager .finish').show();
            $('#rootwizard').find('.pager .finish').removeClass('disabled');
        } else {
            $('#rootwizard').find('.pager .next').show();
            $('#rootwizard').find('.pager .finish').hide();
        }
    }});


$('#rootwizard .finish').click(function () {
    var $validator = $('#companyForm').data('bootstrapValidator').validate();
    if ($validator.isValid()) {
        document.getElementById("companyForm").submit();
    }

});
$('#activate').on('ifChanged', function(event){
    $('#companyForm').bootstrapValidator('revalidateField', $('#activate'));
});