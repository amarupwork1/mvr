
$("#quizForm").bootstrapValidator({
    fields: {

        

user_id:{
            validators:{
            notEmpty:{
                message:'This field is required'
        },
        },
}
    }
});

$('#rootwizard').bootstrapWizard({
  //  'tabClass': 'nav nav-pills',
    
   
    onTabShow: function(tab, navigation, index) {
        var $total = navigation.find('li').length;
        var $current = index + 1;
          $('#rootwizard').find(' .pagination').hide();
        
         var previous;
         var next;
         var i;
         for (i = 1; i < 4; i++) { 

        
          next = $current+i;
          previous = $current-i;
          $('#rootwizard').find(' .pagination_'+$current).show();
          $('#rootwizard').find(' .pagination_'+next).show();
          $('#rootwizard').find(' .pagination_'+previous).show();
        }
         
        // If it's the last tab then hide the last button and show the finish instead
        if ($current >= $total) {

            $('#rootwizard').find('.pager .next').hide();
            $('#rootwizard').find('.pager .finish').show();
            $('#rootwizard').find('.pager .finish').removeClass('disabled');
        } else {
            $('#rootwizard').find('.pager .next').show();
            $('#rootwizard').find('.pager .finish').hide();
        }
    }});


$('#rootwizard .finish').click(function () {
    var $validator = $('#quizForm').data('bootstrapValidator').validate();
    if ($validator.isValid()) {
        document.getElementById("quizForm").submit();
    }

});
