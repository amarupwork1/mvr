

$("#new_corrective").bootstrapValidator({
    fields: {
         company_id: {
            validators: {
                notEmpty: {
                    message: 'Company is required'
                }
            }
        },driver_id: {
            validators: {
                notEmpty: {
                    message: 'Driver is required'
                }
            }
        },

date_of_incident :{
    validators: {
        notEmpty:{
        message: "This field is required"
    }
}
},
event_type :{
    validators: {
        notEmpty:{
        message: "This field is required"
    }
}
},
date_recommended :{
    validators: {
        notEmpty:{
        message: "This field is required"
    }
}
},
due_date_for_completion :{
    validators: {
        notEmpty:{
        message: "This field is required"
    }
}
},
recommended_corrective_action :{
    validators: {
        notEmpty:{
        message: "This field is required"
    }
}
},
class_recommendation :{
    validators: {
        notEmpty:{
        message: "This field is required"
    }
}
},
date_completed :{
    validators: {
        notEmpty:{
        message: "This field is required"
    }
}
},
other_explanation :{
    validators: {
        notEmpty:{
        message: "This field is required"
    }
}
}
    }
});


$('#rootwizard').bootstrapWizard({
    'tabClass': 'nav nav-pills',
    'onNext': function(tab, navigation, index) {
        var $validator = $('#companyForm').data('bootstrapValidator').validate();
        return $validator.isValid();
    },
    onTabClick: function(tab, navigation, index) {
        return false;
    },
    onTabShow: function(tab, navigation, index) {
        var $total = navigation.find('li').length;
        var $current = index + 1;

        // If it's the last tab then hide the last button and show the finish instead
        if ($current >= $total) {
            $('#rootwizard').find('.pager .next').hide();
            $('#rootwizard').find('.pager .finish').show();
            $('#rootwizard').find('.pager .finish').removeClass('disabled');
        } else {
            $('#rootwizard').find('.pager .next').show();
            $('#rootwizard').find('.pager .finish').hide();
        }
    }});


$('#rootwizard .finish').click(function () {
    var $validator = $('#companyForm').data('bootstrapValidator').validate();
    if ($validator.isValid()) {
        document.getElementById("companyForm").submit();
    }

});
$('#activate').on('ifChanged', function(event){
    $('#companyForm').bootstrapValidator('revalidateField', $('#activate'));
});