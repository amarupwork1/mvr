<?php

use Illuminate\Database\Seeder;

class VehicleFieldTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Vehicle_field::class, 50)->create();
        factory(App\vehicle_invoice::class, 50)->create();

    }
}
