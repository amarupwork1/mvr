<?php

use Illuminate\Database\Seeder;

class InspectionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\inspection::class, 10)->create()->each(function ($inspection) {
        $inspection->violation()->save(factory(App\Violation::class)->make());
    });
    }
}
