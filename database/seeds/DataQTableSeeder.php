<?php

use Illuminate\Database\Seeder;

class DataQTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Dataq::class, 50)->create();
    }
}
