<?php

use Faker\Generator as Faker;
use Illuminate\Support\Str;


$factory->define(App\Accident::class, function (Faker $faker) {

     return [
         
			'company_id'=>667,
			'driver_id'=>$faker->numberBetween($min = 639, $max = 700),
			'us_dot'=>$faker->numberBetween($min = 1000000, $max = 9000000),
			'date_of_accident'=>$faker->date($format = 'Y-m-d', $max = 'now'),
			'time_of_accident'=>$faker->time($format = 'H:i:s', $max = 'now'),
			'date_accident_reported'=>$faker->date($format = 'Y-m-d', $max = 'now'),
			'time_accident_reported'=>$faker->time($format = 'H:i:s', $max = 'now'),
			'street_location_of_accident'=>$faker->address,
			'city_location_of_accident'=>$faker->city,
			'state_location_of_accident'=>$faker->state,
			'weather_condition_at_time_accident'=>$faker->time($format = 'H:i:s', $max = 'now'),
			'light_condition_at_time_accident'=>$faker->time($format = 'H:i:s', $max = 'now'),
			'number_of_vehicles_involved'=>$faker->randomDigit,
			'carrier_truck_unit_number_involved'=>$faker->randomDigit,
			'carrier_trailer_unit_number_involved'=>$faker->randomDigit,
			'number_of_facilities'=>$faker->randomDigit,
			'number_of_injuries'=>$faker->randomDigit,
			'number_of_vehicles_towed'=>$faker->randomDigit,
			'federal_MCMIS_report_number'=>$faker->randomDigit,
			'police_report_number'=>$faker->randomDigit,
			'internal_claim_number'=>$faker->randomDigit

         ];
});
