<?php

use Faker\Generator as Faker;
use Illuminate\Support\Str;


$factory->define(App\Dataq::class, function (Faker $faker) {

     return [

			 'event_state'=>$faker->randomElement($array = array ('PA','KY','IL','GA','TN')),
			  'status'=>$faker->randomElement($array = array ('Closed - Citation Results Entered','Closed - Data Correction Made','Closed - No Requestor Response')),
			  'last_updated'=>$faker->date($format = 'Y-m-d', $max = 'now'),
			  'type'=>$faker->randomElement($array = array ('Inspection - Citation with Associated Violation','Inspection - Incorrect Information','Compliance Review/CSA Investigation','Inspection - Assigned to Wrong Carrier')),
			  'entered_by'=>$faker->name,
			  'company_name'=>'TRUCKING COMPANY DEMO',
			  'US_DOT'=>589650,
			  'date_entered'=>$faker->date($format = 'Y-m-d', $max = 'now'),
			  'agency_assigned'=>$faker->randomElement($array = array ('PA SP','KY VE','IL DOT','GA DPS','TN DPS')),
			  'report_id'=>$faker->swiftBicNumber,
			  'documents'=>$faker->randomElement($array = array ('No','Yes'))
         ];
});
