<?php

use Faker\Generator as Faker;
use Illuminate\Support\Str;


$factory->define(App\Iss::class, function (Faker $faker) {

     return [
         
			'company_id'=>185,
			'score'=> $faker->numberBetween($min = 10, $max = 100),
			'basis'=> $faker->randomElement($array = array ('Safety','Insufficient data')),
			'recommendation'=>$faker->randomElement($array = array ('Inspect','Pass','Optional'))


         ];
});
