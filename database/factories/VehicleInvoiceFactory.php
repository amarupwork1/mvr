<?php

use Faker\Generator as Faker;
use App\Vehicle_field;

$factory->define(App\vehicle_invoice::class, function (Faker $faker) {

	$array = Vehicle_field::where('owner_dot',589650)->pluck('license_number');

     return [
        'unit_number' => $faker->randomDigit,
        'vehicle_plate'=>$faker->randomElement($array),
        'vin'=>$faker->swiftBicNumber,
        'invoice_no'=>$faker->swiftBicNumber,
        'service_date'=>$faker->date($format = 'Y-m-d', $max = 'now'),
        'invoice_no'=>$faker->swiftBicNumber,
        'service_provider_type'=>$faker->randomElement($array = array ('Automobile','Bus','Truck')),
        'service_provider_name'=>$faker->name,
        'service_provider_street_address'=>$faker->address,
        'service_provider_city'=>$faker->city,
        'service_provider_state'=>$faker->state,
        'service_provider_zip'=>$faker->postcode,
        'service_provider_phone_number'=>$faker->phoneNumber,
        'odometer_reading'=>$faker->numberBetween($min = 210, $max = 202220),
        'oil_change_performed'=>$faker->numberBetween($min = 210, $max = 202220),
        'dot_annual_inspection'=>$faker->randomElement($array = array ('Yes','No')),
        'required_monthly_inspection'=>$faker->randomElement($array = array ('Yes','No')),
        'required_quarterly_inspection'=>$faker->randomElement($array = array ('Yes','No')),
        'oil_change_performed'=>$faker->randomElement($array = array ('Yes','No')),
        'roadside_insp_violation_involved'=>$faker->randomElement($array = array ('Yes','No')),
        'out_of_service_violation'=>$faker->randomElement($array = array ('Yes','No')),
        'dvir_documented_defect'=>$faker->randomElement($array = array ('Yes','No')),
        'citation_issued'=>$faker->randomElement($array = array ('Yes','No')),
        'invoice_comments'=>$faker->text(600)

    ];
});
