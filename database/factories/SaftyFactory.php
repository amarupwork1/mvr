<?php

use Faker\Generator as Faker;



$factory->define(App\Safety::class, function (Faker $faker) {

	
     return [
        'company_id'=>667,
        'unsafe_driving_onroad'=>$faker->numberBetween($min = 0, $max = 70).'%',
        'hos_onroad'=>$faker->numberBetween($min = 0, $max = 70).'%',
        'driver_fitness_onroad'=>$faker->numberBetween($min = 0, $max = 70).'%',
        'controlled_substances_onroad'=>$faker->numberBetween($min = 0, $max = 70).'%',
        'vehicle_maintenance_onroad'=>$faker->numberBetween($min = 0, $max = 70).'%',
        'crash_indicator_onroad'=>$faker->numberBetween($min = 0, $max = 70).'%',
        'insurance_basic'=>$faker->text(400),
        'snapshot_date'=>$faker->date($format = 'Y-m-d', $max = 'now'),
        'total_inspections'=>$faker->numberBetween($min = 0, $max = 70),
        'inspections_vehicle'=>$faker->numberBetween($min = 0, $max = 70),
        'inspections_driver'=>$faker->numberBetween($min = 0, $max = 70),
        'inspections_hazmat'=>$faker->numberBetween($min = 0, $max = 70),
        'out_of_service_vehicle'=>$faker->numberBetween($min = 0, $max = 70),
        'out_of_service_driver'=>$faker->numberBetween($min = 0, $max = 70),
        'out_of_service_hazmat'=>$faker->numberBetween($min = 0, $max = 70),
        'out_of_service_percent_vehicle'=>$faker->numberBetween($min = 0, $max = 70).'%',
        'out_of_service_percent_driver'=>$faker->numberBetween($min = 0, $max = 70).'%',
        'out_of_service_percent_hazmat'=>$faker->numberBetween($min = 0, $max = 70).'%',
        'national_average_vehicle'=>$faker->numberBetween($min = 0, $max = 70).'%',
        'national_average_driver'=>$faker->numberBetween($min = 0, $max = 70).'%',
        'national_average_hazmat'=>$faker->numberBetween($min = 0, $max = 70).'%'
    ];
});
