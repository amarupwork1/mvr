<?php

use Faker\Generator as Faker;
use Illuminate\Support\Str;


$factory->define(App\Violation::class, function (Faker $faker) {
     return [
	     'unit'=>$faker->randomElement($array = array ('1','2','C','D')),
	     'violation_code'=>$faker->randomElement($array = array ('395.8(e)','393.95(a)','396.3(a)(1)','395.3(a)(2)-PROP','392.2WC','362.2WC')),
	     'violations_discovered'=>$faker->randomElement($array = array ('False report of drivers record of duty status','Frame rail flange improperly bent/cut/notched other than by vehicle manufacturer','Inoperable head lamps','Tire-flat and/or audible air leak')),
	     'oos'=>$faker->randomElement($array = array ('No','Yes')),
	     'violations_category'=>$faker->randomElement($array = array ('FALSE LOG BOOK','ALL OTHER DRIVER VIOLATIONS','FRAMES','LIGHTING','EMERGENCY EQUIPMENT','BRAKES, ALL OTHERS')),
	     'violations_basics_category'=>$faker->randomElement($array = array ('hours_of_service_compliance','Not in SMS','vehicle_maintenance')),
	     'state_citation_number'=>$faker->swiftBicNumber,
	     'state_citation_result'=>'N/A',
	     'certify'=>'',
	     'post_crash'=>$faker->randomElement($array = array ('NO','Yes')),
	     'iep'=>'',
	     'AccidentRelated'=>$faker->randomElement($array = array ('NO','Yes')),
	     'Notified'=>1];
});
