<?php

use Faker\Generator as Faker;
use Illuminate\Support\Str;


$factory->define(App\Notification::class, function (Faker $faker) {

     return [
         
			'company_id'=>667,
			'email'=> $faker->email,
			'status'=>$faker->randomElement($array = array ('1','0'))


         ];
});
