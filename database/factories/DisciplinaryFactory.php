<?php

use Faker\Generator as Faker;
use Illuminate\Support\Str;


$factory->define(App\Disciplinary::class, function (Faker $faker) {

     return [
         
			'company_id'=>667,
			'driver_id'=>$faker->numberBetween($min = 639, $max = 700),
			'logging_policy_disciplinary'=>$faker->text(200),
			'logging_date_placed_current_phase'=>$faker->date($format = 'Y-m-d', $max = 'now'),

			'logging_date_current_phase_expires'=>$faker->date($format = 'Y-m-d', $max = 'now'),

			'unsafe_driving_policy'=>$faker->text(200),
			'unsafe_driving_date_placed_current_phase'=>$faker->date($format = 'Y-m-d', $max = 'now'),

			'unsafe_driving_date_current_expires'=>$faker->date($format = 'Y-m-d', $max = 'now'),

			'preventive_maintenance_policy_disciplinary'=>$faker->text(200),
			'preventive_maintenance_date_placed'=>$faker->date($format = 'Y-m-d', $max = 'now'),

			'preventive_maintenance_date_phase_expired'=>$faker->date($format = 'Y-m-d', $max = 'now'),

			'accident_incident_policy_disciplinary'=>$faker->text(200),
			'accident_incident_date_placed'=>$faker->date($format = 'Y-m-d', $max = 'now'),

			'accident_incident_date_phase_expired'=>$faker->date($format = 'Y-m-d', $max = 'now'),

			'daily_vehicle_inspection_policy_disciplinary'=>$faker->text(200),
			'daily_vehicle_inspection_date_placed'=>$faker->date($format = 'Y-m-d', $max = 'now'),

			'daily_vehicle_inspection_date_phase_expired'=>$faker->date($format = 'Y-m-d', $max = 'now'),

			'road_side_inspection_policy_disciplinary'=>$faker->text(200),
			'road_side_inspection_date_placed'=>$faker->date($format = 'Y-m-d', $max = 'now'),

			'road_side_inspection_date_phase_expired'=>$faker->date($format = 'Y-m-d', $max = 'now'),

			'vehicle_defects_inspection_policy_disciplinary'=>$faker->text(200),
			'vehicle_defects_inspection_date_placed'=>$faker->date($format = 'Y-m-d', $max = 'now'),

			'vehicle_defects_inspection_date_phase_expired'=>$faker->date($format = 'Y-m-d', $max = 'now'),



         ];
});
