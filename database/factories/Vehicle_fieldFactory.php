<?php

use Faker\Generator as Faker;



$factory->define(App\Vehicle_field::class, function (Faker $faker) {

	
     return [
        'owner_dot' =>589650,
        'unit_number' => $faker->randomDigit,
        'license_state' => $faker->state,
        'license_number' => $faker->swiftBicNumber,
        'year'=>$faker->numberBetween($min = 2010, $max = 2020),
        'make'=>$faker->numberBetween($min = 2010, $max = 2020),
        'vin'=>$faker->swiftBicNumber,
        'equipment_type'=>$faker->randomElement($array = array ('Automobile','Bus','Truck')),
        'tire_size'=>$faker->numberBetween($min = 20, $max = 70),
        'vehicle_weight'=>$faker->numberBetween($min = 10, $max = 50),
        'gvwr'=>$faker->numberBetween($min = 250510, $max = 2055205),
        'annual_inspection_date'=>$faker->date($format = 'Y-m-d', $max = 'now'),
        'pm_cycle_last_performed'=>$faker->date($format = 'Y-m-d', $max = 'now'),
        'pm_cycle_next_due'=>$faker->date($format = 'Y-m-d', $max = 'now'),
        'last_pm_mileage'=>$faker->numberBetween($min = 20, $max = 70),
        'next_pm_mileage'=>$faker->numberBetween($min = 20, $max = 70),
        'last_pm_date'=>$faker->date($format = 'Y-m-d', $max = 'now'),
        'pm_cycle_days'=>$faker->numberBetween($min = 20, $max = 70),
        'next_pm_date'=>$faker->date($format = 'Y-m-d', $max = 'now'),
        'oil_change_mileage_interval'=>$faker->numberBetween($min = 639, $max = 700)
    ];
});
