<?php

use Faker\Generator as Faker;
use Illuminate\Support\Str;


$factory->define(App\Driver::class, function (Faker $faker) {
     return [
	     'company_id'=>185,
	     'first_name'=>$faker->firstName,
	     'last_name'=>$faker->lastName,
	     'DOB'=>$faker->date($format = 'Y-m-d', $max = 'now'),
	     'license_state'=>$faker->state,
	     'license_number'=>$faker->swiftBicNumber,
	     'license_exp'=>$faker->date($format = 'Y-m-d', $max = 'now'),
	     'hire_date'=>$faker->date($format = 'Y-m-d', $max = 'now'),
	     'termination_date'=>$faker->date($format = 'Y-m-d', $max = 'now'),
	     'reason_for_termination'=>$faker->text(15),
	     'employment_type'=>$faker->randomElement($array = array ('Fleet Owner','Independent Contractor','Other')),
	     'mobile_number'=>'',
	     'email'=>$faker->email,
	     'street_address'=>$faker->address,
	     'city'=>$faker->city,
	     'state'=>$faker->state,
	     'zipcode'=>$faker->postcode];
});
