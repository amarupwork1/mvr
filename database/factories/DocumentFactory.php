<?php

use Faker\Generator as Faker;
use Illuminate\Support\Str;


$factory->define(App\Document::class, function (Faker $faker) {

     return [
         
			'company_id'=>667,
			'driver_id'=>$faker->numberBetween($min = 639, $max = 700),
			'description'=> $faker->text(500),
			'doc_type'=>$faker->randomElement($array = array ('Accident Supporting Documents','Annual MVR','Awards/ Recognition'))


         ];
});
