<?php

use Faker\Generator as Faker;



$factory->define(App\Crash::class, function (Faker $faker) {

	
     return [
        'company_id'=>185,
        'mcmx'=>$faker->numberBetween($min = 250510, $max = 2055205),
        'number_of_fatalities'=>$faker->randomDigit,
        'injuries'=>$faker->randomDigit,
        'traffic_level'=>$faker->randomElement($array = array ('ONE-WAY TRAFFICWAY NOT DIVIDED','TWO-WAY TRAFFICWAY DIVIDED POSITIVE BARRIER','TWO-WAY TRAFFICWAY NOT DIVIDED')),
        'weather'=>$faker->randomElement($array = array ('RAIN','NO ADVERSE CONDITIONS','UNKNOWN')),
        'tow_away'=>$faker->randomElement($array = array ('Y','N')),
        'vehicles_in_crash'=>$faker->randomDigit,
        'access_control'=>$faker->randomElement($array = array ('NO CONTROL','FULL CONTROL','PARTIAL ACCESS CONTROL')),
        'light_level'=>$faker->randomElement($array = array ('DAYLIGHT','DARK - LIGHTED','DARK - NOT LIGHTED')),
        'surface_condition'=>$faker->randomElement($array = array ('DRY','SNOW','WET')),
        'date_time'=>$faker->dateTime($max = 'now', $timezone = null),
        'city_state'=>$faker->city,
        'name'=>$faker->name,
        'citation_issued'=>$faker->randomElement($array = array ('YES','UNKNOWN','No')),
        'location'=>$faker->address,
        'county'=>$faker->country,
        'dob'=>$faker->date($format = 'Y-m-d', $max = 'now'),
        'license'=>$faker->swiftBicNumber,
        'age'=>$faker->numberBetween($min = 20, $max = 70),
        'reporting_state'=>$faker->state,
        'vin'=>$faker->swiftBicNumber,
        'reporting_agency'=>$faker->randomElement($array = array ('Illinois State Police','ISP PERU 16','MT GILEAD OSHP','LEBANON OSP')),
        'state_recordable'=>$faker->state,
        'report'=>$faker->swiftBicNumber,
        'plate'=>$faker->swiftBicNumber,
        'license_state'=>$faker->state,
        'gvw'=>$faker->numberBetween($min = 0, $max = 100),
        'gvw_range'=>$faker->numberBetween($min = 10, $max = 10000),
        'CrashTotalSeverityWeight'=>$faker->numberBetween($min = 50, $max = 500)
    ];
});
