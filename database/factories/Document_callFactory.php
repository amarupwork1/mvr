<?php

use Faker\Generator as Faker;
use Illuminate\Support\Str;


$factory->define(App\Document_call::class, function (Faker $faker) {

     return [
         
			'company_id'=>667,
			'driver_id'=>$faker->numberBetween($min = 639, $max = 700),
			'subject'=> $faker->text(20),
			'email'=> $faker->email,
			'phone'=> $faker->phoneNumber,
			'notes'=> $faker->text(500),


         ];
});
