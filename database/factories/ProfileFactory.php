<?php

use Faker\Generator as Faker;
use Illuminate\Support\Str;


$factory->define(App\Profile::class, function (Faker $faker) {
     return [
	     'gender'=>$faker->randomElement($array = array ('Male','Female')),
	     'dob'=>$faker->date($format = 'Y-m-d', $max = 'now'),
	     'country'=>$faker->swiftBicNumber,
	     'state'=>$faker->state,
	     'city'=>$faker->city,
	     'address'=>$faker->address,
	     'postal'=>$faker->postcode];
});
