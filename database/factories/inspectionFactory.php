<?php

use Faker\Generator as Faker;
use Illuminate\Support\Str;


$factory->define(App\inspection::class, function (Faker $faker) {

     return [
         
			'company_id'=>185,
			 'mcmx'=> $faker->postcode,
			 'report'=>$faker->swiftBicNumber,
			 'inspection_level'=>$faker->randomElement($array = array ('DRIVER-ONLY','WALK-AROUND','FULL')),
			 'inspection_date'=>$faker->date($format = 'Y-m-d', $max = 'now'),
			 'inspection_location'=>$faker->address,
			 'start_end_time'=>$faker->date($format = 'Y-m-d', $max = 'now'),
			 'post_crash_inspection'=>$faker->randomElement($array = array ('No','Yes')),
			 'name'=>$faker->name,
			 'license_state'=>$faker->randomElement($array = array ('CA','ND')),
			 'material_desc'=>'',
			 'dob'=>$faker->date($format = 'Y-m-d', $max = 'now'),
			 'license'=>$faker->swiftBicNumber,
			 'report_qty'=>$faker->randomDigit,
			 'age'=>$faker->numberBetween($min = 20, $max = 100),
			 'waste'=>$faker->text('20'),
			 'notified'=>1

         ];
});
