<?php

use Faker\Generator as Faker;

$factory->define(App\Vehicle_field::class, function (Faker $faker) {
     return [
        'owner_dot' =>589650,
        'unit_number' => $faker->randomDigit,
        'license_state' => $faker->state,
        'license_number' => $faker->swiftBicNumber
    ];
});
