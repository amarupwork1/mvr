<?php

use Faker\Generator as Faker;
use Illuminate\Support\Str;


$factory->define(App\Casenote::class, function (Faker $faker) {

     return [
         
			'company_id'=>667,
			'driver_id'=>$faker->numberBetween($min = 639, $max = 700),
			 'notes'=> $faker->text(400)

         ];
});
