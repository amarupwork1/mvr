<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCorrectivesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('correctives', function (Blueprint $table) {
            $table->increments('id');
             $table->integer('driver_id')->nullable();
            $table->integer('company_id')->nullable();
            $table->date('date_of_incident')->nullable();
            $table->string('event_type')->nullable();
            $table->date('date_recommended')->nullable();
            $table->date('due_date_for_completion')->nullable();
            $table->string('recommended_corrective_action')->nullable();
            $table->string('class_recommendation')->nullable();
            $table->date('date_completed')->nullable();
            $table->text('other_explanation')->nullable();
            $table->timestamps();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('correctives');
    }
}
