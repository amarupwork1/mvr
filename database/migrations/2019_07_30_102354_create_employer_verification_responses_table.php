<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEmployerVerificationResponsesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employer_verification_responses', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('employer_verifications_id')->nullable();
            $table->integer('employer_verification_companies_id')->nullable();
            $table->string('cur_company')->nullable();
            $table->string('cur_email')->nullable();
            $table->string('cur_street_address')->nullable();
            $table->string('cur_phone')->nullable();
            $table->string('cur_fax')->nullable();
            $table->string('cur_city')->nullable();
            $table->string('cur_state')->nullable();
            $table->string('cur_zipcode')->nullable();
            $table->string('prev_company')->nullable();
            $table->string('prev_email')->nullable();
            $table->string('prev_street_address')->nullable();
            $table->string('prev_phone')->nullable();
            $table->string('prev_fax')->nullable();
            $table->string('prev_city')->nullable();
            $table->string('prev_state')->nullable();
            $table->string('prev_zipcode')->nullable();
            $table->string('driver_name')->nullable();
            $table->string('prev_emp_sign')->nullable();
            $table->string('prev_contact_person')->nullable();
            $table->string('job_app_for')->nullable();
            $table->string('cur_emp_worked_from')->nullable();
            $table->string('cur_emp_worked_to')->nullable();
            $table->string('inq_comp_driver')->nullable();
            $table->string('inq_owner_operator')->nullable();
            $table->text('explain_inq')->nullable();
            $table->string('inq_owner_other')->nullable();
            $table->string('inq_types_of_truck_oprd')->nullable();
            $table->string('inq_com_transp')->nullable();
            $table->string('inq_area_opr')->nullable();
            $table->string('inq_acc_descrp')->nullable();
            $table->string('inq_leave_res')->nullable();
            $table->string('inq_re_employ')->nullable();
            $table->text('inq_notes')->nullable();
            $table->string('alcohol_test')->nullable();
            $table->dateTime('alcohol_test_date')->nullable();
            $table->string('substances_test')->nullable();
            $table->dateTime('substances_test_date')->nullable();
            $table->string('refusals')->nullable();
            $table->dateTime('refusals_date')->nullable();
            $table->string('rehabilitation_comp')->nullable();
            $table->dateTime('rehabilitation_comp_date')->nullable();
            $table->string('per_pro_name')->nullable();
            $table->string('per_pro_title')->nullable();
            $table->string('per_pro_company')->nullable();
            $table->dateTime('per_pro_date')->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('employer_verification_responses');
    }
}
