<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateQuestionPretestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('question_pretests', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('topic_id')->nullable();
            $table->text('question')->nullable();
            $table->text('question_type')->nullable();
          

            $table->string('a')->nullable();
            $table->string('b')->nullable();
            $table->string('c')->nullable();
            $table->string('d')->nullable();
            $table->string('answer')->nullable();
            $table->text('code_snippet')->nullable();
            $table->text('answer_exp')->nullable();
            $table->string('question_img')->nullable();
            $table->string('question_video_link')->nullable();
            $table->integer('order_column')->nullable();
            $table->timestamps();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('question_pretests');
    }
}
