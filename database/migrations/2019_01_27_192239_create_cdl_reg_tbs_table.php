<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCdlRegTbsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cdl_reg_tbs', function (Blueprint $table) {
            $table->increments('id');
        
            $table->string('fname')->nullable();
            $table->string('lname')->nullable();
            $table->string('email')->nullable();
            $table->string('Phone')->nullable();
            $table->string('cell')->nullable();
            $table->string('OptOutSMS')->nullable();
            $table->string('address')->nullable();
            $table->string('city')->nullable();
            $table->string('state')->nullable();
            $table->string('zip')->nullable();
            $table->string('cdl_number')->nullable();
            $table->string('company_id')->nullable();
            $table->string('company_name')->nullable();
            $table->string('verified')->nullable();
            $table->string('pwd')->nullable();
            $table->string('file')->nullable();
            $table->string('TimeZone')->nullable();
            $table->string('Citation_Tracker_User_Email')->nullable();
            $table->string('Citation_Tracker_User_First_Name')->nullable();
            $table->string('Citation_Tracker_User_Last_Name')->nullable();
            $table->string('DOT_Number')->nullable();
            $table->string('access_level')->nullable();
            $table->string('timestamp')->nullable();
            $table->string('status')->nullable();
            $table->string('token')->nullable();
                $table->timestamps();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cdl_reg_tbs');
    }
}
