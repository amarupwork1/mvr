<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCdlSmsInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cdl_sms_infos', function (Blueprint $table) {
            $table->increments('id');
          
            $table->string('message_id')->nullable();
            $table->string('error_text')->nullable();
            $table->string('status')->nullable();
            $table->string('message')->nullable();
            $table->string('sent_to')->nullable();
            $table->string('raw_result')->nullable();
            $table->string('timestamp')->nullable();
              $table->timestamps();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cdl_sms_infos');
    }
}
