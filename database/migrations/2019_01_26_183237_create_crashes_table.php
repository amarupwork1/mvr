<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCrashesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crashes', function (Blueprint $table) {
            $table->increments('id');
           
            $table->integer('company_id')->nullable();
            $table->string('mcmx')->nullable();
            $table->string('number_of_fatalities')->nullable();
            $table->string('injuries')->nullable();
            $table->string('traffic_level')->nullable();
            $table->string('weather')->nullable();
            $table->string('tow_away')->nullable();
            $table->string('vehicles_in_crash')->nullable();
            $table->string('access_control')->nullable();
            $table->string('light_level')->nullable();
            $table->string('surface_condition')->nullable();
            $table->string('date_time')->nullable();
            $table->string('city_state')->nullable();
            $table->string('name')->nullable();
            $table->string('citation_issued')->nullable();
            $table->string('location')->nullable();
            $table->string('county')->nullable();
            $table->string('dob')->nullable();
            $table->string('license')->nullable();
            $table->string('age')->nullable();
            $table->string('reporting_state')->nullable();
            $table->string('reporting_sequence')->nullable();
            $table->string('vin')->nullable();
            $table->string('hm_placards')->nullable();
            $table->string('reporting_agency')->nullable();
            $table->string('state_recordable')->nullable();
            $table->string('plate')->nullable();
            $table->string('release_of_cargo')->nullable();
            $table->string('officer_badge')->nullable();
            $table->string('federally_recordable')->nullable();
            $table->string('license_state')->nullable();
            $table->string('body_type')->nullable();
            $table->string('report')->nullable();
            $table->string('configuration')->nullable();
            $table->string('gvw')->nullable();
            $table->string('gvw_range')->nullable();
             $table->timestamps();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('crashes');
    }
}
