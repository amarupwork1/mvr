<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDataqsRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dataqs_requests', function (Blueprint $table) {
            $table->increments('id');
          
            $table->string('review_id')->nullable();
            $table->string('id_')->nullable();
            $table->date('date_submitted')->nullable();
            $table->string('state')->nullable();
            $table->string('request_type')->nullable();
            $table->string('status')->nullable();
            $table->string('requestor')->nullable();
              $table->timestamps();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('dataqs_requests');
    }
}
