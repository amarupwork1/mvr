<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDisciplinariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('disciplinaries', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id')->nullable();
            $table->integer('driver_id')->nullable();
            $table->string('logging_policy_disciplinary')->nullable();
            $table->date('logging_date_placed_current_phase')->nullable();
            $table->date('logging_date_current_phase_expires')->nullable();
            $table->string('unsafe_driving_policy')->nullable();
            $table->date('unsafe_driving_date_placed_current_phase')->nullable();
            $table->date('unsafe_driving_date_current_expires')->nullable();
            $table->string('preventive_maintenance_policy_disciplinary')->nullable();
            $table->date('preventive_maintenance_date_placed')->nullable();
            $table->date('preventive_maintenance_date_phase_expired')->nullable();
            $table->string('accident_incident_policy_disciplinary')->nullable();
            $table->date('accident_incident_date_placed')->nullable();
            $table->date('accident_incident_date_phase_expired')->nullable();
            $table->string('daily_vehicle_inspection_policy_disciplinary')->nullable();
            $table->date('daily_vehicle_inspection_date_placed')->nullable();
            $table->date('daily_vehicle_inspection_date_phase_expired')->nullable();
            $table->string('road_side_inspection_policy_disciplinary')->nullable();
            $table->date('road_side_inspection_date_placed')->nullable();
            $table->date('road_side_inspection_date_phase_expired')->nullable();
            $table->string('vehicle_defects_inspection_policy_disciplinary')->nullable();
            $table->date('vehicle_defects_inspection_date_placed')->nullable();
            $table->date('vehicle_defects_inspection_date_phase_expired')->nullable();
            $table->timestamps();

            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('disciplinaries');
    }
}
