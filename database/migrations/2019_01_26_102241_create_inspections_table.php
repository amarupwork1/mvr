<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateInspectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inspections', function (Blueprint $table) {
            $table->increments('id');
           
            $table->integer('company_id')->nullable();
            $table->string('mcmx')->nullable();
            $table->string('report')->nullable();
            $table->string('inspection_level')->nullable();
            $table->date('inspection_date')->nullable();
            $table->string('inspection_location')->nullable();
            $table->string('start_end_time')->nullable();
            $table->string('post_crash_inspection')->nullable();
            $table->string('name')->nullable();
            $table->string('license_state')->nullable();
            $table->string('material_desc')->nullable();
            $table->string('dob')->nullable();
            $table->string('license')->nullable();
            $table->string('report_qty')->nullable();
            $table->string('age')->nullable();
            $table->string('waste')->nullable();
            $table->string('notified')->nullable();
             $table->timestamps();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('inspections');
    }
}
