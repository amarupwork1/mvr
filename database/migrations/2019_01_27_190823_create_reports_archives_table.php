<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateReportsArchivesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reports_archives', function (Blueprint $table) {
            $table->increments('id');
            
            $table->string('user_id')->nullable();
            $table->string('snapshot_date')->nullable();
            $table->string('driver_name')->nullable();
            $table->string('license_state')->nullable();
            $table->string('license_number')->nullable();
            $table->string('crash_no')->nullable();
            $table->string('insp_w_vio')->nullable();
            $table->string('vio_no')->nullable();
            $table->string('RF')->nullable();
            $table->string('UD')->nullable();
            $table->string('HOS')->nullable();
            $table->string('DF')->nullable();
            $table->string('DA')->nullable();
            $table->string('VM')->nullable();
            $table->string('HAZ')->nullable();
            $table->string('driver_ranking')->nullable();
            $table->string('DPI')->nullable();
            $table->timestamps();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('reports_archives');
    }
}
