<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateIssesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('isses', function (Blueprint $table) {
            $table->increments('id');
          
            $table->integer('company_id')->nullable();
            $table->foreign('company_id')->references('id')->on('companies');

            $table->string('score')->nullable();
            $table->string('recommendation')->nullable();
            $table->string('basis')->nullable();
              $table->timestamps();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('isses');
    }
}
