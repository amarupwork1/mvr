<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('Name')->nullable();
            $table->string('company')->nullable();
            $table->string('company_id')->nullable();
            $table->string('Address')->nullable();
            $table->string('Birthdate')->nullable();
            $table->string('City')->nullable();
            $table->string('State')->nullable();
            $table->string('Zip')->nullable();
            $table->string('Dl_Number')->nullable();
            $table->string('Class_Commercial')->nullable();
            $table->string('Vehicle_Lic_No')->nullable();
            $table->string('Citation_Type')->nullable();
            $table->string('violation')->nullable();
            $table->string('Location_violation')->nullable();
            $table->string('City_County_Occurence')->nullable();
            $table->string('Speed_Approx')->nullable();
            $table->string('Arresting_Officer_Name')->nullable();
            $table->string('Note')->nullable();
            $table->string('file')->nullable();
            $table->string('path')->nullable();
            $table->string('date_time')->nullable();
            $table->string('user_email')->nullable();
            $table->string('indicator')->nullable();
            $table->string('lawyer_email')->nullable();
            $table->string('Admin_note')->nullable();
            $table->string('citation_no')->nullable();
            $table->string('status')->nullable();
            $table->string('updated_by')->nullable();
            $table->string('court_date')->nullable();
            $table->string('court_address')->nullable();
            $table->string('court_phone')->nullable();
            $table->string('ticket_dispo')->nullable();
            $table->string('date_issued')->nullable();
            $table->string('court_name')->nullable();
            $table->string('county')->nullable();
            $table->string('ticket_number')->nullable();
            $table->string('lawyer_id')->nullable();
            $table->string('road_side_inspection')->nullable();
            $table->string('road_side_inspection_results')->nullable();
            $table->string('sales_agent')->nullable();
            $table->string('fname')->nullable();
            $table->string('lname')->nullable();
            $table->string('sales_agent_name')->nullable();
            $table->string('sales_agent_email')->nullable();
            $table->string('sales_agent_id')->nullable();
            $table->string('sf_id')->nullable();
            $table->string('DataQ_Number__c')->nullable();
            $table->string('Roadside_Inspection_Number__c')->nullable();
            $table->string('Ticket_Type')->nullable();
            $table->string('Beginning_Fine_Amount')->nullable();
            $table->string('Final_Fine_Amount')->nullable();
            $table->string('Processor_Name')->nullable();
            $table->string('Processor_Email')->nullable();
            $table->string('Processor_Ph_Number')->nullable();
            $table->string('Processor_Notes_To_Attorney')->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tickets');
    }
}
