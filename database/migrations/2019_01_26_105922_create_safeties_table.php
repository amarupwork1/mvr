<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSafetiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('safeties', function (Blueprint $table) {
            $table->increments('id');
          
            $table->integer('company_id')->nullable();
            $table->foreign('company_id')->references('id')->on('companies');
            $table->string('unsafe_driving_onroad')->nullable();
            $table->string('hos_onroad')->nullable();
            $table->string('driver_fitness_onroad')->nullable();
            $table->string('controlled_substances_onroad')->nullable();
            $table->string('vehicle_maintenance_onroad')->nullable();
            $table->string('hazardous_materials_onroad')->nullable();
            $table->string('crash_indicator_onroad')->nullable();
            $table->string('insurance_onroad')->nullable();
            $table->string('unsafe_driving_investigation')->nullable();
            $table->string('hos_investigation')->nullable();
            $table->string('driver_fitness_investigation')->nullable();
            $table->string('controlled_substances_investigation')->nullable();
            $table->string('vehicle_maintenance_investigation')->nullable();
            $table->string('hazardous_materials_investigation')->nullable();
            $table->string('crash_indicator_investigation')->nullable();
            $table->string('insurance_investigation')->nullable();
            $table->string('unsafe_driving_basic')->nullable();
            $table->string('hos_basic')->nullable();
            $table->string('driver_fitness_basic')->nullable();
            $table->string('controlled_substances_basic')->nullable();
            $table->string('vehicle_maintenance_basic')->nullable();
            $table->string('hazardous_materials_basic')->nullable();
            $table->string('crash_indicator_basic')->nullable();
            $table->string('insurance_basic')->nullable();
            $table->string('snapshot_date')->nullable();
            $table->string('total_inspections')->nullable();
            $table->string('inspections_vehicle')->nullable();
            $table->string('inspections_driver')->nullable();
            $table->string('inspections_hazmat')->nullable();
            $table->string('out_of_service_vehicle')->nullable();
            $table->string('out_of_service_driver')->nullable();
            $table->string('out_of_service_hazmat')->nullable();
            $table->string('out_of_service_percent_vehicle')->nullable();
            $table->string('out_of_service_percent_driver')->nullable();
            $table->string('out_of_service_percent_hazmat')->nullable();
            $table->string('national_average_vehicle')->nullable();
            $table->string('national_average_driver')->nullable();
            $table->string('national_average_hazmat')->nullable();
              $table->timestamps();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('safeties');
    }
}
