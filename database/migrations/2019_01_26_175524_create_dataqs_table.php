<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDataqsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dataqs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('event_state')->nullable();
            $table->string('status')->nullable();
            $table->date('last_updated')->nullable();
            $table->string('type')->nullable();
            $table->string('entered_by')->nullable();
            $table->string('company_name')->nullable();
            $table->string('US_DOT')->nullable();
            $table->date('date_entered')->nullable();
            $table->string('agency_assigned')->nullable();
            $table->string('report_id')->nullable();
            $table->string('documents')->nullable();
            $table->text('source')->nullable();
            $table->timestamps();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('dataqs');
    }
}
