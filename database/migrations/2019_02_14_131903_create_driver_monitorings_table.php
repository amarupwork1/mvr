<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDriverMonitoringsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('driver_monitorings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('driver_id')->nullable();
            $table->string('state')->nullable();
            $table->string('license_number')->nullable();
            $table->string('first_name')->nullable();
            $table->string('middle_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('dob')->nullable();
            $table->string('postal_code')->nullable();
            $table->string('work_start_date')->nullable();
            $table->text('division_name')->nullable();
            $table->string('division_tags')->nullable();
            $table->string('driver_reference_number')->nullable();
            $table->string('cdl_driver')->nullable();
            $table->string('schedule_names')->nullable();
            $table->string('auth_code')->nullable();
            $table->string('last_mvr_date')->nullable();
            $table->string('custom_date1')->nullable();
            $table->string('client_id')->nullable();
            $table->string('bad_data')->nullable();
            $table->string('work_status')->nullable();
            $table->string('notified')->nullable();
            $table->text('source')->nullable();
            $table->timestamps();
            
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('driver_monitorings');
    }
}
