<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFormControlledSubstanceAlcoholsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('form_controlled_substance_alcohols', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('company_id')->nullable();
            $table->date('application_date')->nullable();
            $table->string('first_name')->nullable();
            $table->string('middle_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('address')->nullable();
            $table->string('home_telephone')->nullable();
            $table->string('city')->nullable();
            $table->string('state')->nullable();
            $table->string('zip')->nullable();
            $table->string('cell_telephone')->nullable();
            $table->string('date_of_birth')->nullable();
            $table->string('social_security_number')->nullable();
            $table->string('physical_street_address')->nullable();
            $table->string('tested_pre_employment_drug')->nullable();
            $table->string('return_to_duty_process')->nullable();
            $table->date('date_of_sign')->nullable();
            $table->string('signature')->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('form_controlled_substance_alcohols');
    }
}
