<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateInsuranceInformationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('insurance_informations', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('dot_number')->nullable();
            $table->string('docket_no')->nullable();
            $table->string('blanket_company')->nullable();
            $table->string('property')->nullable();
            $table->string('passenger')->nullable();
            $table->string('household_goods')->nullable();
            $table->string('private')->nullable();
            $table->string('enterprise')->nullable();
            $table->string('common_application_pending')->nullable();
            $table->string('bipd_insurance_required')->nullable();
            $table->string('cargo_insurance_required')->nullable();
            $table->string('cargo_insurance_on_file')->nullable();
            $table->string('undeliverable_mail')->nullable();
            $table->string('contract_application_pending')->nullable();
            $table->string('mail_telephone_and_fax')->nullable();
            $table->string('common_authority_status')->nullable();
            $table->string('business_tel_and_fax')->nullable();
            $table->string('broker_authority_status')->nullable();
            $table->text('mail_address')->nullable();
            $table->text('bussiness_address')->nullable();
            $table->string('bond_insurance_required')->nullable();
            $table->string('doing_business_as_name')->nullable();
            $table->string('boc3_yes')->nullable();
            $table->string('contract_authority_status')->nullable();
            $table->string('legal_name')->nullable();
            $table->string('bond_insurance_on_file')->nullable();
            $table->string('bipd_insurance_on_file')->nullable();
            $table->string('broker_application_pending')->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('insurance_informations');
    }
}
