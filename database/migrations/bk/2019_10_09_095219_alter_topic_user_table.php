<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTopicUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('topic_user', function (Blueprint $table) {
            $table->integer('company_id')->nullable();
            $table->date('date_of_incident')->nullable();
            $table->string('event_type')->nullable();
            $table->date('date_recommended')->nullable();
            $table->date('due_date_for_completion')->nullable();
            $table->string('recommended_corrective_action')->nullable();
            $table->string('class_recommendation')->nullable();
            $table->date('date_completed')->nullable();
            $table->text('other_explanation')->nullable();
             $table->timestamps();

         });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
