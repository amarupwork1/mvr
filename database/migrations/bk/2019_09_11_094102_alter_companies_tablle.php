<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCompaniesTablle extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::table('companies', function (Blueprint $table) {
            $table->string('mc_number')->after('dot_pi')->nullable();
            $table->string('ca_permit_number')->after('mc_number')->nullable();
            $table->string('duns_number')->after('ca_permit_number')->nullable();
            $table->string('scac_number')->after('duns_number')->nullable();
            $table->string('kent_kyu_number')->after('scac_number')->nullable();
            $table->string('tx_cp_number')->after('kent_kyu_number')->nullable();
            $table->string('ifta_number')->after('tx_cp_number')->nullable();
            $table->string('ucr')->after('ifta_number')->nullable();
            $table->string('ucr_year')->after('ucr')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
