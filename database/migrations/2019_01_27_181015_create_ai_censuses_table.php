<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAiCensusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ai_censuses', function (Blueprint $table) {
            $table->increments('id');
            
            $table->string('dot_number')->nullable();
            $table->text('legal_name')->nullable();
            $table->text('dba_name')->nullable();
            $table->string('carrier_operation')->nullable();
            $table->string('hm_flag')->nullable();
            $table->string('pc_flag')->nullable();
            $table->string('phy_street')->nullable();
            $table->string('phy_city')->nullable();
            $table->string('phy_state')->nullable();
            $table->string('phy_zip')->nullable();
            $table->string('phy_country')->nullable();
            $table->string('mailing_street')->nullable();
            $table->string('mailing_city')->nullable();
            $table->string('mailing_state')->nullable();
            $table->string('mailing_zip')->nullable();
            $table->string('mailing_country')->nullable();
            $table->string('telephone')->nullable();
            $table->string('fax')->nullable();
            $table->string('email_address')->nullable();
            $table->string('mcs150_date')->nullable();
            $table->string('mcs150_mileage')->nullable();
            $table->string('mcs150_mileage_year')->nullable();
            $table->string('add_date')->nullable();
            $table->string('oic_state')->nullable();
            $table->string('nbr_power_unit')->nullable();
            $table->string('driver_total')->nullable();
            $table->timestamps();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ai_censuses');
    }
}
