<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCorrespondencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('correspondences', function (Blueprint $table) {

            $table->increments('id');
            $table->unsignedInteger('company_id');
            $table->string('name')->nullable();
            $table->string('action')->nullable();
            $table->date('date_sent')->nullable();
            $table->integer('notified')->nullable();
            $table->timestamps();
            $table->foreign('company_id')->references('id')->on('companies');

            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('correspondences');
    }
}
