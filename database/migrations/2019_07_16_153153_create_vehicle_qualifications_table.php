<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVehicleQualificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicle_qualifications', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('operating_carrier_dot')->nullable();
            $table->string('unit_number')->nullable();
            $table->string('license_state')->nullable();
            $table->string('license_number')->nullable();
            $table->string('vin')->nullable();
            $table->string('file_received_by_cdlm')->nullable();
            $table->string('initial_vehicle_qualification_file_reviewed_by_cdlm')->nullable();
            $table->text('name_of_initial_vehicle_qualification_file_reviewer')->nullable();
            $table->date('date_of_initial_vehicle_qualification_file_review')->nullable();
            $table->date('date_of_most_recent_vehicle_qualification_file_review')->nullable();
            $table->text('veh_qual_file_issues_comments')->nullable();
            $table->string('copy_of_lease_agreement')->nullable();
            $table->string('receipt_of_equipment')->nullable();
            $table->string('proof_of_financial_responsibility')->nullable();
            $table->string('certificate_of_insurance')->nullable();
            $table->string('proof_of_ownership')->nullable();
            $table->string('specification_sheet')->nullable();
            $table->string('certificate_of_registration')->nullable();
            $table->string('manufacturers_certificate')->nullable();
            $table->string('manufacturers_asme_u1a')->nullable();
            $table->string('annual_unit_inspection')->nullable();
            $table->string('license_plate_renewal')->nullable();
            $table->string('external_visual_inspection_and_testing')->nullable();
            $table->string('internal_visual_inspection')->nullable();
            $table->string('leakage_testing')->nullable();
            $table->string('thickness_testing')->nullable();
            $table->string('pressure_test')->nullable();
            $table->string('lining_inspection')->nullable();
            $table->string('new_mexico_permit')->nullable();
            $table->string('california_air_resources_permit')->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('vehicle_qualifications');
    }
}
