<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFcraAppFormsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fcra_app_forms', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('consumer_reporting_agency_1')->nullable();
            $table->string('consumer_reporting_agency_2')->nullable();
            $table->string('drivers_license_state')->nullable();
            $table->string('preferred_company_name')->nullable();
            $table->string('employement_reporting_agency_1')->nullable();
            $table->string('employement_reporting_agency_2')->nullable();
            $table->string('employement_drivers_license_state')->nullable();
            $table->string('printed_name')->nullable();
            $table->string('social_security_number')->nullable();
            $table->string('drivers_license_number2')->nullable();
            $table->string('drivers_license_state2')->nullable();
            $table->string('fcra_signature')->nullable();
            $table->string('fcra_signature_date')->nullable();
            $table->string('company_id')->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('fcra_app_forms');
    }
}
