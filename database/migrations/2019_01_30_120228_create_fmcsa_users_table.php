<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFmcsaUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fmcsa_users', function (Blueprint $table) {
            $table->increments('id');
           
            $table->string('login')->nullable();
            $table->string('password')->nullable();
            $table->string('dot_number')->nullable();
            $table->string('company_name')->nullable();
            $table->tinyInteger('login_problem')->nullable();
            $table->string('initialized')->nullable();
            $table->date('inception_date')->nullable();
            $table->text('source')->nullable();
            $table->dateTime('last_success')->nullable();
             $table->timestamps();  
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('fmcsa_users');
    }
}
