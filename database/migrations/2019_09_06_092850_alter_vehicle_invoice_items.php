<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterVehicleInvoiceItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {


        Schema::table('vehicle_invoice_items', function($table) {
           $table->dropColumn('service');
           $table->dropColumn('unit');
           $table->dropColumn('unit_price');
           $table->dropColumn('quantity');
           $table->dropColumn('total');

          
       });

        Schema::table('vehicle_invoice_items', function (Blueprint $table) {
            $table->string('product_name')->after('invoice_id')->nullable();
            $table->string('labor_description')->after('product_name')->nullable();
            $table->string('type_of_service')->after('labor_description')->nullable();
            $table->integer('part_quantity')->after('type_of_service')->nullable();
            $table->integer('part_cost')->after('part_quantity')->nullable();
            $table->integer('labor_hours')->after('part_cost')->nullable();
            $table->integer('labor_cost')->after('labor_hours')->nullable();
            $table->integer('labor_cost_total')->after('labor_cost')->nullable();
            $table->integer('shop_fees_cost')->after('labor_cost_total')->nullable();
            $table->integer('road_service_cost')->after('shop_fees_cost')->nullable();


        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
