<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDriversTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('drivers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->integer('company_id')->unsigned()->nullable();
            $table->string('unique_key')->nullable();;
            $table->string('first_name')->nullable();;
            $table->string('middle_name')->nullable();
            $table->string('last_name')->nullable();;
            $table->string('suffix')->nullable();
            $table->date('DOB')->nullable();;
            $table->string('license_state')->nullable();;
            $table->string('license_number')->nullable();;
            $table->date('license_exp')->nullable();;
            $table->date('hire_date')->nullable();;
            $table->date('termination_date')->nullable();
            $table->text('reason_for_termination')->nullable();
            $table->text('employment_type')->nullable();
            $table->string('mobile_number')->nullable();;

            $table->string('email')->nullable();;
            $table->text('street_address')->nullable();;
            $table->text('sec_street_address')->nullable();
            $table->string('city')->nullable();;
            $table->string('state')->nullable();;
            $table->string('zipcode')->nullable();;

            $table->date('hiring_drug_screen_date')->nullable();
            $table->string('drug_screen_availability')->nullable();
            $table->date('from_mro_date')->nullable();
            $table->string('mro_availability')->nullable();
            $table->date('hiring_mvr_date')->nullable();
            $table->string('mvr_availability')->nullable();
            $table->date('first_trip_date')->nullable();
            $table->string('first_trip_availability')->nullable();
            $table->date('current_dot_physical_date')->nullable();
            $table->date('mec_exp_date')->nullable();

            $table->string('dot_medical_examine_name')->nullable();
            $table->string('medical_examine_nation_registry_num')->nullable();
            $table->date('most_recent_date')->nullable();
            $table->date('date_of_review_of_mvr')->nullable();
            $table->string('first_and_last_name_of_reviewer')->nullable();
            $table->date('date_of_mec_exp_mvr')->nullable();
            $table->date('date_recent_certificate_of_violation')->nullable();
            $table->date('date_of_review_certi_vio')->nullable();
            $table->string('name_of_reviewer_of_cert')->nullable();
            $table->date('comm_driv_licen_exp')->nullable();

            $table->string('hazmat_endorsement')->nullable();
            $table->date('hazmat_end_exp_date')->nullable();
            $table->string('tank_endorse')->nullable();
            $table->date('tax_endor_expi_date')->nullable();
            $table->string('passenger_endor')->nullable();
            $table->date('passenger_endor_exp_date')->nullable();
            $table->string('endorse_other')->nullable();
            $table->string('hearing_device_restrict')->nullable();
            $table->string('medical_waiver_req')->nullable();
            $table->string('restriction_other')->nullable();
            $table->string('twic_qualified')->nullable();
            $table->date('twic_expiration')->nullable();
            $table->string('fast_qualified')->nullable();
            $table->date('fast_exp')->nullable();
            $table->string('passport')->nullable();
            $table->date('passport_exp')->nullable();

            $table->string('driver_qualification_file_received_by_cdlm')->nullable();
            $table->string('initial_driver_qualification_file_reviewed_by_cdlm')->nullable();

            $table->string('name_of_initial_driver_qualification_file_reviewer')->nullable();
            $table->date('date_of_initial_driver_qualification_file_review')->nullable();

            $table->string('driver_application_in_file')->nullable();
            $table->string('inquiry_to_previous_employers_in_file')->nullable();
            $table->string('hiring_motor_vehicle_record_in_file')->nullable();
            $table->string('commercial_drivers_license_front_and_back_in_file')->nullable();
            $table->string('road_test_in_file')->nullable();
            $table->string('certification_of_road_test_in_file')->nullable();
            $table->string('motor_vehicle_record_ran_within_last_12_months')->nullable();
            $table->string('certificate_of_violations_signed_by_driver_within_last_12_months')->nullable();
            $table->string('dot_medical_examiners_certificate_mec')->nullable();
            $table->string('medical_waiver')->nullable();
            $table->string('national_registry_of_certified_medical_examiners_verified')->nullable();
            $table->text('dq_file_issues_comments')->nullable();
            $table->boolean('status')->default('0');
            $table->timestamps();

          

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('drivers');
    }
}
