<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documents', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('driver_id')->nullable();
                $table->integer('company_id')->nullable();
                $table->text('description')->nullable();
                $table->string('doc_type')->nullable();
                $table->text('file')->nullable();
                $table->text('source')->nullable();
                $table->timestamps();
               
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('documents');
    }
}
