<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEmployerVerificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employer_verifications', function (Blueprint $table) {
                $table->increments('id');
                $table->string('us_dot')->nullable();
                $table->unsignedBigInteger('driver_id')->nullable();
                $table->string('company_name')->nullable();
                $table->string('company_address')->nullable();
                $table->date('contact_date')->nullable();
                $table->string('contact_method')->nullable();
                $table->text('information_received')->nullable();
                $table->string('source')->nullable();
                $table->timestamps();
               // $table->foreign('driver_id')->references('id')->on('users');


            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('employer_verifications');
    }
}
