<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWitnessTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('witness_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('accident_id');
            $table->string('witness_name')->nullable();
            $table->integer('witness_phone')->nullable();
            $table->string('medical_attn_sought_after')->nullable();
            $table->text('accident_diary')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('witness_details');
    }
}
