<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCsWatchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cs_watches', function (Blueprint $table) {
            $table->increments('id');
            $table->string('section')->nullable();
            $table->text('description')->nullable();
            $table->text('violation_group')->nullable();
            $table->string('severity_weight')->nullable();
            $table->string('violation_DSMS')->nullable();
            $table->string('driver_responsible')->nullable();
            $table->string('observable')->nullable();
            $table->string('basics_category')->nullable()->default('unsafe_driving');
            $table->string('defect_type')->nullable();
            $table->integer('red_flag')->nullable();
           $table->timestamps();

            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cs_watches');
    }
}
