<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVehiclePeriodicInspectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicle_periodic_inspections', function (Blueprint $table) {
            $table->increments('id');
            $table->string('vehicle_plate');
            $table->date('last_required_monthly_inspection')->nullable();
            $table->date('next_required_monthly_inspection')->nullable();
            $table->date('last_required_quarterly_inspection')->nullable();
            $table->date('next_required_quarterly_inspection')->nullable();
            $table->date('last_dot_annual_inspection')->nullable();
            $table->date('next_dot_annual_inspection')->nullable();
            $table->date('last_oil_change_performed_date')->nullable();
            $table->string('last_oil_change_performed_odometer')->nullable();
            $table->string('next_oil_change_performed_odometer')->nullable();
            $table->timestamps();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('vehicle_periodic_inspections');
    }
}
