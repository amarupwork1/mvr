<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDataqsDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dataqs_documents', function (Blueprint $table) {
            $table->increments('id');
          
            $table->integer('review_id')->nullable();
            $table->string('date_uploaded')->nullable();
            $table->string('document_name')->nullable();
            $table->string('document_url')->nullable();
            $table->string('title')->nullable();
            $table->string('content_type')->nullable();
            $table->string('size')->nullable();
              $table->timestamps();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('dataqs_documents');
    }
}
