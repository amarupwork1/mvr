<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAccidentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('accidents', function (Blueprint $table) {
            $table->string('witness_name')->after('preventable_decision')->nullable();
            $table->string('witness_phone_number')->after('witness_name')->nullable();
            $table->string('medical_attn_sought_after')->after('witness_phone_number')->nullable();
            $table->string('accident_diary')->after('medical_attn_sought_after')->nullable();
            

        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
