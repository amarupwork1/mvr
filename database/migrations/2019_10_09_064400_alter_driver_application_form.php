<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterDriverApplicationForm extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::table('driver_application_forms', function (Blueprint $table) {
          $table->integer('company_id')->nullable();
            $table->string('mailing_state')->nullable();
            $table->string('mailing_zip')->nullable();
            $table->string('mailing_country')->nullable();
            $table->string('home_telephone')->nullable();
            $table->string('mobile_telephone')->nullable();
            $table->string('date_of_birth')->nullable();
            $table->string('social_security_number')->nullable();
            $table->string('previous_physical_street_address')->nullable();
            $table->string('previous_physical_street_address2')->nullable();
            $table->string('previous_physical_city')->nullable();
            $table->string('previous_physical_state')->nullable();
            $table->string('previous_physical_zip')->nullable();
            $table->string('previous_physical_country')->nullable();
            $table->string('date_from')->nullable();
            $table->string('date_to')->nullable();
            $table->string('license_state')->nullable();
            $table->string('license_number')->nullable();
            $table->string('license_expiration_date')->nullable();
            $table->string('license_country')->nullable();
            $table->string('previous_license_state')->nullable();
            $table->string('previous_license_number')->nullable();
            $table->string('previous_license_expiration_date')->nullable();
            $table->string('previous_license_country')->nullable();
            $table->string('type_of_vehicle_driven')->nullable();
            $table->string('driven_from')->nullable();
            $table->string('driven_to')->nullable();
            $table->string('approximate_mileage_driven')->nullable();
            $table->string('involved_in_any_accidents')->nullable();
            $table->string('accident_date')->nullable();
            $table->string('accident_describe')->nullable();
            $table->string('accident_fatalities')->nullable();
            $table->string('accident_injuries')->nullable();
            $table->string('traffic_violation_convictions')->nullable();
            $table->string('violation_date')->nullable();
            $table->string('violation')->nullable();
            $table->string('violation_state')->nullable();
            $table->string('violation_commercial_vehicle')->nullable();
            $table->string('driver_license_denied')->nullable();
            $table->string('state_of_issuance')->nullable();
            $table->string('explanation')->nullable();
            $table->string('history_employer')->nullable();
            $table->string('history_dates_from')->nullable();
            $table->string('history_dates_to')->nullable();
            $table->string('history_address')->nullable();
            $table->string('history_supervisor')->nullable();
            $table->string('history_city')->nullable();
            $table->string('history_state')->nullable();
            $table->string('history_zip')->nullable();
            $table->string('history_telephone')->nullable();
            $table->string('history_email')->nullable();
            $table->string('history_fax')->nullable();
            $table->string('federal_motor_carrier_safety')->nullable();
            $table->string('substance_and_alcohol')->nullable();
            $table->string('reason_for_leaving')->nullable();
            $table->string('applicant_signature')->nullable();
            $table->string('signature_date')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
