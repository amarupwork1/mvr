<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCompanySnapshotFullsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_snapshot_fulls', function (Blueprint $table) {
            $table->increments('id');
           
            $table->string('date_str')->nullable();
            $table->string('date')->nullable();
            $table->string('date_info')->nullable();
            $table->string('usdot')->nullable();
            $table->string('entity_type')->nullable();
            $table->string('operating_status')->nullable();
            $table->string('out_of_service_date')->nullable();
            $table->string('legal_name')->nullable();
            $table->string('DBA_name')->nullable();
            $table->string('physical_address')->nullable();
            $table->string('phone')->nullable();
            $table->string('mailing_address')->nullable();
            $table->string('state_carrier_ID_number')->nullable();
            $table->string('MCMXFF')->nullable();
            $table->string('DUNS_number')->nullable();
            $table->string('power_units')->nullable();
            $table->string('drivers')->nullable();
            $table->string('MCS150_form_date')->nullable();
            $table->string('MCS150_mileage_year')->nullable();
            $table->string('operation_classification')->nullable();
            $table->string('carrier_operation')->nullable();
            $table->string('cargo_carried')->nullable();
            $table->string('inspections_vehicle')->nullable();
            $table->string('inspections_driver')->nullable();
            $table->string('inspections_hazmat')->nullable();
            $table->string('inspections_iep')->nullable();
            $table->string('out_of_service_vehicle')->nullable();
            $table->string('out_of_service_driver')->nullable();
            $table->string('out_of_service_hazmat')->nullable();
            $table->string('out_of_service_iep')->nullable();
            $table->string('out_of_service_percent_vehicle')->nullable();
            $table->string('out_of_service_percent_driver')->nullable();
            $table->string('out_of_service_percent_hazmat')->nullable();
            $table->string('out_of_service_percent_iep')->nullable();
            $table->string('nat_average_vehicle')->nullable();
            $table->string('nat_average_driver')->nullable();
            $table->string('nat_average_hazmat')->nullable();
            $table->string('nat_average_iep')->nullable();
            $table->date('rating_date')->nullable();
            $table->date('review_date')->nullable();
            $table->string('rating')->nullable();
            $table->string('type')->nullable();
            $table->string('status_color')->nullable();
            $table->string('hash')->nullable();
             $table->timestamps();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('company_snapshot_fulls');
    }
}
