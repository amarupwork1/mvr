<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterVehicleInvoices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vehicle_invoices', function (Blueprint $table) {
            $table->string('unit_number')->after('id')->nullable();
            $table->string('vin')->after('unit_number')->nullable();
            $table->string('service_provider_type')->after('service_date')->nullable();
            $table->string('service_provider_name')->after('service_provider_type')->nullable();
            $table->string('service_provider_street_address')->after('service_provider_name')->nullable();
            $table->string('service_provider_city')->after('service_provider_street_address')->nullable();
            $table->string('service_provider_state')->after('service_provider_city')->nullable();
            $table->string('service_provider_zip')->after('service_provider_state')->nullable();
            $table->string('service_provider_phone_number')->after('service_provider_zip')->nullable();
            $table->string('odometer_reading')->after('service_provider_phone_number')->nullable();
            $table->string('type_of_work')->after('odometer_reading')->nullable();
            $table->string('dot_annual_inspection')->after('type_of_work')->nullable();
            $table->string('required_monthly_inspection')->after('dot_annual_inspection')->nullable();
            $table->string('required_quarterly_inspection')->after('required_monthly_inspection')->nullable();
            $table->string('oil_change_performed')->after('required_quarterly_inspection')->nullable();
            $table->string('roadside_insp_violation_involved')->after('oil_change_performed')->nullable();
            $table->string('out_of_service_violation')->after('roadside_insp_violation_involved')->nullable();
            $table->string('dvir_documented_defect')->after('out_of_service_violation')->nullable();
            $table->string('citation_issued')->after('dvir_documented_defect')->nullable();
            $table->string('invoice_comments')->after('citation_issued')->nullable();
            $table->integer('invoice_parts_total')->after('invoice_comments')->nullable();
            $table->integer('invoice_labor_total')->after('invoice_parts_total')->nullable();
            $table->integer('invoice_fees_total')->after('invoice_labor_total')->nullable();
            $table->integer('sale_tax')->after('invoice_fees_total')->nullable();
            $table->integer('invoice_total')->after('sale_tax')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
