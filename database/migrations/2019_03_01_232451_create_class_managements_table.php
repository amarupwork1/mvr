<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateClassManagementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('class_managements', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('section')->nullable();
            $table->tinyInteger('description')->nullable();
            $table->text('basic_category')->nullable();
            $table->text('violation_group')->nullable();
            $table->text('class_assigned')->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('class_managements');
    }
}
