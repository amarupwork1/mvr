<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterHosServiceRecords extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('hos_service_records', function (Blueprint $table) {
            $table->string('signing_key')->after('driver_id')->nullable();
            $table->string('document_key')->after('signing_key')->nullable();
            $table->string('auth_token')->after('document_key')->nullable();
            


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
