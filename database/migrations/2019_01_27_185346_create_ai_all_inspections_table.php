<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAiAllInspectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ai_all_inspections', function (Blueprint $table) {
            $table->increments('id');
         
            $table->string('unique_id')->nullable();
            $table->string('report_number')->nullable();
            $table->string('report_state')->nullable();
            $table->string('dot_number')->nullable();
            $table->string('insp_date')->nullable();
            $table->string('insp_level_id')->nullable();
            $table->string('county_code_state')->nullable();
            $table->string('time_weight')->nullable();
            $table->string('driver_oos_total')->nullable();
            $table->string('vehicle_oos_total')->nullable();
            $table->string('total_hazmat_sent')->nullable();
            $table->string('oos_total')->nullable();
            $table->string('hazmat_oos_total')->nullable();
            $table->string('hazmat_placard_req')->nullable();
            $table->string('unit_type_desc')->nullable();
            $table->string('unit_make')->nullable();
            $table->string('unit_license')->nullable();
            $table->string('unit_license_state')->nullable();
            $table->string('vin')->nullable();
            $table->string('unit_decal_number')->nullable();
            $table->string('unit_type_desc2')->nullable();
            $table->string('unit_make2')->nullable();
            $table->string('unit_license2')->nullable();
            $table->string('unit_license_state2')->nullable();
            $table->string('vin2')->nullable();
            $table->string('unit_decal_number2')->nullable();
            $table->string('unsafe_insp')->nullable();
            $table->string('fatigued_insp')->nullable();
            $table->string('dr_fitness_insp')->nullable();
            $table->string('subt_alcohol_insp')->nullable();
            $table->string('vh_maint_insp')->nullable();
            $table->string('hm_insp')->nullable();
            $table->string('basic_viol')->nullable();
            $table->string('unsafe_viol')->nullable();
            $table->string('fatigued_viol')->nullable();
            $table->string('dr_fitness_viol')->nullable();
            $table->string('subt_alcohol_viol')->nullable();
            $table->string('vh_maint_viol')->nullable();
            $table->string('hm_viol')->nullable();
               $table->timestamps();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ai_all_inspections');
    }
}
