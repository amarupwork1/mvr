<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVehiclesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicles', function (Blueprint $table) {
            $table->increments('id');
            
            $table->integer('inspection_id')->nullable();
            $table->string('unit')->nullable();
            $table->string('type')->nullable();
            $table->string('make')->nullable();
            $table->string('state')->nullable();
            $table->string('plate')->nullable();
            $table->string('equipment_id')->nullable();
            $table->string('vin')->nullable();
            $table->string('iep_usdot')->nullable();
            $table->text('meta')->nullable();
			$table->timestamps();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('vehicles');
    }
}
