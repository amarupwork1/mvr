<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAiAllViolationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ai_all_violations', function (Blueprint $table) {
            $table->increments('id');
           
            $table->string('unique_id')->nullable();
            $table->string('insp_date')->nullable();
            $table->string('dot_number')->nullable();
            $table->string('viol_code')->nullable();
            $table->string('basic_desc')->nullable();
            $table->string('oos_indicator')->nullable();
            $table->string('oos_weight')->nullable();
            $table->string('severity_weight')->nullable();
            $table->string('time_weight')->nullable();
            $table->string('tot_severity_wght')->nullable();
            $table->string('viol_value')->nullable();
            $table->string('section_desc')->nullable();
            $table->string('group_desc')->nullable();
            $table->string('viol_unit')->nullable();
            $table->string('notified')->nullable();
             $table->timestamps();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ai_all_violations');
    }
}
