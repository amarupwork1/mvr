<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDataqsReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dataqs_reviews', function (Blueprint $table) {
            $table->increments('id');
        
            $table->integer('account_id')->nullable();
            $table->string('id_')->nullable();
            $table->string('date_entered')->nullable();
            $table->string('type')->nullable();
            $table->string('status')->nullable();
            $table->string('assigned_to_agency')->nullable();
            $table->string('name')->nullable();
            $table->string('requestor_usdot')->nullable();
            $table->string('email_address')->nullable();
            $table->string('company_agency_name')->nullable();
            $table->string('report_state')->nullable();
            $table->string('report_number')->nullable();
            $table->string('state')->nullable();
            $table->string('date')->nullable();
            $table->string('carrier_name')->nullable();
            $table->string('usdot')->nullable();
            $table->string('address')->nullable();
            $table->string('mc_mx')->nullable();
            $table->string('driver_name')->nullable();
            $table->string('violation')->nullable();
            $table->string('violation_description')->nullable();
            $table->text('explanation')->nullable();
            $table->string('view_information')->nullable();
                $table->timestamps();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('dataqs_reviews');
    }
}
