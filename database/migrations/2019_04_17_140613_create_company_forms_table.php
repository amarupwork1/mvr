<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCompanyFormsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_forms', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->text('title')->nullable();
            $table->text('menu')->nullable();
            $table->longText('text')->nullable();
            $table->longText('meta')->nullable();
            $table->text('template')->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('company_forms');
    }
}
