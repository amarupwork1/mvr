<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRoadtestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roadtests', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->date('date')->nullable();
            $table->integer('driver_id')->nullable();
            $table->string('ssn')->nullable();
            $table->string('class')->nullable();
            $table->text('equipment_driven')->nullable();
            $table->text('trailer')->nullable();
            $table->string('length_of_test')->nullable();
            $table->string('mi_from')->nullable();
            $table->string('mi_to')->nullable();
            $table->string('start_time')->nullable();
            $table->string('finish_time')->nullable();
            $table->string('weather_conditions')->nullable();
            $table->smallInteger('check_condition')->nullable();
            $table->smallInteger('check_fuel')->nullable();
            $table->smallInteger('check_around_unit')->nullable();
            $table->smallInteger('testing_steering')->nullable();
            $table->smallInteger('check_horn')->nullable();
            $table->smallInteger('check_instruments')->nullable();
            $table->smallInteger('check_dashboard')->nullable();
            $table->smallInteger('clean_windshield')->nullable();
            $table->smallInteger('reviews_signs')->nullable();
            $table->smallInteger('connect_glad_before')->nullable();
            $table->smallInteger('connect_glad_light')->nullable();
            $table->smallInteger('couples_no_difficulty')->nullable();
            $table->smallInteger('raises_landing')->nullable();
            $table->smallInteger('check_kingpin')->nullable();
            $table->smallInteger('check_coupling')->nullable();
            $table->smallInteger('support_trailer_before')->nullable();
            $table->smallInteger('places_transmission')->nullable();
            $table->smallInteger('start_engine')->nullable();
            $table->smallInteger('check_instruments_intervals')->nullable();
            $table->smallInteger('engine_rpm')->nullable();
            $table->smallInteger('know_tractor_protection')->nullable();
            $table->smallInteger('tests_service_brakes')->nullable();
            $table->smallInteger('build_air_pressure_before')->nullable();
            $table->smallInteger('start_unit_moving')->nullable();
            $table->smallInteger('clutch_properly')->nullable();
            $table->smallInteger('adjusts_speed_headlights')->nullable();
            $table->smallInteger('dims_lights')->nullable();
            $table->smallInteger('check_area_before')->nullable();
            $table->smallInteger('utilizes_mirrors')->nullable();
            $table->smallInteger('signals_backing')->nullable();
            $table->smallInteger('avoid_backing_blind')->nullable();
            $table->smallInteger('parks_without_hitting')->nullable();
            $table->smallInteger('parks_correct_distance')->nullable();
            $table->smallInteger('secures_unit_properly')->nullable();
            $table->smallInteger('enters_traffic_parked')->nullable();
            $table->smallInteger('parks_pavement')->nullable();
            $table->smallInteger('park_secures_unit_properly')->nullable();
            $table->smallInteger('emergency_warning_signal')->nullable();
            $table->smallInteger('slowing_clutch_gears')->nullable();
            $table->smallInteger('slowing_gears_down_before')->nullable();
            $table->smallInteger('slowing_hills_start')->nullable();
            $table->smallInteger('slowing_test_brakes')->nullable();
            $table->smallInteger('slowing_uses_brakes')->nullable();
            $table->smallInteger('slowing_use_mirrors')->nullable();
            $table->smallInteger('slowing_plans_stop')->nullable();
            $table->smallInteger('slowing_braking_stops')->nullable();
            $table->smallInteger('turning_signals_intention')->nullable();
            $table->smallInteger('turning_proper_lane')->nullable();
            $table->smallInteger('turning_checks_traffic')->nullable();
            $table->smallInteger('turning_restricts_traffic')->nullable();
            $table->smallInteger('turning_complete_turn')->nullable();
            $table->smallInteger('traffic_plans_stop')->nullable();
            $table->smallInteger('traffic_obeys')->nullable();
            $table->smallInteger('traffic_complete_stop')->nullable();
            $table->smallInteger('intersection_yields')->nullable();
            $table->smallInteger('intersection_cross_traffic')->nullable();
            $table->smallInteger('intersection_prepared_stop')->nullable();
            $table->smallInteger('crossing_minimum')->nullable();
            $table->smallInteger('crossing_proper_gear')->nullable();
            $table->smallInteger('crossing_rules')->nullable();
            $table->smallInteger('passing_sufficient_space')->nullable();
            $table->smallInteger('passing_safe_locations')->nullable();
            $table->smallInteger('passing_changing_lanes')->nullable();
            $table->smallInteger('passing_warns')->nullable();
            $table->smallInteger('passing_sufficient_speed')->nullable();
            $table->smallInteger('passing_return_right_lane')->nullable();
            $table->smallInteger('speed_observes_limits')->nullable();
            $table->smallInteger('speed_consistent_ability')->nullable();
            $table->smallInteger('speed_adjust')->nullable();
            $table->smallInteger('speed_down')->nullable();
            $table->smallInteger('speed_constant')->nullable();
            $table->smallInteger('safety_yields')->nullable();
            $table->smallInteger('safety_strives')->nullable();
            $table->smallInteger('safety_faster_pass')->nullable();
            $table->smallInteger('safety_horn')->nullable();
            $table->smallInteger('miscellaneous_alert')->nullable();
            $table->smallInteger('miscellaneous_traffic_conditions')->nullable();
            $table->smallInteger('miscellaneous_anticipates_problems')->nullable();
            $table->smallInteger('miscellaneous_notaking_eye')->nullable();
            $table->smallInteger('miscellaneous_checks_instruments')->nullable();
            $table->smallInteger('miscellaneous_professional_appearance')->nullable();
            $table->smallInteger('miscellaneous_calm_pressure')->nullable();
            $table->text('remarks')->nullable();
            $table->string('general_performance')->nullable();
            $table->text('performance_explain')->nullable();
            $table->string('qualified_for')->nullable();
            $table->string('special_equipment')->nullable();
            $table->text('signature')->nullable();
            $table->text('source')->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('roadtests');
    }
}
