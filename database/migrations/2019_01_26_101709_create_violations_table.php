<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateViolationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('violations', function (Blueprint $table) {
            $table->increments('id');
           
            $table->integer('inspection_id')->nullable();
            $table->string('unit')->nullable();
            $table->string('violation_code')->nullable();
            $table->string('oos')->nullable();
            $table->string('violations_category')->nullable();
            $table->text('violations_discovered')->nullable();
            $table->string('state_citation_number')->nullable();
            $table->string('state_citation_result')->nullable();
            $table->string('certify')->nullable();
            $table->string('post_crash')->nullable();
            $table->string('iep')->nullable();
            $table->integer('notified')->nullable();
             $table->timestamps();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('violations');
    }
}
