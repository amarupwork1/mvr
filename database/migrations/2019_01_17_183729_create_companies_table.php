<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('usdot')->nullable();           
          /*  $table->string('company_name')->nullable();
            $table->string('company_email')->nullable();*/
            $table->string('legal_name')->nullable();
            $table->string('dba_name')->nullable();
            $table->integer('zip_code')->nullable();
            $table->text('physical_address')->nullable();
            $table->text('mailing_address')->nullable();
            $table->string('phone')->nullable();
            $table->string('mobile')->nullable();
            $table->string('company_fax')->nullable();
            $table->string('owner_name')->nullable();
            $table->string('owner_phone')->nullable();
            $table->string('owner_email')->nullable();
            $table->string('ein')->nullable();
            $table->text('fields')->nullable();
            $table->text('lmc_url')->nullable();
            $table->string('lmc_username')->nullable();
            $table->string('lmc_password')->nullable();
            $table->text('twc_url')->nullable();
            $table->string('twc_username')->nullable();
            $table->string('twc_password')->nullable();
            $table->text('eld_url')->nullable();
            $table->string('eld_username')->nullable();
            $table->string('eld_password')->nullable();
            $table->text('mw1_url')->nullable();
            $table->string('mw1_username')->nullable();
            $table->string('mw1_password')->nullable();
            $table->text('mw2_url')->nullable();
            $table->string('mw2_username')->nullable();
            $table->string('mw2_password')->nullable();
            $table->text('mw3_url')->nullable();
            $table->string('mw3_username')->nullable();
            $table->string('mw3_password')->nullable();

           
            $table->timestamps();
          
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('companies');
    }
}
