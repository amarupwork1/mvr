<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAccidentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accidents', function (Blueprint $table) {
            $table->increments('id');
            $table->string('us_dot')->nullable();
            $table->integer('company_id')->nullable();
            $table->integer('driver_id')->nullable();
            $table->date('date_of_accident')->nullable();
            $table->time('time_of_accident')->nullable();
            $table->date('date_accident_reported')->nullable();
            $table->time('time_accident_reported')->nullable();
            $table->string('street_location_of_accident')->nullable();
            $table->string('city_location_of_accident')->nullable();
            $table->string('state_location_of_accident')->nullable();
            $table->string('weather_condition_at_time_accident')->nullable();
            $table->string('light_condition_at_time_accident')->nullable();
            $table->integer('number_of_vehicles_involved')->nullable();
            $table->string('carrier_truck_unit_number_involved')->nullable();
            $table->string('carrier_trailer_unit_number_involved')->nullable();
            $table->string('DOT_recordable_accident')->nullable();
            $table->string('number_of_facilities')->nullable();
            $table->string('number_of_injuries')->nullable();
            $table->string('number_of_vehicles_towed')->nullable();
            $table->string('hazmat_matrial')->nullable();
            $table->string('hazmat_spilled')->nullable();
            $table->string('federal_MCMIS_report_number')->nullable();
            $table->string('police_report_number')->nullable();
            $table->string('internal_claim_number')->nullable();
            $table->string('police_report_obtained')->nullable();
            $table->string('facilities_within_24_hours')->nullable();
            $table->string('transport_of_medical_attention')->nullable();
            $table->string('tow_away_from_scene')->nullable();
            $table->string('moving_citation_issued')->nullable();
            $table->string('citation_written')->nullable();
            $table->string('post_accidental_drug')->nullable();
            $table->string('breath_alcohal_test')->nullable();
            $table->string('breath_alcohal_test_8_hours')->nullable();
            $table->string('urine_drug_test_completed')->nullable();
            $table->string('crash_preventable')->nullable();
            $table->string('preventable_decision')->nullable();
            $table->timestamps();

            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('accidents');
    }
}
