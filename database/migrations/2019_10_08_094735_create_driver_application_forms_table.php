<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDriverApplicationFormsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('driver_application_forms', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->date('date_of_signed')->nullable();
            $table->string('first_name')->nullable();
            $table->string('middle_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('suffix')->nullable();
            $table->string('physical_street_address')->nullable();
            $table->string('physical_street_address_2')->nullable();
            $table->string('physical_city')->nullable();
            $table->string('physical_state')->nullable();
            $table->string('physical_zip')->nullable();
            $table->string('physical_country')->nullable();
            $table->string('mailing_street_addres')->nullable();
            $table->string('mailing_street_addres2')->nullable();
            $table->string('mailing_city')->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('driver_application_forms');
    }
}
