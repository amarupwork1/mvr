<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAiCarriersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ai_carriers', function (Blueprint $table) {
            $table->increments('id');
           
            $table->string('dot_number')->nullable();
            $table->string('insp_total')->nullable();
            $table->string('driver_insp_total')->nullable();
            $table->string('driver_oos_insp_total')->nullable();
            $table->string('vehicle_insp_total')->nullable();
            $table->string('vehicle_oos_insp_total')->nullable();
            $table->string('unsafe_driv_insp_w_viol')->nullable();
            $table->string('unsafe_driv_measure')->nullable();
            $table->string('unsafe_driv_ac')->nullable();
            $table->string('hos_driv_insp_w_viol')->nullable();
            $table->string('hos_driv_measure')->nullable();
            $table->string('hos_driv_ac')->nullable();
            $table->string('driv_fit_insp_w_viol')->nullable();
            $table->string('driv_fit_measure')->nullable();
            $table->string('driv_fit_ac')->nullable();
            $table->string('contr_subst_insp_w_viol')->nullable();
            $table->string('contr_subst_measure')->nullable();
            $table->string('contr_subst_ac')->nullable();
            $table->string('veh_maint_insp_w_viol')->nullable();
            $table->string('veh_maint_measure')->nullable();
            $table->string('veh_maint_ac')->nullable();
             $table->timestamps();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ai_carriers');
    }
}
