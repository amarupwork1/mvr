<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDataqsResponsesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dataqs_responses', function (Blueprint $table) {
            $table->increments('id');
        
            $table->string('review_id')->nullable();
            $table->date('response_date')->nullable();
            $table->string('status_changed_to')->nullable();
            $table->string('status_prior_to_response')->nullable();
            $table->string('entered_by')->nullable();
            $table->string('phone_number')->nullable();
            $table->text('trooper_comment')->nullable();
                $table->timestamps();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('dataqs_responses');
    }
}
