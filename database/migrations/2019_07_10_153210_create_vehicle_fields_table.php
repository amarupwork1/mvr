<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVehicleFieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicle_fields', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('owner_dot')->nullable();
            $table->string('operating_carrier_dot')->nullable();
            $table->string('owner_type')->nullable();
            $table->string('unit_number')->nullable();
            $table->string('license_state')->nullable();
            $table->string('license_number')->nullable();
            $table->string('year')->nullable();
            $table->string('make')->nullable();
            $table->string('vin')->nullable();
            $table->string('equipment_type')->nullable();
            $table->string('tire_size')->nullable();
            $table->string('vehicle_weight')->nullable();
            $table->string('gvwr')->nullable();
            $table->string('annual_inspection_date')->nullable();
            $table->string('pm_cycle_last_performed')->nullable();
            $table->string('pm_cycle_next_due')->nullable();
            $table->string('last_pm_mileage')->nullable();
            $table->string('next_pm_mileage')->nullable();
            $table->string('last_pm_date')->nullable();
            $table->string('pm_cycle_days')->nullable();
            $table->string('next_pm_date')->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('vehicle_fields');
    }
}
