<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePspDiscreleasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('psp_discreleases', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('company')->nullable();
            $table->string('preferred_company_name')->nullable();
            $table->string('authorization_company_name')->nullable();
            $table->date('date_psp_release_signed')->nullable();
            $table->string('psp_release_signature')->nullable();
            $table->string('applicant_printed_name')->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('psp_discreleases');
    }
}
