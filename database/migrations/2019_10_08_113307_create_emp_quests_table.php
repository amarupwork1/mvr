<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEmpQuestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('emp_quests', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('company_id')->nullable();
            $table->string('employer_name')->nullable();
            $table->date('signed_date')->nullable();
            $table->string('mailing_address')->nullable();
            $table->string('mailing_city')->nullable();
            $table->string('mailing_state')->nullable();
            $table->string('mailing_zip')->nullable();
            $table->integer('telephone')->nullable();
            $table->string('fax_number')->nullable();
            $table->string('email_address')->nullable();
            $table->string('printed_name')->nullable();
            $table->string('preffered_company_name')->nullable();
            $table->string('prev_emp_release_signature')->nullable();
            $table->date('date_prev_emp_release_signature')->nullable();
            $table->string('requested_company_name')->nullable();
            $table->string('requested_address')->nullable();
            $table->string('requested_city')->nullable();
            $table->string('requested_state')->nullable();
            $table->string('requested_zip')->nullable();
            $table->string('requested_telephone_number')->nullable();
            $table->string('requested_fax_number')->nullable();
            $table->string('requested_contact_person')->nullable();
            $table->string('requested_email_address')->nullable();
            $table->string('requested_printed_name')->nullable();
            $table->string('job_applying_for')->nullable();
            $table->string('inquiry_printed_name')->nullable();
            $table->date('inquiry_work_from')->nullable();
            $table->date('inquiry_work_to')->nullable();
            $table->string('if_no_explain')->nullable();
            $table->string('is_company_driver')->nullable();
            $table->string('is_owener')->nullable();
            $table->string('is_other')->nullable();
            $table->string('truck_type')->nullable();
            $table->string('commodities_transported')->nullable();
            $table->string('area_of_operation')->nullable();
            $table->string('is_accidents')->nullable();
            $table->string('yes_accident_desc')->nullable();
            $table->string('reason_for_leaving')->nullable();
            $table->string('re_employ')->nullable();
            $table->string('re_employ_no_explain')->nullable();
            $table->string('additional_comments')->nullable();
            $table->string('greater_alcohol_test')->nullable();
            $table->date('date_if_alcohol_test_yes')->nullable();
            $table->string('substances_test_results')->nullable();
            $table->date('date_substances_test_results_yes')->nullable();
            $table->string('refusals_tested')->nullable();
            $table->date('date_refusals_tested_yes')->nullable();
            $table->string('was_rehabilitation_complete')->nullable();
            $table->string('date_was_rehabilitation_complete_yes')->nullable();
            $table->string('name')->nullable();
            $table->string('title')->nullable();
            $table->date('company')->nullable();
            $table->string('date')->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('emp_quests');
    }
}
