<div class="form-group row justify-content-center  {{ $errors->has('report_provided_by') ? 'has-error' : ''}}">
    {!! Form::label('report_provided_by', 'Report Provided By', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('report_provided_by', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        {!! $errors->first('report_provided_by', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group row justify-content-center  {{ $errors->has('date_completed') ? 'has-error' : ''}}">
    {!! Form::label('date_completed', 'Date Completed', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('date_completed', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        {!! $errors->first('date_completed', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group row justify-content-center  {{ $errors->has('customer_sub') ? 'has-error' : ''}}">
    {!! Form::label('customer_sub', 'Customer Sub', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('customer_sub', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        {!! $errors->first('customer_sub', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group row justify-content-center  {{ $errors->has('customer_ref') ? 'has-error' : ''}}">
    {!! Form::label('customer_ref', 'Customer Ref', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('customer_ref', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        {!! $errors->first('customer_ref', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group row justify-content-center  {{ $errors->has('first_name') ? 'has-error' : ''}}">
    {!! Form::label('first_name', 'First Name', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('first_name', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        {!! $errors->first('first_name', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group row justify-content-center  {{ $errors->has('mvr_status') ? 'has-error' : ''}}">
    {!! Form::label('mvr_status', 'Mvr Status', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('mvr_status', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        {!! $errors->first('mvr_status', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group row justify-content-center  {{ $errors->has('last_name') ? 'has-error' : ''}}">
    {!! Form::label('last_name', 'Last Name', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('last_name', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        {!! $errors->first('last_name', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group row justify-content-center  {{ $errors->has('customer') ? 'has-error' : ''}}">
    {!! Form::label('customer', 'Customer', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('customer', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        {!! $errors->first('customer', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group row justify-content-center  {{ $errors->has('license') ? 'has-error' : ''}}">
    {!! Form::label('license', 'License', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('license', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        {!! $errors->first('license', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group row justify-content-center  {{ $errors->has('accident_info') ? 'has-error' : ''}}">
    {!! Form::label('accident_info', 'Accident Info', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::textarea('accident_info', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        {!! $errors->first('accident_info', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group row justify-content-center  {{ $errors->has('actor') ? 'has-error' : ''}}">
    {!! Form::label('actor', 'Actor', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('actor', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        {!! $errors->first('actor', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group row justify-content-center  {{ $errors->has('state') ? 'has-error' : ''}}">
    {!! Form::label('state', 'State', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('state', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        {!! $errors->first('state', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group row justify-content-center  {{ $errors->has('license_info') ? 'has-error' : ''}}">
    {!! Form::label('license_info', 'License Info', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::textarea('license_info', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        {!! $errors->first('license_info', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group row justify-content-center  {{ $errors->has('driver_info') ? 'has-error' : ''}}">
    {!! Form::label('driver_info', 'Driver Info', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::textarea('driver_info', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        {!! $errors->first('driver_info', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group row justify-content-center">
    <div class="col-lg-4 col-12 align-content-center">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
    </div>
</div>
