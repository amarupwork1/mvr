@extends('layouts.app')

@section('content')
    <div class="container-fluid bg-white mt-5">
        <div class="row">
            <div class="col-md-12">
                <div class="white-box">
                    <h3 class="box-title pull-left">Create New MVR Report</h3>
                    @can('view-'.str_slug('Mvr_report'))
                        <a class="btn btn-success pull-right" href="{{url('/mvr_reports')}}">
                            <i class="icon-arrow-left-circle"></i> View Mvr_report</a>
                    @endcan
                    <div class="clearfix"></div>
                    <hr>
                    @if ($errors->any())
                        <ul class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif

                    {!! Form::open(['url' => '/mvr_reports', 'files' => true]) !!}

                    @include ('mvr_reports.form')

                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
@endsection
