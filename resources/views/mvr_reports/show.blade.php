@extends('layouts.app')

@section('content')
    <div class="container-fluid bg-white mt-5">
        <!-- .row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                     <h5> Report Provided By 
                      {{ $mvr_report->report_provided_by }} </h5>
                      <hr>
                    <h3 style="    font-size: 33px; padding-bottom: 13px;" class="box-title pull-left">MVR (Motor Vehicle Report)</h3>
                    @can('view-'.str_slug('Mvr_report'))
                        <a class="btn btn-success pull-right" href="{{ url('/mvr_reports') }}">
                            <i class="icon-arrow-left-circle" aria-hidden="true"></i> Back</a>
                    @endcan
                    <div class="clearfix"></div>
                    <hr>

                    <div class="col-md-6">
                    <div class="table-responsive">
                        <table class="table table">
                         <tbody>
                     
                            <tr>
                                <th> Date Completed </th>
                                <td> {{ $mvr_report->date_completed }} </td>
                            </tr>
                            <tr>
                                <th> Customer Sub </th>
                                <td> {{ $mvr_report->customer_sub }} </td>
                            </tr>

                            <tr> <th>Customer Ref</th>
                                <td>{{$mvr_report->customer_ref}} </td>
                            </tr>

                            <tr> <th>First Name</th>
                                <td>{{$mvr_report->first_name}} </td>
                            </tr>

                            <tr> <th>MVR Status</th>
                                <td>{{$mvr_report->mvr_status}} </td>
                            </tr>

                            <tr> <th>Last Name</th>
                                <td>{{$mvr_report->last_name}} </td>
                            </tr>

                            <tr> <th>Customer</th>
                                <td>{{$mvr_report->customer}} </td>
                            </tr>

                            <tr> <th>License</th>
                                <td>{{$mvr_report->license}} </td>
                            </tr>

                            <tr> <th>Accident Info</th>
                                <td>{{$mvr_report->accident_info}} </td>
                            </tr>

                            <tr> <th>Actor</th>
                                <td>{{$mvr_report->actor}} </td>
                            </tr>

                            <tr> <th>State</th>
                                <td>{{$mvr_report->state}} </td>
                            </tr>

                            <tr> <th>License Info</th>
                                <td>{{$mvr_report->license_info}} </td>
                            </tr>

                            <tr> <th>Driver Info</th>
                                <td>{{$mvr_report->driver_info}} </td>
                            </tr>
                    </tbody>
                </table>
            </div>

        </div>

          
         <div class="col-md-5">
                    <div class="table-responsive">
                        <table class="table table">
                         <tbody>
                     
                            <tr>
                                <th> Date Completed </th>
                                <td> {{ $mvr_report->date_completed }} </td>
                            </tr>
                            <tr>
                                <th> Customer Sub </th>
                                <td> {{ $mvr_report->customer_sub }} </td>
                            </tr>

                            <tr> <th>Customer Ref</th>
                                <td>{{$mvr_report->customer_ref}} </td>
                            </tr>

                            <tr> <th>First Name</th>
                                <td>{{$mvr_report->first_name}} </td>
                            </tr>

                            <tr> <th>MVR Status</th>
                                <td>{{$mvr_report->mvr_status}} </td>
                            </tr>

                            <tr> <th>Last Name</th>
                                <td>{{$mvr_report->last_name}} </td>
                            </tr>

                            <tr> <th>Customer</th>
                                <td>{{$mvr_report->customer}} </td>
                            </tr>

                            <tr> <th>License</th>
                                <td>{{$mvr_report->license}} </td>
                            </tr>

                            <tr> <th>Accident Info</th>
                                <td>{{$mvr_report->accident_info}} </td>
                            </tr>

                            <tr> <th>Actor</th>
                                <td>{{$mvr_report->actor}} </td>
                            </tr>

                            <tr> <th>State</th>
                                <td>{{$mvr_report->state}} </td>
                            </tr>

                            <tr> <th>License Info</th>
                                <td>{{$mvr_report->license_info}} </td>
                            </tr>

                            <tr> <th>Driver Info</th>
                                <td>{{$mvr_report->driver_info}} </td>
                            </tr>
                    </tbody>
                </table>
            </div>

        </div>
            <div class="table-responsive">


@isset($mvr_report->misc)
<h3 class="box-title pull-left">Miscellaneous / STATE SPECIFICINFORMATION</h3>
  <table class="table table">
     <tbody>



            <tr>
                    <th>cdl medical self certification</th> 
                    <td>{{$mvr_report->misc->cdl_medical_self_certification}}</td>
            </tr>
            <tr>
                    <th>medical examiner specialty</th> 
                    <td>{{$mvr_report->misc->medical_examiner_specialty}}</td>
            </tr>
            <tr>
                    <th>examiner juridiction</th> 
                    <td>{{$mvr_report->misc->examiner_juridiction}}</td>
            </tr>
            <tr>
                    <th>medical restriction</th> 
                    <td>{{$mvr_report->misc->medical_restriction}}</td>
            </tr>
            <tr>
                    <th>medical examiner</th> 
                    <td>{{$mvr_report->misc->medical_examiner}}</td>
            </tr>
            <tr>
                    <th>expiration date</th> 
                    <td>{{$mvr_report->misc->expire_date}}</td>
            </tr>
            <tr>
                    <th>issue date</th> 
                    <td>{{$mvr_report->misc->issue_date}}</td>
            </tr>
            <tr>
                    <th>registry</th> 
                    <td>{{$mvr_report->misc->registry}}</td>
            </tr>
            <tr>
                    <th>license</th> 
                    <td>{{$mvr_report->misc->license}}</td>
            </tr>
            <tr>
                    <th>status</th> 
                    <td>{{$mvr_report->misc->status}}</td>
            </tr>
            <tr>
                    <th>phone</th> 
                    <td>{{$mvr_report->misc->phone}}</td>
            </tr>
            </tbody>
                        </table>
            @endisset

            @isset($mvr_report->driver)
            
               
                    <h3 class="box-title pull-left">Driver </h3>
                    <table class="table table">
                        <tbody>

                            <tr> <th>Driver Name </th> <td>{{ $mvr_report->driver->driver_name }}</td></tr>
                            <tr> <th>Social security No </th> <td>{{ $mvr_report->driver->social_sec_no }}</td></tr>
                            <tr> <th>Date of Birth </th> <td>{{ $mvr_report->driver->dob }}</td></tr>
                        </tbody>
                    </table>

            

            @endisset


            @isset($mvr_report->violations)
            
                @foreach($mvr_report->violations as $violations)
                    <h3 class="box-title pull-left">Violation {{$loop->iteration}}</h3>
                    <table class="table table">
                        <tbody>

                            <tr> <th>location </th> <td>{{ $violations->location }}</td></tr>
                            <tr> <th>reason </th> <td>{{ $violations->reason }}</td></tr>
                            <tr> <th>dates </th> <td>{{ $violations->dates }}</td></tr>
                            <tr> <th>veh </th> <td>{{ $violations->veh }}</td></tr>
                        </tbody>
                    </table>

                @endforeach

            @endisset


                            
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

