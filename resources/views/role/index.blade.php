@extends('layouts.app')



@section('content')
    <div class="container-fluid bg-white mt-5">
        <!-- .row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    <h3 class="box-title pull-left">Role Management</h3>
                    <a style="display: none;" class="btn btn-success pull-right" href="{{url('role/create')}}"><i class="icon-plus"></i> Add
                        Role</a>
                    <div class="clearfix"></div>
                    <hr>
                    <div class="row  justify-content-center">
                        <div class="col-lg-12 col-12 mt-5 align-self-center">
                            <div class="table-responsive">
                                <table id="myTable" class="table table-striped table-borderless">
                                    <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Name</th>
                                        <th width="220px">Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($roles as $key=>$role)
                                        <tr>
                                            <td>{{$key+1}}</td>
                                            <td>{{ucfirst($role->name)}}</td>
                                            <th>
                                                <a class="btn btn-info btn-sm" href="{{url('role/edit/'.$role->id)}}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                                                &nbsp;&nbsp;
                                                <a class="delete1 btn btn-danger btn-sm"
                                                   href="{{url('role/delete/'.$role->id)}}"><i class="fa fa-trash-o"></i> Delete</a>
                                            </th>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@push('js')
   

    <script>
       
      $(function() {
        $('#myTable').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{url()->current()}}",
               columns: [
            
            {data: 'id', name: 'id'},
            {data: 'name',name: 'name'},
            
            {data: 'action',name: 'action'},

           
        ]

        });
    }); 
    </script>

@endpush