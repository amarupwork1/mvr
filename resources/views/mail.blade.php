<h1>Hi, {{ $name }}</h1>
<p>Click the the button below to reset your password.</p>

<p>Someone hopefully you has requested a password reset on your account. If it is you go ahead and reset your password, if not please ignore this email.</p>

<p>Email: {{ $email }}</p>

<p><a target="_blank" rel="noopener noreferrer" href="{{ $resetLink }}"  style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; border-radius: 3px; box-shadow: 0 2px 3px rgba(0, 0, 0, 0.16); color: #FFF; display: inline-block; text-decoration: none; -webkit-text-size-adjust: none; background-color: #3097D1; border-top: 10px solid #3097D1; border-right: 18px solid #3097D1; border-bottom: 10px solid #3097D1; border-left: 18px solid #3097D1;">Reset Password</a></p>

<p>Thanks</p>