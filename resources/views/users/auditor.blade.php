@extends('layouts.app')

@push('before-css')
    <link href="{{asset('plugins/components/datatables/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet"
          type="text/css"/>
@endpush

@section('content')



    <div class="container-fluid bg-white mt-5">
        <!-- .row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    <h3 class="box-title pull-left">@if (isset($data['title']) and !(empty($data['title']))) {{$data['title']}}  @else Users @endif List</h3>
                    <a  class="btn btn-success pull-right" href="{{url('user/create/auditor')}}"><i class="icon-plus"></i> Add user</a>
                    <div class="clearfix"></div>
                    <hr>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table id="myTable" class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Role</th>
                                        <th>Company</th>
                                        <th>Updated at</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($users as $key=>$user)
                                        <tr>
                                            <td>{{$key+1}}</td>
                                            <td>{{ucwords($user->name)}}</td>
                                            <td>{{ucwords($user->roles()->pluck('name')->implode(', '))}}</td>
                                            <td></td>
                                            <td>{{$user->updated_at->diffForHumans()}}</td>
                                            <td>{{$user->created_at->diffForHumans()}}</td>
                                            <th>
                                                <a class="btn btn-info" href="{{url('user/edit/'.$user->id)}}"><i class="fa fa-pencil"></i> Edit</a> &nbsp;&nbsp;
                                                <a class="delete btn btn-danger" href="{{url('user/delete/'.$user->id)}}"><i class="fa fa-trash"></i> Delete</a>
                                            </th>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>
@endsection
@push('js')
   

    <script>
       
      $(function() {
        $('#myTable').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{url()->current()}}",
               columns: [
            
          
            {data: 'id',name: 'id'},
            {data: 'name',name: 'name'},
            {data: 'email',name: 'email'},
            {data: 'roles',name: 'roles'},
            {data: 'company',name: 'company'},
            {data: 'updated_at',name: 'updated_at'},
            {data: 'action',name: 'action'},

           
        ]

        });
    }); 
    </script>

@endpush