@extends('layouts.app')

@push('before-css')
    <link href="{{asset('plugins/components/datatables/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet"
          type="text/css"/>
@endpush

@section('content')



    <div class="container-fluid bg-white mt-5">
        <!-- .row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    <h3 class="box-title pull-left">@if (isset($data['title']) and !(empty($data['title']))) {{$data['title']}}  @else Users @endif List</h3>
                    <a  class="btn btn-success pull-right" href="{{url('user/create')}}"><i class="icon-plus"></i> Add user</a>
                    <div class="clearfix"></div>
                    <hr>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table id="myTable" class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Role</th>
                                        <th>Created at</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($users as $key=>$user)
                                        <tr>
                                            <td>{{$key+1}}</td>
                                            <td>{{ucwords($user->name)}}</td>
                                            <td>{{$user->email}}</td>
                                            <td>{{ucwords($user->roles()->pluck('name')->implode(', '))}}</td>
                                            <td>{{$user->created_at->diffForHumans()}}</td>
                                            <th>
                                                <a class="btn btn-info" href="{{url('user/edit/'.$user->id)}}"><i class="fa fa-pencil-alt"></i> Edit</a> &nbsp;&nbsp;
                                                <a class="delete btn btn-danger" href="{{url('user/delete/'.$user->id)}}"><i class="fa fa-trash"></i> Delete</a>
                                            </th>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>
@endsection
@push('js')
   

    <script>
       
      $(function() {
        $('#myTable').DataTable({
        });
    }); 
    </script>

@endpush