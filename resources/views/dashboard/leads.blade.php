    @extends('layouts.app')

@push('before-css')
    <!-- chartist CSS -->
    <link href="{{asset('plugins/vendors/morrisjs/morris.css')}}" rel="stylesheet">
    <!--c3 CSS -->
    <link href="{{asset('plugins/vendors/c3-master/c3.min.css')}}" rel="stylesheet">
    <!--Toaster Popup message CSS -->
    <link href="{{asset('plugins/vendors/toast-master/css/jquery.toast.css')}}" rel="stylesheet">
    <!-- Dashboard server Page CSS -->
    <link href="{{asset('assets/css/pages/dashboard-server.css')}}" rel="stylesheet">
@endpush

@section('content')
    <div class="container-fluid" id="dashboard">
        <!-- ============================================================== -->
        <!-- Info box -->
        <!-- ============================================================== -->
        <div class="row">
            <!-- Column -->
            <div class="col-lg-4 col-md-6">
                <a href="{{url('lead')}}">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex pt-3 pb-2 no-block">
                            <div class="align-self-center mr-3 knob-icon">
                                <input class="dial"  data-plugin="knob" data-width="70" data-height="70"  data-linecap="round" data-fgColor="#f95476" data-thickness=".2" value="85" />

                                <i class=" fas fa-question-circle text-pink"></i> </div>
                            <div class="align-slef-center mr-auto">
                               
                                <h2 class="m-b-0 text-uppercase font-18 font-medium lp-5"> Lead</h2>
                                <h6 class="text-light m-b-0">Total Leads <strong>{{ $data['leads'] }}</strong></h6>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
            </div>
            <!-- Column -->
            <!-- Column -->
            <div class="col-lg-4 col-md-6">
                <a href="{{url('lead_response')}}">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex pt-3 pb-2 no-block">
                            <div class="align-self-center mr-3 knob-icon knob-icon-23">
                                <input  class="dial"  data-plugin="knob" data-width="70" data-height="70"  data-linecap="round" data-fgColor="#4886ff " data-thickness=".2" value="40" />
                             
                                <i class="fas fa-trophy  font-25 text-primary"></i> </div>
                            <div class="align-slef-center mr-auto">
                                

                                <h2 class="m-b-0 text-uppercase font-18 font-medium lp-5"> Lead responses</h2>
                                <h6 class="text-light m-b-0">Total Lead Responses <strong> {{ $data['lead_response'] }} </strong></h6>


                            </div>
                        </div>
                    </div>
                </div>
            </a>
            </div>
            <!-- Column -->
            <!-- Column -->
            <div class="col-lg-4 col-md-12">
              <a href="{{url('insurance-information')}}">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex pt-3 pb-2 no-block">
                            <div class="align-self-center mr-3 knob-icon knob-icon-23">
                                <input class="dial"   data-plugin="knob" data-width="70" data-height="70"  data-linecap="round" data-fgColor="#ffb74e" data-thickness=".2" value="89" />
                                <i class="fas fa-sticky-note font-25 text-warning t-23 l-25"></i> </div>
                            <div class="align-slef-center mr-auto">
                                

                                <h2 class="m-b-0 text-uppercase font-18 font-medium lp-5"> Insurance Leads</h2>
                                <h6 class="text-light m-b-0">Total Insurance Leads<strong> {{ $data['InsuranceInformation'] }} </strong></h6>

                            </div>
                        </div>
                    </div>
                </div>
            </a>
            </div>
            <!-- Column -->
        </div>

           

   
 
    </div>
@endsection

@push('js')
    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->
    <!--c3 JavaScript -->
    <script src="{{asset('plugins/vendors/d3/d3.min.js')}}"></script>
    <script src="{{asset('plugins/vendors/c3-master/c3.min.js')}}"></script>
    <!--jquery knob -->
    <script src="{{asset('plugins/vendors/knob/jquery.knob.js')}}"></script>
    <!--Sparkline JavaScript -->
    <script src="{{asset('plugins/vendors/sparkline/jquery.sparkline.min.js')}}"></script>
    <!--Morris JavaScript -->
    <script src="{{asset('plugins/vendors/raphael/raphael-min.js')}}"></script>
    <script src="{{asset('plugins/vendors/morrisjs/morris.js')}}"></script>
    <!-- Popup message jquery -->
    <script src="{{asset('plugins/vendors/toast-master/js/jquery.toast.js')}}"></script>
    <!-- Dashboard JS -->
    <script src="{{asset('assets/js/dashboard-server.js')}}"></script>
    <script src="{{asset('assets/js/random-class.js')}}"></script>


    <script>
        $(".dial").knob();

        $({animatedVal: 0}).animate({animatedVal: 80}, {
            duration: 2000,
            easing: "swing",
            step: function() {
                $(".dial").val(Math.ceil(this.animatedVal)).trigger("change");
            }
        });
    </script>
 <script src="{{asset('assets/js/charts/highcharts.js')}}"></script>
 <script src="{{asset('assets/js/charts/modules/series-label.js')}}"></script>
 <script src="{{asset('assets/js/charts/modules/exporting.js')}}"></script>


@endpush