    @extends('layouts.app')

@push('before-css')
    <!-- chartist CSS -->
    <link href="{{asset('plugins/vendors/morrisjs/morris.css')}}" rel="stylesheet">
    <!--c3 CSS -->
    <link href="{{asset('plugins/vendors/c3-master/c3.min.css')}}" rel="stylesheet">
    <!--Toaster Popup message CSS -->
    <link href="{{asset('plugins/vendors/toast-master/css/jquery.toast.css')}}" rel="stylesheet">
    <!-- Dashboard server Page CSS -->
    <link href="{{asset('assets/css/pages/dashboard-server.css')}}" rel="stylesheet">
@endpush

@section('content')
    <div class="container-fluid" id="dashboard">
        <!-- ============================================================== -->
        <!-- Info box -->
        <!-- ============================================================== -->
        <div class="row">
            <!-- Column -->
            <div class="col-lg-4 col-md-6">
                <a href="{{url('driver/driver')}}">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex pt-3 pb-2 no-block">
                            <div class="align-self-center mr-3 knob-icon">
                                <input class="dial"  data-plugin="knob" data-width="70" data-height="70"  data-linecap="round" data-fgColor="#f95476" data-thickness=".2" value="85" />
                                <i class=" fa fa-address-card text-pink"></i> </div>
                            <div class="align-slef-center mr-auto">
                                <h2 class="m-b-0 text-uppercase font-18 font-medium lp-5">Drivers</h2>
                                <h6 class="text-light m-b-0">Total Driver Registered <strong>{{ $data['drivers'] }}</strong></h6>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
            </div>
            <!-- Column -->
            <!-- Column -->
            <div class="col-lg-4 col-md-6">
                <a href="{{url('vehicle-field')}}">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex pt-3 pb-2 no-block">
                            <div class="align-self-center mr-3 knob-icon knob-icon-23">
                                <input  class="dial"  data-plugin="knob" data-width="70" data-height="70"  data-linecap="round" data-fgColor="#4886ff " data-thickness=".2" value="40" />
                                <i class="fa    fas fa-truck  font-25 text-primary"></i> </div>
                            <div class="align-slef-center mr-auto">
                                <h2 class="m-b-0 text-uppercase font-18 font-medium lp-5">Vehicles</h2>
                                <h6 class="text-light m-b-0">Total Vehicles <strong>{{$data['vehicles']}}</strong></h6>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
            </div>
            <!-- Column -->
            <!-- Column -->
            <div class="col-lg-4 col-md-12">
              <a href="{{url('company/casenote')}}">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex pt-3 pb-2 no-block">
                            <div class="align-self-center mr-3 knob-icon knob-icon-23">
                                <input class="dial"   data-plugin="knob" data-width="70" data-height="70"  data-linecap="round" data-fgColor="#ffb74e" data-thickness=".2" value="89" />
                                <i class="fas fa-sticky-note font-25 text-warning t-23 l-25"></i> </div>
                            <div class="align-slef-center mr-auto">
                                <h2 class="m-b-0 text-uppercase font-18 font-medium lp-5">Case Notes</h2>
                                <h6 class="text-light m-b-0"> Case Notes Registered <strong>{{$data['casenotes']}}</strong></h6>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
            </div>
            <!-- Column -->
        </div>

           <div class="row">
            <!-- Column -->
            <div class="col-lg-4 col-md-6">
                <a href="{{url('document/document')}}">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex pt-3 pb-2 no-block">
                            <div class="align-self-center mr-3 knob-icon">
                                <input class="dial"  data-plugin="knob" data-width="70" data-height="70"  data-linecap="round" data-fgColor="#f95476" data-thickness=".2" value="85" />
                                <i class=" fa fas fa-file-upload text-pink ml-28"></i> </div>
                            <div class="align-slef-center mr-auto">
                                <h2 class="m-b-0 text-uppercase font-18 font-medium lp-5"> Documents </h2>
                                <h6 class="text-light m-b-0"> Documents Requested  <strong>{{ $data['documents'] }}</strong></h6>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
            </div>
            <!-- Column -->
            <!-- Column -->
            <div class="col-lg-4 col-md-6">
                <a href="{{url('document_call')}}">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex pt-3 pb-2 no-block">
                            <div class="align-self-center mr-3 knob-icon knob-icon-23">
                                <input  class="dial"  data-plugin="knob" data-width="70" data-height="70"  data-linecap="round" data-fgColor="#4886ff " data-thickness=".2" value="40" />
                                <i class="fas fa-comments  font-25 text-primary"></i> </div>
                            <div class="align-slef-center mr-auto">
                                <h2 class="m-b-0 text-uppercase font-18 font-medium lp-5"> Document Calls </h2>
                                <h6 class="text-light m-b-0"> Document Calls Recieved  <strong>{{$data['Document_call']}}</strong></h6>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
            </div>
            <!-- Column -->
            <!-- Column -->
            <div class="col-lg-4 col-md-12">
              <a href="{{url('accident')}}">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex pt-3 pb-2 no-block">
                            <div class="align-self-center mr-3 knob-icon knob-icon-23">
                                <input class="dial"   data-plugin="knob" data-width="70" data-height="70"  data-linecap="round" data-fgColor="#ffb74e" data-thickness=".2" value="89" />
                                <i class="fas fa-car-crash font-25 text-warning ml-19"></i> </div>
                            <div class="align-slef-center mr-auto">
                                <h2 class="m-b-0 text-uppercase font-18 font-medium lp-5"> Accidents </h2>
                                <h6 class="text-light m-b-0"> Total Accidents <strong>{{$data['Accident']}}</strong></h6>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
            </div>
            <!-- Column -->
        </div>
        <!-- ============================================================== -->
        <!-- End Info box -->
        <!-- chart box one -->
        <!-- ============================================================== -->
        <div class="row">
            <!-- Column -->
            <div class="col-lg-12 col-md-12">
                <div class="card panel-main o-income panel-refresh">
                    <div class="refresh-container">
                        <div class="preloader">
                            <div class="loader">
                                <div class="loader__figure"></div>
                            </div>
                        </div>
                    </div>
                 
					 <div class="card-body panel-wrapper">
                        
                     
                        {!! $chart->container() !!}

                    </div>
                </div>
            </div>
           <div class="col-lg-12 col-md-12">
                <div class="card panel-main random-data o-income panel-refresh">
                    <div class="refresh-container">
                        <div class="preloader">
                            <div class="loader">
                                <div class="loader__figure"></div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body panel-wrapper" style="">
                        <div class="d-flex m-t-10 p-l-10 m-b-10 no-block">
                            <h5 class="card-title m-b-0 align-self-center">Latest Alerts</h5>
                        </div>
                        <div class="table-wrapper ">
                            
                            <div class="scrollbox2">
                                <div id="slimtest2" style="">
                                    <div class="table-responsive">
                                        <table class="table table-hover m-b-5">
                                            <thead>
                                                <th>Driver</th>
                                                <th>Carrier</th>
                                                <th>Type</th>
                                                <th>Expiration Date</th>
                                                <th>Description</th>
                                            </thead>
                                            <tbody>
                                            @foreach($data['alerts'] as $alert)
                                            <tr>
                                                <td><a href="{{url('/driver/driver/' . $alert->driver->driver->id)}}"
                                               title="View driver" class="text-danger">{{$alert->driver->driver->first_name ." ".$alert->driver->driver->last_name}}</a></td>

                                                <td>{{$alert->company->name}}</td>
                                                   <td> {{ substr(str_replace("_"," ",strip_tags(ucwords($alert->type))),0,70) }} </td>
                                                <td>@if($alert->expired_on != null)
                                                    {{Carbon\Carbon::parse($alert->expired_on)->format('m/d/Y')}} 
                                                    @endif
                                                </td>
                                                <td>{!!$alert->description!!} </td>
                                            </tr>
                                            @endforeach
                                            
                                         
                                           
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

          
            <!-- Column -->
        </div>
        <!-- ============================================================== -->
        <!-- End chart box one -->
        <!-- chart box two -->
        <!-- ============================================================== -->
        <div class="row" style="display: none;">
            <!-- Column -->
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex m-b-10 no-block">
                            <h5 class="card-title m-b-0 align-self-center">Average CPU usage</h5>
                        </div>
                        <div id="extra-area-chart"></div>
                    </div>
                </div>
            </div>
            <!-- Column -->
        </div>
        <!-- ============================================================== -->
        <!-- End chart box two -->
        <!-- chart box three -->
        <!-- ============================================================== -->
        <div class="row" style="display: none;">
            <!-- Column -->
            <div class="col-lg-12 col-md-12">
                <div class="card  o-income">
                    <div class="card-body">
                        <div class="row no-margin">
                            <div class="col-lg-8 col-md-12">
                                <div class="d-flex panel-main m-b-10 no-block  panel-refresh">
                                    <div class="refresh-container">
                                        <div class="preloader">
                                            <div class="loader">
                                                <div class="loader__figure"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <h5 class="card-title m-b-0 align-self-center">CPU productivity</h5>
                                    <div class="ml-auto text-light-blue"> <a href="#" class="pull-left text-light-blue inline-block refresh mr-15"> <i class="fas fa-sync"></i> Update </a> </div>
                                </div>
                                <ul class="list-inline m-b-10 text-uppercase lp-5 font-medium font-12">
                                    <li><i class="fa fa-square m-r-5 text-warning"></i> CPU</li>
                                    <li><i class="fa fa-square m-r-5 text-pink"></i> Memory</li>
                                    <li><i class="fa fa-square m-r-5 text-primary"></i> Disc (C: D:)</li>
                                </ul>
                                <div id="morris-area-chart"></div>
                            </div>
                            <div class="col-lg-4 col-md-12" >
                                <div class="panel-main pl-5 m-b-10 no-block  panel-refresh bordered-left-light bordered-css">
                                    <div class="refresh-container">
                                        <div class="preloader">
                                            <div class="loader">
                                                <div class="loader__figure"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <h5 class="card-title m-b-0 align-self-center">CPU</h5>
                                    <div class="block m-t-10 m-b-40">
                                        <div class="cpu-div-left"> <span class="block font-25 text-pink">10%</span> Using </div>
                                        <div class="cpu-div-right pl-4"> <span class="block font-25 text-primary">2.98 GHz</span> Speed </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <hr>
                                    <h5 class="card-title m-t-30 m-b-0 align-self-center">Memory</h5>
                                    <div class="block m-t-10 m-b-40">
                                        <div class="cpu-div-left"> <span class="block font-25 text-pink">4.4 Gb</span> Using </div>
                                        <div class="cpu-div-right pl-4"> <span class="block font-25 text-primary">3.6 Gb GHz</span> Available </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <hr>
                                    <h5 class="card-title m-t-30  m-b-0 align-self-center">Disc (C: D:)</h5>
                                    <div class="block m-t-10 m-b-30">
                                        <div class="cpu-div-left"> <span class="block font-25 text-pink">12%</span> Activity time </div>
                                        <div class="cpu-div-right pl-4"> <span class="block font-25 text-primary">27.5 ms</span> Response time </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Column -->
        </div>
        <!-- ============================================================== -->
        <!-- End chart box three -->
        <!-- table box one -->
        <!-- ============================================================== -->
        <div class="row" style="">
            <!-- Column -->
            <div class="col-lg-12 col-md-12">
                <div class="card panel-main o-income panel-refresh">
                    <div class="refresh-container">
                        <div class="preloader">
                            <div class="loader">
                                <div class="loader__figure"></div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body panel-wrapper">
                        <div class="d-flex m-b-10 no-block">
                            <h5 class="card-title m-b-0 align-self-center">Latest Drivers</h5>
                            <div class="ml-auto text-light-blue">  </div>
                        </div>
                        <div class="row scrollbox">
                            <div class="col-lg-12 " id="slimtest1" style="height:477px;">
                                <div class="table-responsive m-t-10">
                                    <table id="myTable" class="table table-bordered2 table-hover" style="overflow: hidden;">
                                        <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Carrier</th>
                                            <th>License</th>
                                            <th>Mobile Number </th>
                                           
                                        </tr>
                                        </thead>
                                        <tbody >
                                            @foreach($data['latest_drivers'] as $driver)
                                        <tr>
                                            <td><a href="{{url('/driver/driver/' . $driver->id)}}"
                                               title="View driver" class="text-warning">{{strtoupper($driver->driver_name)}}</a></td>
                                            <td>{{$driver->company_name}}</td>
                                            <td>{{$driver->license_state}}</td>
                                            <td>{{$driver->mobile_number}}</td>
                                            
                                        </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <!-- Column -->
            <div class="col-lg-4 col-md-12" style="display: none;">
                <div class="card panel-main random-data o-income panel-refresh">
                    <div class="refresh-container">
                        <div class="preloader">
                            <div class="loader">
                                <div class="loader__figure"></div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body p-10 panel-wrapper">
                        <div class="d-flex m-t-10 p-l-10 m-b-10 no-block">
                            <h5 class="card-title m-b-0 align-self-center">I/O Activity</h5>
                            <div class="ml-auto text-light-blue"> <a href="#" class="pull-left text-light-blue inline-block refresh m-r-15"> <i class="fas fa-sync"></i> Update </a> </div>
                        </div>
                        <div class="table-wrapper bookmarking">
                            <div class="bookmarking-main"> <span><i class="fas fa-circle text-primary"></i>Input</span> <span><i class="fas fa-circle text-warning"></i>Output</span> </div>
                            <div class="scrollbox">
                                <div id="slimtest2" style="height:480px;">
                                    <div class="table-responsive">
                                        <table class="table table-hover m-b-5">
                                            <tbody>
                                            <tr>
                                                <td><span class="txt-dark weight-500"><i class="fas fa-circle text-primary"></i>Database backup</span></td>
                                                <td>Beavis</td>
                                            </tr>
                                            <tr>
                                                <td><span class="txt-dark weight-500"><i class="fas fa-circle text-warning"></i>Form creating in task...</span></td>
                                                <td>Felix</td>
                                            </tr>
                                            <tr>
                                                <td><span class="txt-dark weight-500"><i class="fas fa-circle text-warning"></i>Add new products in...</span></td>
                                                <td>Neosoft</td>
                                            </tr>
                                            <tr>
                                                <td><span class="txt-dark weight-500"><i class="fas fa-circle text-warning"></i>Form creating in task...</span></td>
                                                <td>Felix</td>
                                            </tr>
                                            <tr>
                                                <td><span class="txt-dark weight-500"><i class="fas fa-circle text-primary"></i>Form creating in task...</span></td>
                                                <td>Cannibus</td>
                                            </tr>
                                            <tr>
                                                <td><span class="txt-dark weight-500"><i class="fas fa-circle text-warning"></i>Database backup</span></td>
                                                <td>Beavis</td>
                                            </tr>
                                            <tr>
                                                <td><span class="txt-dark weight-500"><i class="fas fa-circle text-primary"></i>Form creating in task...</span></td>
                                                <td>Felix</td>
                                            </tr>
                                            <tr>
                                                <td><span class="txt-dark weight-500"><i class="fas fa-circle text-primary"></i>Creating form statistics</span></td>
                                                <td>Cannibus</td>
                                            </tr>
                                            <tr>
                                                <td><span class="txt-dark weight-500"><i class="fas fa-circle text-warning"></i>Add new products in...</span></td>
                                                <td>Neosoft</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Column -->
        </div>
        <!-- ============================================================== -->
        <!-- End table box one -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
    </div>
@endsection

@push('js')
    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->
    <!--c3 JavaScript -->
    <script src="{{asset('plugins/vendors/d3/d3.min.js')}}"></script>
    <script src="{{asset('plugins/vendors/c3-master/c3.min.js')}}"></script>
    <!--jquery knob -->
    <script src="{{asset('plugins/vendors/knob/jquery.knob.js')}}"></script>
    <!--Sparkline JavaScript -->
    <script src="{{asset('plugins/vendors/sparkline/jquery.sparkline.min.js')}}"></script>
    <!--Morris JavaScript -->
    <script src="{{asset('plugins/vendors/raphael/raphael-min.js')}}"></script>
    <script src="{{asset('plugins/vendors/morrisjs/morris.js')}}"></script>
    <!-- Popup message jquery -->
    <script src="{{asset('plugins/vendors/toast-master/js/jquery.toast.js')}}"></script>
    <!-- Dashboard JS -->
    <script src="{{asset('assets/js/dashboard-server.js')}}"></script>
    <script src="{{asset('assets/js/random-class.js')}}"></script>


    <script>
        $(".dial").knob();

        $({animatedVal: 0}).animate({animatedVal: 80}, {
            duration: 2000,
            easing: "swing",
            step: function() {
                $(".dial").val(Math.ceil(this.animatedVal)).trigger("change");
            }
        });
    </script>
 <script src="{{asset('assets/js/charts/highcharts.js')}}"></script>
 <script src="{{asset('assets/js/charts/modules/series-label.js')}}"></script>
 <script src="{{asset('assets/js/charts/modules/exporting.js')}}"></script>
{!! $chart->script() !!}

@endpush