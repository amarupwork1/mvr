<!DOCTYPE html>
<html>
<head>
	<title>On Board Driver PDF</title>
	<style>.total_border{border-top:1px solid #000;}.borderBottom{border-bottom:1px solid #000;}</style>
</head>
<body>
	<h2 align="center">HOURS OF SERVICE RECORD<br><br>FOR<br><br>FIRST TIME OR INTERMITTENT DRIVERS</h2>
	<p><em>I</em><em>n</em><em>structi</em><em>o</em><em>ns:  </em>When using a driver for the first time or intermittently, a signed statement must be obtained, giving the total time on duty (driving and on duty) during the immediate previous<br>seven days and the time at which the driver was last relieved from duty prior to beginning work.</p>
	<table width="100%" style="margin-top:60px">
		<tr><td>Name <em>(print):</em></td><td colspan="4" class="borderBottom">{{ $driver_name }}</td></tr>
		<tr><td colspan="3" style="padding-top:50px">&nbsp;</td><td align="center">Day</td><td align="center">Total Time On Duty</td></tr>
		<tr><td colspan="3">&nbsp;</td><td align="center">1</td><td align="center">{{ $day1 }}</td></tr>
		<tr><td colspan="3">&nbsp;</td><td align="center">2</td><td align="center">{{ $day2 }}</td></tr>
		<tr><td colspan="3">&nbsp;</td><td align="center">3</td><td align="center">{{ $day3 }}</td></tr>
		<tr><td colspan="3">&nbsp;</td><td align="center">4</td><td align="center">{{ $day4 }}</td></tr>
		<tr><td colspan="3">&nbsp;</td><td align="center">5</td><td align="center">{{ $day5 }}</td></tr>
		<tr><td colspan="3">&nbsp;</td><td align="center">6</td><td align="center">{{ $day6 }}</td></tr>
		<tr><td colspan="3">&nbsp;</td><td align="center">7</td><td align="center">{{ $day7 }}</td></tr>
		<tr><td colspan="3">&nbsp;</td><td colspan="2" class="total_border"></td></tr>
		<tr><td colspan="3">&nbsp;</td><td align="right">Total</td><td align="center">{{ $totalHours }}</td></tr>
	</table>
	
	<p style="margin-top:60px">I hereby certify that the information contained herein is true to the best of my knowledge.  And  the time at which I was last relieved from duty prior was: </p>
	
	<p>Time and date last relieved from duty: <span class="borderBottom">{{ $date }} &nbsp; {{ $time }}</span></p>
	<p style="margin-top:40px">Signature:  _____________________________   Date: ______________________</p>
	
	<p style="margin-top:40px">395.8G)(2) Motor carriers, when using a driver for the first time or intermittently, shall obtain from the driver a signed statement giving the total time on duty during the immediately preceding 7 days and the time at which the driver was last relieved from duty prior to beginning work for the motor carriers.</p>
</body>
</html>