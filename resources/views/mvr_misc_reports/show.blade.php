@extends('layouts.app')

@section('content')
    <div class="container-fluid bg-white mt-5">
        <!-- .row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    <h3 class="box-title pull-left">Mvr_misc_report {{ $mvr_misc_report->id }}</h3>
                    @can('view-'.str_slug('Mvr_misc_report'))
                        <a class="btn btn-success pull-right" href="{{ url('/mvr_misc_reports') }}">
                            <i class="icon-arrow-left-circle" aria-hidden="true"></i> Back</a>
                    @endcan
                    <div class="clearfix"></div>
                    <hr>
                    <div class="table-responsive">
                        <table class="table table">
                            <tbody>
                            <tr>
                                <th>ID</th>
                                <td>{{ $mvr_misc_report->id }}</td>
                            </tr>
                            <tr><th> Mvr Report Id </th><td> {{ $mvr_misc_report->mvr_report_id }} </td></tr><tr><th> Cdl Medical Self Certification </th><td> {{ $mvr_misc_report->cdl_medical_self_certification }} </td></tr><tr><th> Medical Examiner Specialty </th><td> {{ $mvr_misc_report->medical_examiner_specialty }} </td></tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

