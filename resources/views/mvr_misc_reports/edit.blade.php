@extends('layouts.app')
@section('content')
    <div class="container-fluid bg-white mt-5">
        <div class="row">
            <div class="col-md-12">
                <div class="white-box">
                    <h3 class="box-title pull-left">Edit Mvr_misc_report #{{ $mvr_misc_report->id }}</h3>
                    @can('view-'.str_slug('Mvr_misc_report'))
                        <a class="btn btn-success pull-right" href="{{ url('/mvr_misc_reports') }}">
                            <i class="icon-arrow-left-circle" aria-hidden="true"></i> Back</a>
                    @endcan
                    <div class="clearfix"></div>
                    <hr>

                    @if ($errors->any())
                        <ul class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif

                    {!! Form::model($mvr_misc_report, [
                        'method' => 'PATCH',
                        'url' => ['/mvr_misc_reports', $mvr_misc_report->id],
                        'files' => true
                    ]) !!}

                    @include ('mvr_misc_reports.form', ['submitButtonText' => 'Update'])

                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
@endsection
