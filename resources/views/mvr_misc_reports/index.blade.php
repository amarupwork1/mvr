@extends('layouts.app')

@push('before-css')
    <link href="{{asset('plugins/components/datatables/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet"
          type="text/css"/>
@endpush

@section('content')
    <div class="container-fluid bg-white mt-5">
        <!-- .row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    <h3 class="box-title pull-left">Mvr Misc Reports</h3>
                    @can('add-'.str_slug('Mvr_misc_report'))
                        <a class="btn btn-success pull-right" href="{{ url('/mvr_misc_reports/create') }}"><i
                                    class="icon-plus"></i> Add Mvr Misc Reports</a>
                    @endcan
                    <div class="clearfix"></div>
                    <hr>
                    <div class="table-responsive">
                        <table class="table table-borderless" id="myTable">
                            <thead>
                            <tr>
                                <th>Mvr Report Id</th>

                                <th>Cdl Medical Self Certification</th>
                                <th>Medical Examiner Specialty</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                         
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection


@push('js')


        <script>

      $(function() {
       var oTable =  $('#myTable').DataTable({
            'aoColumnDefs': [{
                    'bSortable': false,
                  'aTargets': [-1] /* 1st one, start by the right */
                }],
            processing: true,
            serverSide: true,
             ajax: {
                url: "{{url()->current()}}"
            },

               columns: [
            {data: 'mvr_report_id', name: 'mvr_report_id'},
            {data: 'cdl_medical_self_certification',name: 'cdl_medical_self_certification'},
            {data: 'medical_examiner_specialty',name: 'medical_examiner_specialty'},

            {data: 'action',name: 'action'},
        ]

        });



    });
    </script>

@endpush