<div class="form-group row justify-content-center  {{ $errors->has('mvr_report_id') ? 'has-error' : ''}}">
    {!! Form::label('mvr_report_id', 'Mvr Report Id', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('mvr_report_id', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        {!! $errors->first('mvr_report_id', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group row justify-content-center  {{ $errors->has('cdl_medical_self_certification') ? 'has-error' : ''}}">
    {!! Form::label('cdl_medical_self_certification', 'Cdl Medical Self Certification', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('cdl_medical_self_certification', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        {!! $errors->first('cdl_medical_self_certification', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group row justify-content-center  {{ $errors->has('medical_examiner_specialty') ? 'has-error' : ''}}">
    {!! Form::label('medical_examiner_specialty', 'Medical Examiner Specialty', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('medical_examiner_specialty', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        {!! $errors->first('medical_examiner_specialty', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group row justify-content-center  {{ $errors->has('examiner_juridiction') ? 'has-error' : ''}}">
    {!! Form::label('examiner_juridiction', 'Examiner Juridiction', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('examiner_juridiction', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        {!! $errors->first('examiner_juridiction', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group row justify-content-center  {{ $errors->has('medical_restriction') ? 'has-error' : ''}}">
    {!! Form::label('medical_restriction', 'Medical Restriction', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('medical_restriction', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        {!! $errors->first('medical_restriction', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group row justify-content-center  {{ $errors->has('medical_examiner') ? 'has-error' : ''}}">
    {!! Form::label('medical_examiner', 'Medical Examiner', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('medical_examiner', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        {!! $errors->first('medical_examiner', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group row justify-content-center  {{ $errors->has('expire_date') ? 'has-error' : ''}}">
    {!! Form::label('expire_date', 'Expire Date', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('expire_date', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        {!! $errors->first('expire_date', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group row justify-content-center  {{ $errors->has('issue_date') ? 'has-error' : ''}}">
    {!! Form::label('issue_date', 'Issue Date', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('issue_date', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        {!! $errors->first('issue_date', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group row justify-content-center  {{ $errors->has('registry') ? 'has-error' : ''}}">
    {!! Form::label('registry', 'Registry', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('registry', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        {!! $errors->first('registry', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group row justify-content-center  {{ $errors->has('license') ? 'has-error' : ''}}">
    {!! Form::label('license', 'License', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('license', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        {!! $errors->first('license', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group row justify-content-center  {{ $errors->has('status') ? 'has-error' : ''}}">
    {!! Form::label('status', 'Status', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('status', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group row justify-content-center  {{ $errors->has('phone') ? 'has-error' : ''}}">
    {!! Form::label('phone', 'Phone', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('phone', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        {!! $errors->first('phone', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group row justify-content-center">
    <div class="col-lg-4 col-12 align-content-center">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
    </div>
</div>
