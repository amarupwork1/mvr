@extends('layouts.app')

@push('before-css')
    <link href="{{asset('plugins/components/datatables/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet"
          type="text/css"/>

          <style>
              
           #myTable_wrapper #myTable tr td.dataTables_empty {
                    display: table-cell;
                   
                }

          </style>
@endpush

@section('content')
    <div class="container-fluid bg-white mt-5">
        <!-- .row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    <h3 class="box-title pull-left">Mvr files</h3>
                    @can('add-'.str_slug('mvrfiles'))
                        <a class="btn btn-success pull-right" href="{{ url('/mvr-files/create') }}"><i
                                    class="icon-plus"></i> Add Mvrfiles</a>
                    @endcan
                    <div class="clearfix"></div>
                    <hr>
                    <div class="table-responsive">
                        <table class="table table-borderless" id="myTable">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Status</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                           
                            </tbody>
                        </table>
                        
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection



@push('js')
   <script>

      $(function() {
       var oTable =  $('#myTable').DataTable({
            'aoColumnDefs': [{
                    'bSortable': false,
                  'aTargets': [-1] /* 1st one, start by the right */
                }],
            processing: true,
            serverSide: true,
             ajax: {
                url: "{{url()->current()}}"
            },

               columns: [
            {data: 'id', name: 'id'},
            {data: 'name', name: 'name'},
            {data: 'status',name: 'status'},

            {data: 'action',name: 'action'},
        ]

        });



    });
    </script>
    </script>

@endpush