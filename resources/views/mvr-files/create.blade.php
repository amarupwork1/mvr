@extends('layouts.app')

@section('content')
    <div class="container-fluid bg-white mt-5">
        <div class="row">
            <div class="col-md-12">
                <div class="white-box">
                    <h3 class="box-title pull-left">Create New MvrFile</h3>
                    @can('view-'.str_slug('MvrFile'))
                        <a class="btn btn-success pull-right" href="{{url('/mvr-files')}}">
                            <i class="icon-arrow-left-circle"></i> View MvrFile</a>
                    @endcan
                    <div class="clearfix"></div>
                    <hr>
                    @if ($errors->any())
                        <ul class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif

                    {!! Form::open(['url' => '/mvr-files', 'files' => true]) !!}

                    @include ('mvr-files.form')

                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
@endsection
