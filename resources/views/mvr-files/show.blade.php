@extends('layouts.app')

@section('content')
    <div class="container-fluid bg-white mt-5">
        <!-- .row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    <h3 class="box-title pull-left">Mvr File </h3>
                    @can('view-'.str_slug('mvrfiles'))
                        <a class="btn btn-success pull-right" href="{{ url('/mvr-files') }}">
                            <i class="icon-arrow-left-circle" aria-hidden="true"></i> Back</a>
                    @endcan
                    <div class="clearfix"></div>
                    <hr>
                    <div class="table-responsive">
                        <table class="table table">
                            <tbody>
                           
                          
                            <tr>
                                <th> File </th>
                                <td> @if(!empty($mvrfile->path)) <a target="_blank" class="btn btn-primary " href="{{URL::asset('storage/uploads/mvrfiles/'.$mvrfile->path)}}"> View File {{ $mvrfile->name }} </a>@else No File @endif</td>
                            </tr>
                            <tr>
                                <th> Status </th>
                                <td> @if($mvrfile->status==1) Active @else In Active @endif </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

