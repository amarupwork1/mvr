<header class="topbar">
    <div Class="container2">
        <nav class="navbar top-navbar navbar-expand-md navbar-light  @if(auth()->check() && Auth::user()->isCompany()) CompanyProfile @endif">
            <!-- ============================================================== -->
            <!-- Logo -->
            <!-- ============================================================== -->
            <div class="navbar-header"><a class="navbar-brand" href="{{url('/')}}">


               @if(auth()->check() && Auth::user()->isCompany())
                    @if(auth()->user()->profile->pic == null or (!file_exists('storage/uploads/users/'.auth()->user()->profile->pic)))
                         <img width="160" src="{{asset('assets/imgs/site-logo.png')}}" alt="logo" class="dark-logo">
                    @else
                        <img class="company_img" width="160" height="75"  src="{{asset('storage/uploads/users/'.auth()->user()->profile->pic)}}"
                             alt="logo" class="dark-logo">
                    @endif

                @else
                    <img width="160" src="{{asset('assets/imgs/site-logo.png')}}" alt="logo" class="dark-logo">
                @endif
                    </a>
                </div>
            <!-- ============================================================== -->

            <!-- End Logo -->

             <!-- comapny name -->
            <div class="comany_title">
            @if(auth()->check() && Auth::user()->isCompany())
            <h3>  {{ auth()->user()->name }}</h3>
            @endif


            </div>

             <!-- End company name  -->
            <!-- ============================================================== -->
            <div class="top-bar-main">
                <!-- ============================================================== -->
                <!-- toggle and nav items -->
                <!-- ============================================================== -->
                <div class="float-left">
                    <ul class="navbar-nav">
                        <li class="nav-item "><a
                                    class="nav-link navbar-toggler sidebartoggler waves-effect waves-dark float-right"
                                    href="javascript:void(0)"><span class="navbar-toggler-icon"></span></a></li>
                        <!-- ============================================================== -->
                        <!-- Search -->
                        <!-- ============================================================== -->
                        

                    </ul>
                </div>
                <!-- ============================================================== -->
                <!-- User profile and search -->
                <!-- ============================================================== -->
                @if(auth()->check())
                  @php

                    $notification_user = App\User::find(auth()->user()->id);


                      

                      
                  @endphp

                    <div class="float-right pr-3">
                        <ul class="navbar-nav my-lg-0 float-right User_ProfileUl">
                            <!-- Notification -->
                              <li class="nav-item dropdown"><a class="nav-link dropdown-toggle waves-effect waves-dark notification_request"
                                                             href="" data-toggle="dropdown" aria-haspopup="true"
                                                             aria-expanded="false"> <i class="mdi mdi-bell"></i>
                                    <div class="notify">
                                      <!-- New Notification -->
                                      @if($notification_user->unread_notification_count())
                                          <span class="heartbit"></span>
                                          <span class="point"></span>
                                      @endif
                                      <!-- End New Notification -->
                                    </div>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right mailbox animated fadeIn">
                                    <ul>
                                        <li>

                                            @if($notification_user->unread_notification_count())
                                                <div class="drop-title">You have <span class="highlighted">{{$notification_user->unread_notification_count()}} new</span>
                                                Notifications
                                                </div>
                                            @else
                                                <div class="drop-title">You do not have new notification.
                                                </div>

                                            @endif
                                            
                                        </li>
                                        <li>
                                            <div class="message-center">
                                                <!-- Message -->

                                                @foreach ($notification_user->unread_notification()->get() as $notification):
                                                    
                                                    <a href="{{ url('notifications')}}">
                                                    <div class="mail-content"><i class="fas fa-envelope"></i> {{ Illuminate\Support\Str::words($notification->data['message'], 20," ...") }}
                                                        <span class="float-right text-light">{{ $notification->created_at->diffForHumans() }}</span></div>
                                                </a>
                                                @endforeach

                                                
                                                
                                                </div>
                                        </li>
                                        <li><a class="nav-link text-center" href="{{ url('notifications')}}">See all
                                                notifications </a></li>
                                    </ul>
                                </div>
                            </li>

                            <div class="User_Profile">
                                <span>
                                    <!-- <i class="fas fa-bell BellIcon"></i> -->
                                    <span class="mobile">{{auth()->user()->name}}</span>
                                    <i class="fas fa-angle-down DownArrow"></i>
                                </span>
                            </div>
                            <li class="nav-item dropdown u-pro">
                                <a class="nav-link dropdown-toggle waves-effect waves-dark profile-pic" href=""
                                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    @if(auth()->check())
                                        @if(auth()->user()->profile->pic == null or (!file_exists('storage/uploads/users/'.auth()->user()->profile->pic)))
                                            <img src="{{asset('storage/uploads/users/no_avatar.jpg')}}" alt="user-img"
                                            >
                                        @else
                                            <img src="{{asset('storage/uploads/users/'.auth()->user()->profile->pic)}}"
                                                 alt="user-img">
                                        @endif
                                    @endif
                                    <span class="circle-status"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right animated fadeIn">
                                    <ul class="dropdown-user">
                                        <li class="text-center">
                                            <div class="dw-user-box">
                                                <div class="u-img">

                                                    @if(auth()->user()->profile->pic == null)
                                                        <img src="{{asset('storage/uploads/users/no_avatar.jpg')}}"
                                                             alt="user-img"
                                                        >
                                                    @else
                                                    <img src="{{asset('storage/uploads/users/no_avatar.jpg')}}"
                                                             alt="user-img"
                                                        >
                                                    @endif


                                                    <div class="clearfix"></div>
                                                    <div class="u-text">
                                                        <h4>{{auth()->user()->name}}</h4>
                                                        <p class="text-light"><span
                                                                    class="status-circle bg-success"></span>online
                                                            <i class="fas fa-chevron-down small"></i></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li role="separator" class="divider"></li>

                                        <li role="separator" class="divider"></li>
                                        <li><a href="{{url('account-settings')}}"><i class="fas fa-user mr-1"></i> My Profile</a></li>
                                        <li><a href="{{url('account-settings')}}"><i class="fas fa-cog mr-1"></i>
                                                Settings</a></li>
                                        <li role="separator" class="divider"></li>
                                        <li><a href="{{url('logout')}}"><i class="fas fa-sign-in-alt mr-1"></i>
                                                Logout</a></li>
                                    </ul>
                                </div>

                            </li>

                        </ul>
                    </div>
                @else

                    <div class="float-right pr-3">
                        <ul class="navbar-nav my-lg-0 float-right">
                            <li class="nav-item">
                                <a class="nav-link " href="{{url('login')}}"><i class="fa fa-user"></i> Login</a>
                            </li>

                        </ul>
                    </div>
                @endif

                <div class="clearfix"></div>
            </div>
        </nav>
    </div>
</header>
