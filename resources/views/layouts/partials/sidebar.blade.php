<aside class="left-sidebar">
	<ul class="nav-bar navbar-inverse hidden-xs-down">
		<li style="display: none;" class="nav-item"><a class="nav-link navbar-toggler sidebartoggler  waves-effect waves-dark float-right" href="javascript:void(0)"><span class="navbar-toggler-icon"></span></a>
		</li>
	</ul>
	<!-- Sidebar scroll-->
	<div class="scroll-sidebar">
		<!-- Sidebar navigation-->

		<nav class="sidebar-nav">
			<ul id="sidebarnav">
				<li class="">
					<a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i
                                class="flaticon-desktop-computer-screen-with-rising-graph"></i><span class="hide-menu">Dashboard</span>
                     </a>
					<ul aria-expanded="false" class="collapse">
						<li>
							<a href="{{url('/')}}"> Dashboard</a>
						</li>

					</ul>
				</li>



				<li class=""><a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false">
					<i class="fas fa-chart-bar"></i><span class="hide-menu">Reports</span></a>
					<ul aria-expanded="false" class="collapse">
					

						<li>
							<a href="{{url('mvr_reports')}}" class="">Mvr Reports</a>
						</li>

						
					</ul>
				</li>

				<li class=""><a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false">
					<i class="fas fa-file"></i><span class="hide-menu">MVR FILES</span></a>
					<ul aria-expanded="false" class="collapse">
					

						<li>
							<a href="{{url('mvr-files')}}" class="">Mvr Files</a>
						</li>

					</ul>
				</li>


				

		



		<!-- end company sidebar-->

			</ul>
		</nav>
		<!-- End Sidebar navigation -->
	</div>
	<!-- End Sidebar scroll-->
</aside>

<style type="text/css">
.sidebar-nav>ul>li>a i {font-size: 19px;margin-right: 12px !important;display: block;vertical-align: middle;color: #acacaf;    max-width: 26px;width: 100%;text-align: center;float: left;font-style: normal;}

.fix-header .topbar {padding: 10px 15px;box-shadow: 0 0 20px rgba(0,0,0,0.1);}
</style>
