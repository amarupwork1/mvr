<!DOCTYPE html>
<html lang="en">
<head>
    
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="{{ config('app.name') }}">
    <meta name="author" content="{{ config('app.name') }}">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <!-- Favicon icon -->
    <link rel="shortcut icon" type="image/x-icon"  href="{{asset('assets/imgs/favicon.ico')}}">
    <title>{{ config('app.name') }}</title>
    <!-- Bootstrap Core CSS -->
    <link href="{{asset('plugins/vendors/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('plugins/vendors/perfect-scrollbar/css/perfect-scrollbar.css')}}" rel="stylesheet">
    <!-- This page CSS -->
    @stack('before-css')


     <link href="{{asset('plugins/components/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet" type="text/css"/>
     <link href="https://cdn.datatables.net/plug-ins/1.10.19/integration/font-awesome/dataTables.fontAwesome.css" type="text/css">
    <link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet"
          type="text/css"/>
    <link rel="stylesheet" href="{{asset('plugins/vendors/toast-master/css/jquery.toast.css')}}">
    <link rel="stylesheet" href="{{asset('plugins/vendors/summernote/summernote-bs4.css')}}">
    <!-- Custom CSS -->
    <link href="{{asset('assets/css/style.css?v=2.0')}}" rel="stylesheet">
  
    <link href="{{asset('assets/css/responsive.css')}}" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    @stack('after-css')

    <style>
        
.autocomplete-wrapper { margin: 44px auto 44px; max-width: 600px; }
.autocomplete-wrapper label { display: block; margin-bottom: .75em; color: #3f4e5e; font-size: 1.25em; }
.autocomplete-wrapper .text-field { padding: 0 15px; width: 100%; height: 40px; border: 1px solid #CBD3DD; font-size: 1.125em; }
.autocomplete-wrapper ::-webkit-input-placeholder { color: #CBD3DD; font-style: italic; font-size: 18px; }
.autocomplete-wrapper :-moz-placeholder { color: #CBD3DD; font-style: italic; font-size: 18px; }
.autocomplete-wrapper ::-moz-placeholder { color: #CBD3DD; font-style: italic; font-size: 18px; }
.autocomplete-wrapper :-ms-input-placeholder { color: #CBD3DD; font-style: italic; font-size: 18px; }

.autocomplete-suggestions { overflow: auto; border: 1px solid #CBD3DD; background: #FFF; }

.autocomplete-suggestion { overflow: hidden; padding: 5px 15px; white-space: nowrap; }

.autocomplete-selected { background: #F0F0F0; }

.autocomplete-suggestions strong { color: #029cca; font-weight: normal; }

    </style>
</head>
<body class="fix-header fix-sidebar card-no-border">
<!-- ============================================================== -->
<!-- Preloader - style you can find in spinners.css')}} -->
<!-- ============================================================== -->
<div class="preloader">
    <div class="loader">
        <div class="loader__figure"></div>
        <p class="loader__label">{{ config('app.name') }}</p>
    </div>
</div>
<!-- ============================================================== -->
<!-- Main wrapper - style you can find in pages.scss -->
<!-- ============================================================== -->
<div id="main-wrapper">
    <!-- ============================================================== -->
    <!-- Container1140px -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Topbar header - style you can find in pages.scss -->
    <!-- ============================================================== -->
    @include('layouts.partials.header')
    <!-- ============================================================== -->
    <!-- End Topbar header -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Left Sidebar - style you can find in sidebar.scss  -->
    <!-- ============================================================== -->
    <div class="container">
        @include('layouts.partials.sidebar')
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            @yield('content')
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Page Content -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- footer -->
    <!-- ============================================================== -->

    <!-- ============================================================== -->
    <!-- End footer -->
    <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Page wrapper  -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- End Wrapper -->
<!-- ============================================================== -->
<!-- All Jquery -->
<!-- ============================================================== -->
<script src="{{asset('plugins/vendors/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap popper Core JavaScript -->
<script src="{{asset('plugins/vendors/bootstrap/js/popper.min.js')}}"></script>
<script src="{{asset('plugins/vendors/bootstrap/js/bootstrap.min.js')}}"></script>
<!-- slimscrollbar scrollbar JavaScript -->
<script src="{{asset('plugins/vendors/ps/perfect-scrollbar.jquery.min.js')}}"></script>
<!--Wave Effects -->
<script src="{{asset('assets/js/waves.js')}}"></script>
<!--Menu sidebar -->
<script src="{{asset('assets/js/sidebarmenu.js')}}"></script>
<!--Custom JavaScript -->
<script src="{{asset('assets/js/custom.min.js')}}"></script>
<script src="{{asset('plugins/vendors/toast-master/js/jquery.toast.js')}}"></script>
<script src="{{asset('plugins/vendors/summernote/summernote-bs4.min.js')}}"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="{{asset('plugins/components/toast-master/js/jquery.toast.js')}}"></script>
<script src="{{asset('plugins/components/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('plugins/components/datatables/bootstrap.js')}}"></script>
<script src="{{asset('plugins/ck/ckeditor.js')}}"></script>
<script src="{{asset('plugins/ck/adapters/jquery.js')}}"></script>
  <script>
  var route_prefix = "{{ url(config('lfm.url_prefix', config('lfm.prefix'))) }}";
  </script>
    <script>
       $('.article-ckeditor').ckeditor({
             height: 700,
            allowedContent : true,
            image_previewText: " ",
            filebrowserImageBrowseUrl: route_prefix + '?type=Images',
            filebrowserImageUploadUrl: route_prefix + '/upload?type=Images&_token={{csrf_token()}}',
            filebrowserBrowseUrl: route_prefix + '?type=Files',
            filebrowserUploadUrl: route_prefix + '/upload?type=Files&_token={{csrf_token()}}',

       });
    </script>
@stack('js')
    
<script>
  
  $('.notification_request').click(function(){
       $.ajax({
               type:'GET',
               url:'/markNotificationRead',
               data:'_token = <?php echo csrf_token() ?>',
               success:function(data) {
                  $("#msg").html(data.msg);
               }
            });
  });

   $('#slimtest1, #slimtest2').perfectScrollbar();
   $('table').on('click', '.btn-danger', function(event)
    {
        event.preventDefault();
      id = $(this).val();
      console.log(id);
         swal({
                  title: "Are you sure?",
                  text: "It will permanently deleted !",
                  icon: "warning",
                  buttons: true,
                  dangerMode: true,
                })
                .then((willDelete) => {
                  if (willDelete) {
                    swal("Selected item has been deleted!", {
                      icon: "success",
                    });
                 $("#"+id).submit();


                  } 
                });
    });


    $('table').on('click', '.archive_alert', function(event)
    {
        event.preventDefault();
      id = $(this).val();
      console.log(id);
         swal({
                  title: "Are you sure?",
                  text: "It will archived !",
                  icon: "warning",
                  buttons: true,
                  dangerMode: true,
                })
                .then((willDelete) => {
                  if (willDelete) {
                    swal("Selected record has been archived!", {
                      icon: "success",
                    });
                 $("#"+id).submit();


                  } 
                });
    });

 $(document).ready(function () {
    $('.textarea').summernote({
        height: 500,                 // set editor height
        minHeight: null,             // set minimum height of editor
        maxHeight: null,             // set maximum height of editor
        focus: true                  // set focus to editable area after initializing summernote
    });
    @php
    $icon  = 'success';
    $heading = "Success!";
     if(\Session::has('icon'))
        $icon = session()->get('icon');
     if(\Session::has('heading'))
        $heading = session()->get('heading');

    @endphp


            
              if ( typeof window.performance != "undefined" && 
                                          window.performance.navigation.type === 2  ) {
              }
              else{

            @if(\Session::has('message'))

 

            $.toast({
                heading: '{{$heading}}',
                position: 'top-center',
                text: '{{session()->get('message')}}',
                loaderBg: '#ff6849',
                icon: '{{$icon}}',
                hideAfter: 3000,
                stack: 6
            });
            @endif
              @if(\Session::has('flash_message'))
            $.toast({
                heading: '{{$heading}}',
                position: 'top-center',
                text: '{{session()->get('flash_message')}}',
                loaderBg: '#ff6849',
                icon: '{{$icon}}',
                hideAfter: 3000,
                stack: 6
            });
            @endif
            }
            
        });

</script>

<script src="{{asset('assets/js/search/adminSearch.js')}}"></script>
<script src="{{asset('assets/js/search/jquery.mockjax.js')}}"></script>
<script src="{{asset('assets/js/search/jquery.autocomplete.js')}}"></script>
<script type="text/javascript">
    
$(function () {
    'use strict';
   
    var queryArray = $.map(query, function (value, key) { return { value: value, data: key }; });
      

    $('#autocomplete-ajax').devbridgeAutocomplete({
        lookup: queryArray,
        lookupFilter: function(suggestion, originalQuery, queryLowerCase) {
            var re = new RegExp('\\b' + $.Autocomplete.utils.escapeRegExChars(queryLowerCase), 'gi');
            console.log(suggestion);
            return re.test(suggestion.value);
        },
        onSelect: function(suggestion) {
            var base_url = "{{ URL::to('/')}}";
            //console.log();
            window.location = base_url+suggestion.data;

            $('#selction-ajax').html('You selected: ' + suggestion.value + ', ' + suggestion.data);
        },
        onHint: function (hint) {
            console.log(hint);
            $('#autocomplete-ajax-x').val(hint);
        }
    });


    
    
    
});



</script>
</body>
</html>
