<!DOCTYPE html>
<html>
<head>
	<title>Employer Verification PDF</title>
	<style>
		.main-head{text-transform:uppercase;}
		.head_tr th{text-transform:uppercase;}
		.border_bottom{border-bottom:1px solid #000;}
		table{width:100%;float:left;}.small_txt{font-size:12px;}
	</style>
	
</head>
<body>
	<h4 align="center" class="main-head">Safety Performance History Records Request</h4>
	<table cellpadding="6" cellspacing="0" style="" rules="none" >
    <tbody>
        <tr class="head_tr">
            <th colspan="3" style="border-left: 2px solid; border-top: 2px solid; border-bottom: 2px solid;">Section 1</th>
            <th colspan="4" style="border-top: 2px solid; border-bottom: 2px solid; border-right: 2px solid;" >Authorization</th>          
        </tr>
		<!--tr>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
		</tr -->
		<tr>
			<td style="padding-top:25px" width="50">I, </td>
			<td style="padding-top:25px" colspan="4" class="border_bottom">{{ $driver_name }}</td>
			<td style="padding-top:25px" class="border_bottom">&nbsp;</td>
			<td style="padding-top:25px">, hereby authorize:</td>
		</tr>
		<tr>
			<td width="100">Previous Employer: </td>
			<td colspan="4" class="border_bottom">{{ $prev_company }}</td>
			<td align="right">Email: </td>
			<td class="border_bottom" >{{ $prev_email }}</td>
		</tr>
		<tr>
			<td width="100">Street Address: </td>
			<td colspan="4" class="border_bottom">{{ $prev_street_address }}</td>
			<td align="right">Phone: </td>
			<td class="border_bottom">{{ $prev_phone }}</td>
		</tr>
		<tr>
			<td width="100">City, State, Zip: </td>
			<td colspan="4" class="border_bottom">{{ $prev_city }}, {{ $prev_state }}, {{ $prev_zipcode }}</td>
			<td align="right">Fax: </td>
			<td class="border_bottom">{{ $prev_fax }}</td>
		</tr>
		<tr>
			<td colspan="7">to release and forward the information requested by section 3 of this document concerning my Alcohal and Controlled Substance Testing records within the previous 3 years from _____________________________</td>
		</tr>
		<tr>
			<td colspan="4"></td>
			<td colspan="3" align="right" class="small_txt">(Date of Employment Application)</td>
		</tr>
		<tr>
			<td colspan="7">to:</td>
		</tr>
		<tr>
			<td width="100">Prospective Employer: </td>
			<td colspan="4" class="border_bottom">{{ $prev_company }}</td>
			<td align="right">Email: </td>
			<td class="border_bottom" >{{ $prev_email }}</td>
		</tr>
		<tr>
			<td width="100">Street Address: </td>
			<td colspan="4" class="border_bottom">{{ $prev_street_address }}</td>
			<td align="right">Phone: </td>
			<td class="border_bottom">{{ $prev_phone }}</td>
		</tr>
		<tr>
			<td width="100">City, State, Zip: </td>
			<td colspan="4" class="border_bottom">{{ $prev_city }}, {{ $prev_state }}, {{ $prev_zipcode }}</td>
			<td align="right">Fax: </td>
			<td class="border_bottom">{{ $prev_fax }}</td>
		</tr>
    </tbody>
</table>
</body>
</html>